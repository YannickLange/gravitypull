/********************************************************************************
** Form generated from reading UI file 'gravityeditor.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRAVITYEDITOR_H
#define UI_GRAVITYEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GravityEditorClass
{
public:
    QAction *actionPhysics;
    QAction *actionFPSCamera;
    QAction *actionStaticCamera;
    QAction *actionPointLight;
    QAction *actionDirectionalLight;
    QAction *actionSpotLight;
    QAction *actionSave;
    QAction *actionSave_Scene;
    QAction *actionSave_Scene_2;
    QAction *actionLoad_Scene;
    QAction *actionMeshRender;
    QAction *actionParticles;
    QAction *actionSound;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuView;
    QMenu *menuActors;
    QMenu *menuLights;
    QMenu *menuCamera;
    QMenu *menuObjects;
    QMenu *menuRender;
    QStatusBar *statusBar;
    QDockWidget *componentDock;
    QWidget *componentDockContents;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QPushButton *createComponentButton;
    QDockWidget *actorDock;
    QWidget *actorDockContents;
    QVBoxLayout *verticalLayout_2;
    QTreeView *actorTreeView;
    QPushButton *createActorButton;
    QDockWidget *assetDock;
    QWidget *assetDockContents;
    QVBoxLayout *verticalLayout_4;
    QTreeView *assetTreeView;

    void setupUi(QMainWindow *GravityEditorClass)
    {
        if (GravityEditorClass->objectName().isEmpty())
            GravityEditorClass->setObjectName(QStringLiteral("GravityEditorClass"));
        GravityEditorClass->resize(1280, 720);
        QPalette palette;
        QBrush brush(QColor(200, 199, 204, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(35, 36, 38, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        QBrush brush2(QColor(45, 46, 48, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush2);
        QBrush brush3(QColor(250, 95, 33, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        GravityEditorClass->setPalette(palette);
        GravityEditorClass->setStyleSheet(QLatin1String("selection-color: rgb(250, 95, 33);\n"
"selection-background-color: rgb(45, 46, 48);\n"
"background-color: rgb(35, 36, 38);\n"
"color: rgb(200, 199, 204);"));
        GravityEditorClass->setDocumentMode(false);
        GravityEditorClass->setTabShape(QTabWidget::Triangular);
        GravityEditorClass->setDockNestingEnabled(true);
        actionPhysics = new QAction(GravityEditorClass);
        actionPhysics->setObjectName(QStringLiteral("actionPhysics"));
        actionFPSCamera = new QAction(GravityEditorClass);
        actionFPSCamera->setObjectName(QStringLiteral("actionFPSCamera"));
        actionStaticCamera = new QAction(GravityEditorClass);
        actionStaticCamera->setObjectName(QStringLiteral("actionStaticCamera"));
        actionPointLight = new QAction(GravityEditorClass);
        actionPointLight->setObjectName(QStringLiteral("actionPointLight"));
        actionDirectionalLight = new QAction(GravityEditorClass);
        actionDirectionalLight->setObjectName(QStringLiteral("actionDirectionalLight"));
        actionSpotLight = new QAction(GravityEditorClass);
        actionSpotLight->setObjectName(QStringLiteral("actionSpotLight"));
        actionSave = new QAction(GravityEditorClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave_Scene = new QAction(GravityEditorClass);
        actionSave_Scene->setObjectName(QStringLiteral("actionSave_Scene"));
        actionSave_Scene_2 = new QAction(GravityEditorClass);
        actionSave_Scene_2->setObjectName(QStringLiteral("actionSave_Scene_2"));
        actionLoad_Scene = new QAction(GravityEditorClass);
        actionLoad_Scene->setObjectName(QStringLiteral("actionLoad_Scene"));
        actionMeshRender = new QAction(GravityEditorClass);
        actionMeshRender->setObjectName(QStringLiteral("actionMeshRender"));
        actionParticles = new QAction(GravityEditorClass);
        actionParticles->setObjectName(QStringLiteral("actionParticles"));
        actionSound = new QAction(GravityEditorClass);
        actionSound->setObjectName(QStringLiteral("actionSound"));
        centralWidget = new QWidget(GravityEditorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("background-color: rgb(63, 63, 63);"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        GravityEditorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GravityEditorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setEnabled(true);
        menuBar->setGeometry(QRect(0, 0, 1280, 18));
        menuBar->setContextMenuPolicy(Qt::DefaultContextMenu);
        menuBar->setAutoFillBackground(false);
        menuBar->setStyleSheet(QLatin1String("QMenuBar::item {\n"
"    spacing: 3px; /* spacing between menu bar items */\n"
"	padding: 1px 4px;\n"
"    background: transparent;\n"
"	background-color: rgb(35, 36, 38);\n"
"	selection-color: rgb(250, 95, 33);\n"
"}\n"
"\n"
"QMenuBar::item:selected { /* when selected using mouse or keyboard */\n"
"    background:  rgb(45, 46, 48);\n"
"	background-color: rgb(45, 46, 48);\n"
"	color: rgb(250, 95, 33);\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"	background-color: rgb(45, 46, 48);\n"
"	selection-color: rgb(250, 95, 33);\n"
"}"));
        menuBar->setDefaultUp(false);
        menuBar->setNativeMenuBar(false);
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuFile->setStyleSheet(QStringLiteral(""));
        menuFile->setTearOffEnabled(true);
        menuFile->setSeparatorsCollapsible(false);
        menuFile->setToolTipsVisible(false);
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuEdit->setStyleSheet(QStringLiteral(""));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuActors = new QMenu(menuBar);
        menuActors->setObjectName(QStringLiteral("menuActors"));
        menuLights = new QMenu(menuActors);
        menuLights->setObjectName(QStringLiteral("menuLights"));
        menuCamera = new QMenu(menuActors);
        menuCamera->setObjectName(QStringLiteral("menuCamera"));
        menuObjects = new QMenu(menuBar);
        menuObjects->setObjectName(QStringLiteral("menuObjects"));
        menuRender = new QMenu(menuObjects);
        menuRender->setObjectName(QStringLiteral("menuRender"));
        GravityEditorClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(GravityEditorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        GravityEditorClass->setStatusBar(statusBar);
        componentDock = new QDockWidget(GravityEditorClass);
        componentDock->setObjectName(QStringLiteral("componentDock"));
        componentDock->setMinimumSize(QSize(150, 187));
        componentDock->setStyleSheet(QStringLiteral(""));
        componentDock->setFeatures(QDockWidget::AllDockWidgetFeatures);
        componentDockContents = new QWidget();
        componentDockContents->setObjectName(QStringLiteral("componentDockContents"));
        verticalLayout_3 = new QVBoxLayout(componentDockContents);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        scrollArea = new QScrollArea(componentDockContents);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 130, 378));
        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_3->addWidget(scrollArea);

        createComponentButton = new QPushButton(componentDockContents);
        createComponentButton->setObjectName(QStringLiteral("createComponentButton"));

        verticalLayout_3->addWidget(createComponentButton);

        componentDock->setWidget(componentDockContents);
        GravityEditorClass->addDockWidget(static_cast<Qt::DockWidgetArea>(2), componentDock);
        actorDock = new QDockWidget(GravityEditorClass);
        actorDock->setObjectName(QStringLiteral("actorDock"));
        actorDock->setMinimumSize(QSize(150, 217));
        actorDock->setStyleSheet(QStringLiteral(""));
        actorDockContents = new QWidget();
        actorDockContents->setObjectName(QStringLiteral("actorDockContents"));
        verticalLayout_2 = new QVBoxLayout(actorDockContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        actorTreeView = new QTreeView(actorDockContents);
        actorTreeView->setObjectName(QStringLiteral("actorTreeView"));
        actorTreeView->setStyleSheet(QLatin1String("QTeeView {\n"
"    show-decoration-selected: 1;\n"
"}\n"
"\n"
"QTreeView::item:hover {\n"
"    background-color: rgba(170, 170, 170, 100);\n"
"	color: rgb(250, 95, 33);\n"
"}\n"
"\n"
"QTreeView::item:selected {\n"
"	background-color: rgba(200, 200, 200, 100);\n"
"	color: rgb(250, 95, 33);\n"
"	border: 1px solid rgb(250, 95, 33);\n"
"}\n"
"\n"
"QTreeView::branch {\n"
"        color: cyan;\n"
"}\n"
"\n"
"QTreeView::branch:selected {\n"
"	color: white;\n"
"	gridline-color: rgba(255, 255, 255, 255);\n"
"}\n"
"\n"
"QTreeView::branch:has-siblings:!adjoins-item {\n"
"        color: cyan;\n"
"}\n"
"\n"
"QTreeView::branch:has-siblings:adjoins-item {\n"
"        color: red;\n"
"}\n"
"\n"
"QTreeView::branch:!has-children:!has-siblings:adjoins-item {\n"
"        color: blue;\n"
"}\n"
"\n"
"QTreeView::branch:closed:has-children:has-siblings {\n"
"        color: pink;\n"
"}\n"
"\n"
"QTreeView::branch:has-children:!has-siblings:closed {\n"
"        color: gray;\n"
"}\n"
"\n"
"QTreeView::branch:open:has-children:has-siblings "
                        "{\n"
"        color: magenta;\n"
"}\n"
"\n"
"QTreeView::branch:open:has-children:!has-siblings {\n"
"        color: green;\n"
"}"));

        verticalLayout_2->addWidget(actorTreeView);

        createActorButton = new QPushButton(actorDockContents);
        createActorButton->setObjectName(QStringLiteral("createActorButton"));

        verticalLayout_2->addWidget(createActorButton);

        actorDock->setWidget(actorDockContents);
        GravityEditorClass->addDockWidget(static_cast<Qt::DockWidgetArea>(1), actorDock);
        assetDock = new QDockWidget(GravityEditorClass);
        assetDock->setObjectName(QStringLiteral("assetDock"));
        assetDock->setMinimumSize(QSize(93, 190));
        assetDockContents = new QWidget();
        assetDockContents->setObjectName(QStringLiteral("assetDockContents"));
        verticalLayout_4 = new QVBoxLayout(assetDockContents);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        assetTreeView = new QTreeView(assetDockContents);
        assetTreeView->setObjectName(QStringLiteral("assetTreeView"));
        assetTreeView->setStyleSheet(QLatin1String(".QWidget {\n"
"   background-color: beige;\n"
"}\n"
"\n"
"/* Nice Windows-XP-style password character. */\n"
"QLineEdit[echoMode=\"2\"] {\n"
"    lineedit-password-character: 9679;\n"
"}\n"
"\n"
"/* We provide a min-width and min-height for push buttons\n"
"   so that they look elegant regardless of the width of the text. */\n"
"QPushButton {\n"
"    background-color: palegoldenrod;\n"
"    border-width: 2px;\n"
"    border-color: darkkhaki;\n"
"    border-style: solid;\n"
"    border-radius: 5;\n"
"    padding: 3px;\n"
"    min-width: 9ex;\n"
"    min-height: 2.5ex;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"   background-color: khaki;\n"
"}\n"
"\n"
"/* Increase the padding, so the text is shifted when the button is\n"
"   pressed. */\n"
"QPushButton:pressed {\n"
"    padding-left: 5px;\n"
"    padding-top: 5px;\n"
"    background-color: #d0d67c;\n"
"}\n"
"\n"
"QLabel, QAbstractButton {\n"
"    font: bold;\n"
"}\n"
"\n"
"/* Mark mandatory fields with a brownish color. */\n"
".mandatory {\n"
"    color: brown;\n"
""
                        "}\n"
"\n"
"/* Bold text on status bar looks awful. */\n"
"QStatusBar QLabel {\n"
"   font: normal;\n"
"}\n"
"\n"
"QStatusBar::item {\n"
"    border-width: 1;\n"
"    border-color: darkkhaki;\n"
"    border-style: solid;\n"
"    border-radius: 2;\n"
"}\n"
"\n"
"QComboBox, QLineEdit, QSpinBox, QTextEdit, QListView {\n"
"    background-color: cornsilk;\n"
"    selection-color: #0a214c; \n"
"    selection-background-color: #C19A6B;\n"
"}\n"
"\n"
"QListView {\n"
"    show-decoration-selected: 1;\n"
"}\n"
"\n"
"QTreeView::item:hover {\n"
"    background-color: wheat;\n"
"}\n"
"\n"
"/* We reserve 1 pixel space in padding. When we get the focus,\n"
"   we kill the padding and enlarge the border. This makes the items\n"
"   glow. */\n"
"QLineEdit, QFrame {\n"
"    border-width: 1px;\n"
"    padding: 1px;\n"
"    border-style: solid;\n"
"    border-color: darkkhaki;\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"/* As mentioned above, eliminate the padding and increase the border. */\n"
"QLineEdit:focus, QFrame:focus {\n"
""
                        "    border-width: 3px;\n"
"    padding: 0px;\n"
"}\n"
"\n"
"/* A QLabel is a QFrame ... */\n"
"QLabel {\n"
"    border: none;\n"
"    padding: 0;\n"
"    background: none;\n"
"}\n"
"\n"
"/* A QToolTip is a QLabel ... */\n"
"QToolTip {\n"
"    border: 2px solid darkkhaki;\n"
"    padding: 5px;\n"
"    border-radius: 3px;\n"
"    opacity: 200;\n"
"}\n"
"\n"
"/* Nice to have the background color change when hovered. */\n"
"QRadioButton:hover, QCheckBox:hover {\n"
"    background-color: wheat;\n"
"}\n"
"\n"
"/* Force the dialog's buttons to follow the Windows guidelines. */\n"
"QDialogButtonBox {\n"
"    button-layout: 0;\n"
"}\n"
"\n"
"\n"
""));

        verticalLayout_4->addWidget(assetTreeView);

        assetDock->setWidget(assetDockContents);
        GravityEditorClass->addDockWidget(static_cast<Qt::DockWidgetArea>(8), assetDock);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuActors->menuAction());
        menuBar->addAction(menuObjects->menuAction());
        menuFile->addAction(actionSave);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_Scene_2);
        menuFile->addAction(actionLoad_Scene);
        menuFile->addSeparator();
        menuActors->addAction(menuCamera->menuAction());
        menuActors->addAction(menuLights->menuAction());
        menuLights->addAction(actionPointLight);
        menuLights->addAction(actionDirectionalLight);
        menuLights->addAction(actionSpotLight);
        menuCamera->addAction(actionFPSCamera);
        menuCamera->addAction(actionStaticCamera);
        menuObjects->addAction(menuRender->menuAction());
        menuObjects->addAction(actionPhysics);
        menuObjects->addAction(actionParticles);
        menuObjects->addAction(actionSound);
        menuRender->addAction(actionMeshRender);

        retranslateUi(GravityEditorClass);

        QMetaObject::connectSlotsByName(GravityEditorClass);
    } // setupUi

    void retranslateUi(QMainWindow *GravityEditorClass)
    {
        GravityEditorClass->setWindowTitle(QApplication::translate("GravityEditorClass", "GravityEditor", 0));
        actionPhysics->setText(QApplication::translate("GravityEditorClass", "Physics", 0));
        actionFPSCamera->setText(QApplication::translate("GravityEditorClass", "FPSCamera", 0));
        actionStaticCamera->setText(QApplication::translate("GravityEditorClass", "Camera", 0));
        actionPointLight->setText(QApplication::translate("GravityEditorClass", "PointLight", 0));
        actionDirectionalLight->setText(QApplication::translate("GravityEditorClass", "DirectionalLight", 0));
        actionSpotLight->setText(QApplication::translate("GravityEditorClass", "SpotLight", 0));
        actionSave->setText(QApplication::translate("GravityEditorClass", "Save All", 0));
        actionSave_Scene->setText(QApplication::translate("GravityEditorClass", "Save Scene", 0));
        actionSave_Scene_2->setText(QApplication::translate("GravityEditorClass", "Save Scene", 0));
        actionLoad_Scene->setText(QApplication::translate("GravityEditorClass", "Load Scene", 0));
        actionMeshRender->setText(QApplication::translate("GravityEditorClass", "MeshRender", 0));
        actionParticles->setText(QApplication::translate("GravityEditorClass", "Particles", 0));
        actionSound->setText(QApplication::translate("GravityEditorClass", "Sound", 0));
#ifndef QT_NO_TOOLTIP
        menuBar->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        menuFile->setTitle(QApplication::translate("GravityEditorClass", "File", 0));
        menuEdit->setTitle(QApplication::translate("GravityEditorClass", "Edit", 0));
        menuView->setTitle(QApplication::translate("GravityEditorClass", "View", 0));
        menuActors->setTitle(QApplication::translate("GravityEditorClass", "Actors", 0));
        menuLights->setTitle(QApplication::translate("GravityEditorClass", "Lights", 0));
        menuCamera->setTitle(QApplication::translate("GravityEditorClass", "Camera", 0));
        menuObjects->setTitle(QApplication::translate("GravityEditorClass", "Components", 0));
        menuRender->setTitle(QApplication::translate("GravityEditorClass", "Render", 0));
        componentDock->setWindowTitle(QApplication::translate("GravityEditorClass", "Component View", 0));
        createComponentButton->setText(QApplication::translate("GravityEditorClass", "Create Componet", 0));
        actorDock->setWindowTitle(QApplication::translate("GravityEditorClass", "Actors", 0));
        createActorButton->setText(QApplication::translate("GravityEditorClass", "Create Actor", 0));
        assetDock->setWindowTitle(QApplication::translate("GravityEditorClass", "Assets", 0));
    } // retranslateUi

};

namespace Ui {
    class GravityEditorClass: public Ui_GravityEditorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRAVITYEDITOR_H
