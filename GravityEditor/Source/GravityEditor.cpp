#include "GravityEditor.h"

#include <QPixmap>
#include <QIcon>

#include <Windows.h>
#include <iostream>

#include <QResizeEvent>
#include <QPainter>
#include <QBitmap>
#include <QFileSystemModel>
#include <QDirModel>
#include "Actor/ActorTreeItem.h"
#include "Actor/ActorTreeModel.h"
#include <GPECore.h>
#include <Managers/Managers.h>

GravityEditor::GravityEditor(QWidget* parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
    ui.setupUi(this);
    
	mpGTWidget = new GTWidget(this);
	setCentralWidget(mpGTWidget);
    setWindowTitle("Gravity Editor");

	//mpApp = mpGTWidget->GetApp();
	//mpView = static_pointer_cast<GTEditorView, GT::ISystemView>(mpApp->GetView());
	//mpScene = mpView->mpManagers->GetSceneManager()->GetCurrentScene();

	//QDirModel* model = new QDirModel(ui.assetTreeView);
	//ui.assetTreeView->setModel(model);
	//ui.assetTreeView->setHeaderHidden(true);
	//
	//ActorTreeModel* actorModel = new ActorTreeModel(mpScene, ui.actorTreeView);
	//ui.actorTreeView->setModel(actorModel);
	//ui.actorTreeView->setHeaderHidden(true);
}

GravityEditor::~GravityEditor()
{
}

void GravityEditor::on_createActorButton_clicked()
{

}
