#ifndef GTWIDGET_H_
#define GTWIDGET_H_

//GT
#include <GravityTech/GravityTechApp.h>
#include <GPECore.h>
#include <Scene/BaseScene.h> 

// qt
#include <QWidget>
#include <QTimer>

//Forward declarations
class GTEditorApp;
class GTEditorView;
class EditorScene;

//Typedefinitions
typedef std::shared_ptr<GTEditorApp> GTEditorAppPtr; 
typedef std::shared_ptr<GTEditorView> GTEditorViewPtr; 
typedef std::shared_ptr<EditorScene> EditorScenePtr;

class GTWidget : public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(GTWidget)

public:
	GTWidget(QWidget* parent = NULL, Qt::WindowFlags flags = 0);
	virtual ~GTWidget();

	inline virtual QPaintEngine* paintEngine() const {return NULL;}

	GTEditorAppPtr GetApp() const;

protected:
	virtual void paintEvent(QPaintEvent* e);
	virtual void resizeEvent(QResizeEvent* e);
	virtual void mousePressEvent(QMouseEvent* e);
	virtual void mouseReleaseEvent(QMouseEvent* e);
	virtual void mouseMoveEvent(QMouseEvent* e);
    virtual void wheelEvent(QWheelEvent* e);
    virtual void keyPressEvent(QKeyEvent* e);

protected:
	QPoint m_prevMousePos;
	bool m_leftMouseDown;
	bool m_rightMouseDown;
	GTEditorAppPtr mpEditorApp;
};

class GTEditorApp : public GT::GravityTechApp
{
public:
	GTEditorApp();
	virtual bool Init(const UINT aWinID);

	GT::RenderManagerPtr GetRenderManager() const;
private:
	GT::RenderManagerPtr mpRenderManager; 
};

class GTEditorView : public GT::SystemView
{
public:
	virtual bool Init(const GTEditorAppPtr& apApp, const UINT aWinID);
	void Update();
	void Draw();
	GT::ManagersPtr mpManagers;
private:
	
};

class EditorScene : public GT::BaseScene
{
public:
	virtual bool Init();
	virtual void Update(double aDeltaTime, float aElapsedTime);
	virtual void Draw(double aDeltaTime, float aElapsedTime);
	virtual void Release();
};


#endif