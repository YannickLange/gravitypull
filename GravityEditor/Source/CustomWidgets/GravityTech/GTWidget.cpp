#include "GTWidget.h"

// qt
#include <QResizeEvent>

//GT
#include <Managers/Managers.h>
#include <Managers/RenderManager.h>
#include <../Scene.h>
#include <Graphics/Texture2D.h>

using namespace GT;

GTWidget::GTWidget(QWidget* parent /* = NULL */, Qt::WindowFlags flags /* = 0 */)
	: QWidget(parent, flags),
	    m_leftMouseDown(false),
	    m_rightMouseDown(false)
{
    //m_renderer = factory->create(winId());
	mpEditorApp.reset(new GTEditorApp);
	mpEditorApp->mWindowHandle = (HWND) winId();
	mpEditorApp->Init(winId());

    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_NativeWindow, true);

    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(0);

    setFocus();
}

GTWidget::~GTWidget()
{
}

void GTWidget::paintEvent(QPaintEvent* event)
{
	//m_renderer->frame();
	GTTime time;
	std::static_pointer_cast<GTEditorView, ISystemView>(mpEditorApp->GetView())->Update(); //TODO
}

GTEditorAppPtr GTWidget::GetApp() const
{
	return mpEditorApp;
}

void GTWidget::resizeEvent(QResizeEvent* event)
{
	std::static_pointer_cast<GTEditorView, ISystemView>(mpEditorApp->GetView())->mpManagers->GetDeviceManager()->SetViewport(width(), height());
}

void GTWidget::mousePressEvent(QMouseEvent* e)
{
    setFocus();

	if (e->button() == Qt::LeftButton)
	{
		m_leftMouseDown = true;
	} 
	else if (e->button() == Qt::RightButton)
	{
		m_rightMouseDown = true;
	}

    m_prevMousePos = e->pos();
}

void GTWidget::mouseReleaseEvent(QMouseEvent* e)
{
	if (e->button() == Qt::LeftButton)
	{
		m_leftMouseDown = false;
	}

	if (e->button() == Qt::RightButton)
	{
		m_rightMouseDown = false;
	}
}

void GTWidget::mouseMoveEvent(QMouseEvent* e)
{
    m_prevMousePos = e->pos();
}

void GTWidget::wheelEvent(QWheelEvent* e)
{

}

void GTWidget::keyPressEvent(QKeyEvent* e)
{

}

GTEditorApp::GTEditorApp() :
	GravityTechApp()
{

}

bool GTEditorApp::Init(const UINT aWinID)
{
	mpView.reset(new GTEditorView()); 
	std::static_pointer_cast<GTEditorView, ISystemView>(mpView)->Init(std::static_pointer_cast<GTEditorApp, GravityTechApp>(GetThis()), aWinID);

	return GravityTechApp::Init();
}

bool GTEditorView::Init(const GTEditorAppPtr& apApp, const UINT aWinID)
{
	bool result = true;

	mpManagers.reset(new Managers());
	result = mpManagers->Init();
	if(!result) return result;

	result = mpManagers->GetDeviceManager()->Init(1280, 720, true, apApp->mWindowHandle);
	if(!result) return result;

	result = mpManagers->GetGeometryManager()->Init(mpManagers->GetDeviceManager());
	if(!result) return result;

	result = mpManagers->GetRenderManager()->Init(mpManagers);
	if(!result) return result;

	result = mpManagers->GetSceneManager()->Init(mpManagers);
	if(!result) return result;

	/*result = mpManagers->GetInputManager()->Init(apApp->mInstance, apApp->mWindowHandle);
	if(!result) return result;

	result = mpManagers->GetSoundManager()->Init(apApp->mWindowHandle);
	if(!result) return result;*/

	//Finally initialize the scene
	EditorScenePtr scene(new EditorScene);
	mpManagers->GetSceneManager()->SetCurrentScene(scene);

	return true;
}

void GTEditorView::Update()
{
	mpManagers->GetSceneManager()->GetCurrentScene()->Update(0.0f, 0.0f);
	Draw();
}

void GTEditorView::Draw()
{
	mpManagers->GetDeviceManager()->Clear(Color(1.0f, 0.5f, 0.5f, 1.0f));
	mpManagers->GetSceneManager()->GetCurrentScene()->Draw(0.0f, 0.0f);
	mpManagers->GetDeviceManager()->Present();
}

bool EditorScene::Init()
{
	BaseScene::Init();
	SetSkybox(L"..\\GravityTech\\Assets\\Textures\\skymap.dds");

	Texture2DPtr texture2(new Texture2D);
	texture2->Init(L"Assets\\T-90\\Main Body.png", GetDeviceManager());

	BaseMaterialPtr material2(new BaseMaterial());
	material2->Init(GetDeviceManager(), GetRenderManager()->GetEffect(L"../GravityTech/Assets/Effects/Basic.fxo"));
		
	TextureMaterialPtr texturematerial(new TextureMaterial);
	texturematerial->Init(GetDeviceManager(), GetRenderManager()->GetEffect(L"../GravityTech/Assets/Effects/Texture.fxo"));
	texturematerial->SetTexture<ID3D11ShaderResourceView*>(texture2->GetShaderResourceView());
	texturematerial->SetSampleType<ID3D11SamplerState*>(texture2->GetSamplerState());

	BaseActorPtr p6 = CreateActor(L"Goat4");
	p6->GetTransform()->SetPosition(10, 0, 20);
	p6->GetTransform()->SetRotation(0, 1, 0, 90);
	p6->AddComponent(GetRenderManager()->GetMeshRender(L"Assets\\T-90\\T-90.obj", texturematerial));

	BaseActorPtr p7 = CreateActor(L"Tank2");
	BaseActorPtr p8 = CreateActor(L"Tank3");
	BaseActorPtr p81 = CreateActor(L"Tank4", p8);
	BaseActorPtr p811 = CreateActor(L"Tank3", p81);
	BaseActorPtr p9 = CreateActor(L"Tank3");

	return true;
}

void EditorScene::Update(double aDeltaTime, float aElapsedTime)
{
	BaseScene::Update(aDeltaTime, aElapsedTime);
}

void EditorScene::Draw(double aDeltaTime, float aElapsedTIme)
{
	BaseScene::Draw(aDeltaTime, aElapsedTIme);
}

void EditorScene::Release()
{
	BaseScene::Release();
}