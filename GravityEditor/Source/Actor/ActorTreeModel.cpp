#include "ActorTreeModel.h"
#include "ActorTreeItem.h"
#include <Actors/BaseActor.h>

ActorTreeModel::ActorTreeModel(const GT::BaseActorPtr& apRootActor, QObject* apParent) :
	QAbstractItemModel(apParent)
{
	mpRootItem = new ActorTreeItem(QString("Name"));
	setupModelData(apRootActor, mpRootItem);
}

ActorTreeModel::~ActorTreeModel()
{
	delete mpRootItem;
}

QVariant ActorTreeModel::data(const QModelIndex &aIndex, int aRole) const
{
	 if (!aIndex.isValid())
	 {
         return QVariant();
	 }

     if (aRole != Qt::DisplayRole)
	 {
         return QVariant();
	 }

     ActorTreeItem *item = static_cast<ActorTreeItem*>(aIndex.internalPointer());

	 return item->data(aIndex.column());
}

Qt::ItemFlags ActorTreeModel::flags(const QModelIndex &aIndex) const
{
	 if (!aIndex.isValid())
	 {
         return 0;
	 }

     return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant ActorTreeModel::headerData(int aSection, Qt::Orientation aOrientation, int aRole) const
{
	 if (aOrientation == Qt::Horizontal && aRole == Qt::DisplayRole)
	 {
		 return mpRootItem->data(aSection);
	 }

     return QVariant();
}

QModelIndex ActorTreeModel::index(int aRow, int aColumn, const QModelIndex &apParent) const
{
	if (!hasIndex(aRow, aColumn, apParent))
	{
         return QModelIndex();
	}

	ActorTreeItem *parentItem;

     if (!apParent.isValid())
	 {
         parentItem = mpRootItem;
	 }
     else
	 {
         parentItem = static_cast<ActorTreeItem*>(apParent.internalPointer());
	 }

	 ActorTreeItem *childItem = parentItem->child(aRow);
     if (childItem)
	 {
         return createIndex(aRow, aColumn, childItem);
	 }
     else
	 {
         return QModelIndex();
	 }
}

QModelIndex ActorTreeModel::parent(const QModelIndex &aIndex) const
{
	if (!aIndex.isValid())
	{
         return QModelIndex();
	}

     ActorTreeItem *childItem = static_cast<ActorTreeItem*>(aIndex.internalPointer());
	 ActorTreeItem *parentItem = childItem->parent();

     if (parentItem == mpRootItem)
	 {
         return QModelIndex();
	 }

	 return createIndex(parentItem->row(), 0, parentItem);
}

int ActorTreeModel::rowCount(const QModelIndex &apParent) const
{
	 ActorTreeItem *parentItem;
     if (apParent.column() > 0)
	 {
         return 0;
	 }

     if (!apParent.isValid())
	 {
		 parentItem = mpRootItem;
	 }
     else
	 {
         parentItem = static_cast<ActorTreeItem*>(apParent.internalPointer());
	 }

	 return parentItem->childCount();
}

int ActorTreeModel::columnCount(const QModelIndex &apParent) const
{
	if (apParent.isValid())
	{
		return static_cast<ActorTreeItem*>(apParent.internalPointer())->columnCount();
	}
	else
	{
		return mpRootItem->columnCount();
	}
}

void ActorTreeModel::setupModelData(const GT::BaseActorPtr& apParentActor, ActorTreeItem *apParent)
{
	GT::BaseActorPtrVec childs = apParentActor->GetChilds();
	if(childs.size() <= 0)
	{
		return;
	}

	for(GT::BaseActorPtrVec::iterator it = childs.begin(); it != childs.end(); it++)
	{
		ActorTreeItem* item = new ActorTreeItem(QString::fromStdWString(it->get()->GetName()), apParent);
		apParent->appendChild(item);
		setupModelData(*it, item);
	}
}
