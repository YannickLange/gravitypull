#include "ActorTreeItem.h"

ActorTreeItem::ActorTreeItem(const QString aData, ActorTreeItem* apParent) :
	mItemData(aData),
	mpParentItem(apParent)
{

}

ActorTreeItem::~ActorTreeItem()
{
	qDeleteAll(mChildItems);
}

void ActorTreeItem::appendChild(ActorTreeItem *apItem)
{
	mChildItems.append(apItem);
}

ActorTreeItem* ActorTreeItem::child(int aRow)
{
	return mChildItems.value(aRow);
}

int ActorTreeItem::childCount() const
{
	return mChildItems.count();
}

int ActorTreeItem::columnCount() const
{
	return 1;
}

QVariant ActorTreeItem::data(int aColumn) const
{
	return mItemData;
}

int ActorTreeItem::row() const
{
	if(mpParentItem)
	{
		return mpParentItem->mChildItems.indexOf(const_cast<ActorTreeItem*>(this));
	}

	return 0;
}

ActorTreeItem* ActorTreeItem::parent()
{
	return mpParentItem;
}
