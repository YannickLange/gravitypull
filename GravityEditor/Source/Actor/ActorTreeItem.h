#ifndef ACTORTREEITEM_H_
#define ACTORTREEITEM_H_

#include <QTCore>

class ActorTreeItem
{
public:
    ActorTreeItem(const QString aData, ActorTreeItem* apParent = 0);
    ~ActorTreeItem();

    void appendChild(ActorTreeItem *apItem);

    ActorTreeItem* child(int aRow);
    int childCount() const;
    int columnCount() const;
    QVariant data(int aColumn) const;
    int row() const;
    ActorTreeItem* parent();

private:
     QList<ActorTreeItem*> mChildItems;
     QString mItemData;
     ActorTreeItem *mpParentItem;
};

#endif