#ifndef ACTORTREEMODEL_H_
#define ACTORTREEMODEL_H_

#include <QAbstractItemModel>
#include <GPECore.h>

class ActorTreeItem;

class ActorTreeModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	ActorTreeModel(const GT::BaseActorPtr& apRootActor, QObject* apParent = 0);
	~ActorTreeModel();

	QVariant data(const QModelIndex &aIndex, int aRole) const;
	Qt::ItemFlags flags(const QModelIndex &aIndex) const;
	QVariant headerData(int aSection, Qt::Orientation aOrientation, int aRole = Qt::DisplayRole) const;
	QModelIndex index(int aRow, int aColumn, const QModelIndex &apParent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &aIndex) const;
	int rowCount(const QModelIndex &apParent = QModelIndex()) const;
	int columnCount(const QModelIndex &apParent = QModelIndex()) const;

 private:
     void setupModelData(const GT::BaseActorPtr& apParentActor, ActorTreeItem *apParent);

     ActorTreeItem *mpRootItem;
};

#endif