#ifndef GRAVITYEDITOR_H_
#define GRAVITYEDITOR_H_

#include "ui_gravityeditor.h"
#include "CustomWidgets/GravityTech/GTWidget.h"
#include <QTreeView>

class GravityEditor : public QMainWindow
{
	Q_OBJECT

public:
	GravityEditor(QWidget* parent = 0, Qt::WindowFlags flags = 0);
	virtual ~GravityEditor();
protected:
	GTWidget* mpGTWidget;
	Ui::GravityEditorClass ui;

	GTEditorAppPtr mpApp;
	GTEditorViewPtr mpView;
	EditorScenePtr mpScene;

	//Slots
	private slots:
		void on_createActorButton_clicked();
};

#endif