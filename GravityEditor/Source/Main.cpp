// qt
#include <QtWidgets/QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QLabel>

//own
#include "GravityEditor.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	// load the system translations provided by Qt
    QTranslator qtTranslator;
	qtTranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

	GravityEditor* editor = new GravityEditor();
	editor->show();

	return app.exec();
}
