#include "GameScene.h"
#include <Managers/Managers.h>
#include <Graphics/Texture2D.h>
#include <Actors/Components/2D/SpriteRender.h>
#include <Actors/Components/Sound/SoundPlayer.h>
#include <Actors/Components/Sound/Sound.h>
#include "SecondGameScene.h"

namespace GravityPull
{
	bool GameScene::Init()
	{
		i = 0;
		BaseScene::Init();
		SetSkybox(L"Assets\\Textures\\skymap.dds");
		
		Texture2DPtr texture(new Texture2D);
		texture->Init(L"Assets\\Goat.dds", GetDeviceManager());

		Texture2DPtr pSpriteTexture(new Texture2D);
		pSpriteTexture->Init(L"Assets\\decal.dds", GetDeviceManager());

		Texture2DPtr texture2(new Texture2D);
		texture2->Init(L"Assets\\T-90\\Main Body.png", GetDeviceManager());

		Texture2DPtr bodyTexture(new Texture2D);
		bodyTexture->Init(L"Assets\\T-90\\Main Body.png", GetDeviceManager());

		Texture2DPtr turretTexture(new Texture2D);
		turretTexture->Init(L"Assets\\T-90\\Turret.png", GetDeviceManager());

		Texture2DPtr gunTexture(new Texture2D);
		gunTexture->Init(L"Assets\\T-90\\Gun.png", GetDeviceManager());

		TextureMaterialPtr texturematerial(new TextureMaterial);
		texturematerial->Init(GetDeviceManager(), GetRenderManager()->GetEffect(L"../GravityTech/Assets/Effects/Texture.fxo"));
		texturematerial->SetTexture<ID3D11ShaderResourceView*>(texture2->GetShaderResourceView());
		texturematerial->SetSampleType<ID3D11SamplerState*>(texture2->GetSamplerState());
		
		SpriteMaterialPtr pSpriteMaterial(new SpriteMaterial);
		pSpriteMaterial->Init(GetDeviceManager(), GetRenderManager()->GetEffect(L"../GravityTech/Assets/Effects/Sprite.fxo"));
		pSpriteMaterial->SetTextureMap<ID3D11ShaderResourceView*>(pSpriteTexture->GetShaderResourceView());
		pSpriteMaterial->SetTextureSample<ID3D11SamplerState*>(pSpriteTexture->GetSamplerState());

		BaseActorPtr p6 = CreateActor(L"Goat4");
		p6->GetTransform()->SetPosition(10, 0, 60);
		p6->GetTransform()->SetRotation(0, 1, 0, 90);
		p6->AddComponent(GetRenderManager()->GetMeshRender(L"Assets\\T-90\\T-90.obj", texturematerial));

		mpTestSprite.reset(new SpriteRender);
		mpTestSprite->Init(pSpriteMaterial, GetDeviceManager());

		return true;
	}

	void GameScene::Update(double aDeltaTime, float aElapsedTime)
	{
		BaseScene::Update(aDeltaTime, aElapsedTime);

		if(GetInputManager()->ButtonDown(KeyBoardKey::KEY_1))
		{
			i += 10 * aDeltaTime;
			p->GetTransform()->SetRotation(1, 0, 0, i);
		}
		else if(GetInputManager()->ButtonDown(KeyBoardKey::KEY_2))
		{
			i -= 10 * aDeltaTime;
			p->GetTransform()->SetRotation(1, 0, 0, i);
		}

		if(GetInputManager()->ButtonPressed(KeyBoardKey::KEY_9))
		{
			GetSceneManager()->SetCurrentScene(BaseScenePtr(new SecondGameScene));
		}
	}

	void GameScene::Draw(double aDeltaTime, float aElapsedTIme)
	{
		BaseScene::Draw(aDeltaTime, aElapsedTIme);

		mpTestSprite->Draw(mOrthoGraphicMatrix, GetDeviceManager());
	}

	void GameScene::Release()
	{
		BaseScene::Release();
	}
};