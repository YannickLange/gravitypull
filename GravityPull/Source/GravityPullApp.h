#ifndef GRAVITYPULLAPP_H_
#define GRAVITYPULLAPP_H_

#include <GravityTech/GravityTechApp.h>

using namespace GT;

namespace GravityPull
{
	class GravityPullApp;
	typedef std::shared_ptr<GravityPullApp> GravityPullAppPtr;

	class GravityPullApp : public GravityTechApp
	{
	public:
		GravityPullApp();
		virtual bool Init();
	};
};
#endif