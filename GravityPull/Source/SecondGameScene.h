#ifndef SECONDGAMESCENE_H_
#define SECONDGAMESCENE_H_

#include <Scene/BaseScene.h>
using namespace GT;

namespace GravityPull
{
	class SecondGameScene : public BaseScene
	{
	public:
		virtual bool Init();
		virtual void Update(double aDeltaTime, float aElapsedTime);
		virtual void Draw(double aDeltaTime, float aElapsedTime);
		virtual void Release();
	};
};

#endif