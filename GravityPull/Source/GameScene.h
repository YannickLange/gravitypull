#ifndef GAMESCENE_H_
#define GAMESCENE_H_

#include <Scene/BaseScene.h>
using namespace GT;

namespace GravityPull
{
	class GameScene;
	typedef std::shared_ptr<GameScene> GameScenePtr;

	class GameScene : public BaseScene
	{
	public:
		virtual bool Init();
		virtual void Update(double aDeltaTime, float aElapsedTime);
		virtual void Draw(double aDeltaTime, float aElapsedTime);
		virtual void Release();
	private:
		BaseActorPtr p;
		SoundPlayerPtr mpSound, mpSound2;
		float i;
		SpriteRenderPtr mpTestSprite;
	};
};

#endif