#include "GravityPullApp.h"
#include "GameScene.h"

namespace GravityPull
{
	GravityPullApp::GravityPullApp() :
		GravityTechApp()
	{

	}

	bool GravityPullApp::Init()
	{
		bool result = true;
		mAppName = L"GravityPull";
	
		Utils::CreateGTWindow(mWindowHandle, L"GravityPull", mInstance, mShowCmd, 1280, 720);
	
		mpView.reset(new GT::HumanView()); 
		GameScenePtr scene(new GameScene());
		result = std::static_pointer_cast<HumanView, ISystemView>(mpView)->Init(GetThis(), scene);

		result = GravityTechApp::Init();

		return result;
	};
};