#include "GravityPullApp.h"

using namespace GravityPull;

//--------------------------------------------------------------------------------------
// Entry point of GravityPull and initialize gamengine
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE aInstance, HINSTANCE aPrevInstance, LPWSTR aCmdLine, int aShowCmd )
{
	GravityPullAppPtr app(new GravityPullApp);

	app->SetWinMainValues(aInstance, aPrevInstance, aCmdLine, aShowCmd);
	app->Init();
	app->Run();
	app->Destroy();

	return 0;
}