#include "SecondGameScene.h"
#include <Managers/Managers.h>
#include <Graphics/Texture2D.h>
#include <Actors/Components/2D/SpriteRender.h>
#include <Actors/Components/Sound/SoundPlayer.h>
#include <Actors/Components/Sound/Sound.h>
#include "GameScene.h"

namespace GravityPull
{
	bool SecondGameScene::Init()
	{
		BaseScene::Init();

		return true;
	}

	void SecondGameScene::Update(double aDeltaTime, float aElapsedTime)
	{
		BaseScene::Update(aDeltaTime, aElapsedTime);

		if(GetInputManager()->ButtonPressed(KeyBoardKey::KEY_9))
		{
			GetSceneManager()->SetCurrentScene(BaseScenePtr(new GameScene));
		}
	}
	

	void SecondGameScene::Draw(double aDeltaTime, float aElapsedTIme)
	{
		BaseScene::Draw(aDeltaTime, aElapsedTIme);
	}

	void SecondGameScene::Release()
	{

	}
};