/************* Resources *************/
cbuffer CBufferPerObject
{
	float4x4 WorldViewProjection : WORLDVIEWPROJECTION;

};

Texture2D Texture : TEXTURE;
SamplerState SampleType : SAMPLETYPE;

/************* Data Structures *************/
struct BASIC_VS_INPUT
{
    float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct BASIC_PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};


/************* Vertex Shader *************/
BASIC_PS_INPUT TextureVertexShader(BASIC_VS_INPUT input)
{
    BASIC_PS_INPUT output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.Position = mul(float4(input.Position, 1.0f), WorldViewProjection);
	output.TexCoord = input.TexCoord;

    return output;
}

/************* Pixel Shader *************/
float4 TexturePixelShader(BASIC_PS_INPUT input) : SV_TARGET
{
	float4 output = mul(float4(1.0f, 0.0f, 0.0f, 0.0f), Texture.Sample(SampleType, input.TexCoord));
    return output;
}

/************* Techniques *************/
technique11 BaseTechnique 
{ 
	pass P0 
	{ 
		SetVertexShader(CompileShader(vs_5_0, TextureVertexShader())); 
		SetPixelShader(CompileShader(ps_5_0, TexturePixelShader())); 
	} 
}
