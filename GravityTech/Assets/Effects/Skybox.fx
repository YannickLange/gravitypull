/************* Resources *************/
cbuffer CBufferPerObject
{
	float4x4 WorldViewProjection : WORLDVIEWPROJECTION;
	float4x4 World : WORLD;
};
float4 Color;
TextureCube SkyboxTextureCube;

SamplerState Sampler
{
	Filter = MIN_MAG_MIP_LINEAR;
};

RasterizerState DisableCulling
{
	CullMode = NONE;
};

/************* Data Structures *************/
struct VS_INPUT
{
    float3 Position : POSITION;
};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float3 TexCoord : TEXCOORD0;
};


/************* Vertex Shader *************/
PS_INPUT SkyboxVertexShader(VS_INPUT input)
{
    PS_INPUT output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.Position = mul(float4(input.Position, 1.0f), WorldViewProjection).xyww;
	output.TexCoord = input.Position;

    return output;
}

/************* Pixel Shader *************/
float4 SkyboxPixelShader(PS_INPUT input) : SV_TARGET
{
	return SkyboxTextureCube.Sample(Sampler, input.TexCoord);
}

/************* Techniques *************/
technique11 SkyboxTechnique 
{ 
	pass P0 
	{ 
		SetVertexShader(CompileShader(vs_5_0, SkyboxVertexShader())); 
		SetPixelShader(CompileShader(ps_5_0, SkyboxPixelShader())); 
		SetRasterizerState(DisableCulling);
	} 
}
