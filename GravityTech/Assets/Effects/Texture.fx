/************* Resources *************/
cbuffer ObjectTransform
{
	float4x4 WorldViewProjection : WORLDVIEWPROJECTION;
	float4x4 World : WORLD;
};

float4 Color = float4(1.0f, 0.0f, 0.0f, 1.0f);
Texture2D Texture : TEXTURE;
SamplerState SampleType : SAMPLETYPE;

float4 AmbientDown = float4(0.0f, 0.0f, 0.0f, 1.0f);
float4 AmbientUp = float4(0.9f, 0.9f, 0.9f, 1.0f);

/************* Data Structures *************/
struct BASIC_VS_INPUT
{
    float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
};

struct BASIC_PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 Normal : NORMAL;
};

float4 CalcAmbient(float3 normal, float4 color)
{
	//Convert from [-1, 1] to [0, 1]
	float up = normal.y * 0.5 + 0.5;

	//Calculate the ambient value
	float4 ambient = AmbientDown + up * AmbientUp;
	
	//Apply the ambient to the color
	return ambient * color;
}

/************* Vertex Shader *************/
BASIC_PS_INPUT TextureVertexShader(BASIC_VS_INPUT input)
{
    BASIC_PS_INPUT output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.Position = mul(float4(input.Position, 1.0f), WorldViewProjection);
	output.TexCoord = input.TexCoord;

	//Transform the normal from object to world space
	output.Normal = mul(input.Normal, (float3x3)World);

    return output;
}

/************* Pixel Shader *************/
float4 TexturePixelShader(BASIC_PS_INPUT input) : SV_TARGET
{
	return CalcAmbient(input.Normal, Texture.Sample(SampleType, input.TexCoord));
	//float4 output = Texture.Sample(SampleType, input.TexCoord);
	//output.rgb *= AmbientDown.rgb * AmbientDown.a;
	//return output;
}

/************* Techniques *************/
technique11 BaseTechnique 
{ 
	pass P0 
	{ 
		SetVertexShader(CompileShader(vs_5_0, TextureVertexShader())); 
		SetPixelShader(CompileShader(ps_5_0, TexturePixelShader()));
	} 
}
