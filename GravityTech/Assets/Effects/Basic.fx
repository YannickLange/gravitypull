/************* Resources *************/
cbuffer CBufferPerObject
{
	float4x4 WorldViewProjection : WORLDVIEWPROJECTION;
	float4x4 World : WORLD;
};

float4 Color;/* = float4(1.0f, 0.0f, 0.0f, 1.0f);*/

/************* Data Structures *************/
struct BASIC_VS_INPUT
{
    float3 Position : POSITION;
};

struct BASIC_PS_INPUT
{
    float4 Position : SV_POSITION;
};

RasterizerState Culling
{
	CullMode = BACK;
};

/************* Vertex Shader *************/
BASIC_PS_INPUT BasicVertexShader(BASIC_VS_INPUT input)
{
    BASIC_PS_INPUT output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.Position = mul(float4(input.Position, 1.0f), WorldViewProjection);

    return output;
}

/************* Pixel Shader *************/
float4 BasicPixelShader(BASIC_PS_INPUT input) : SV_TARGET
{
    return Color;
}

/************* Techniques *************/
technique11 BaseTechnique 
{ 
	pass P0 
	{ 
		SetVertexShader(CompileShader(vs_5_0, BasicVertexShader())); 
		SetPixelShader(CompileShader(ps_5_0, BasicPixelShader())); 
		SetRasterizerState(Culling);
	} 
}
