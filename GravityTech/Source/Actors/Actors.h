#ifndef ACTORS_H_
#define ACTORS_H_

#include <memory>
#include <vector>
using namespace std;

#include "Actors/BaseActor.h"

//Camera
#include "Actors/CommonActors/Camera/BaseCamera.h"
#include "Actors/CommonActors/Camera/FirstPersonCamera.h"


namespace GT
{
	typedef shared_ptr<BaseCamera> BaseCameraPtr;
	typedef shared_ptr<FirstPersonCamera> FirstPersonCameraPtr;
};

#endif