#include "PlaneMesh.h"

namespace GT
{
	PlaneMesh::PlaneMesh() :
		BaseGeometry()
	{
		mIndexCount = 6;
		mVertexCount = 4;
		mTriangleCount = 2;
	}

	PlaneMesh::~PlaneMesh()
	{

	}

	bool PlaneMesh::Init(ID3D11Device* apD3D11Device, Vector2 aHalfSize)
	{
		HRESULT result;
		Vertex* vertices = new Vertex[mVertexCount];
		UINT* indices = new UINT[mIndexCount];

		if(aHalfSize.x == 0.0f)
		{
			aHalfSize.x = 1.0f;
		}
		
		if(aHalfSize.y == 0.0f)
		{
			aHalfSize.y = 1.0f;
		}

		float hh = aHalfSize.x;	//Half height
		float hw = aHalfSize.y;	//Half width
		
		vertices[0] = Vertex(Vector3(hw, hh, 1.0f), Vector2(1.0f, 0.0f), Vector3(0.0f));		//Right bottom
		vertices[1] = Vertex(Vector3(hw, -hh, 1.0f), Vector2(1.0f, 1.0f), Vector3(0.0f));	//Right top
		vertices[2] = Vertex(Vector3(-hw, hh, 1.0f), Vector2(0.0f, 0.0f), Vector3(0.0f));	//Left bottom
		vertices[3] = Vertex(Vector3(-hw, -hh, 1.0f), Vector2(0.0f, 1.0f), Vector3(0.0f));	//Left top

		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;
		indices[3] = 3;
		indices[4] = 2;
		indices[5] = 1;

		//Initialize the index and vertex buffer
		InitVertexBuffer<Vertex>(&mpVertexBuffer, vertices, mVertexCount, apD3D11Device);
		InitIndexBuffer(indices, apD3D11Device);

		// Release the arrays now that the vertex and index buffers have been created and loaded.
		SAFE_ARRAY_DELETE(vertices);
		SAFE_ARRAY_DELETE(indices);

		return true;
	}

};