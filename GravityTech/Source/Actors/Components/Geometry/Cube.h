#ifndef CUBE_H_
#define CUBE_H_

#include "BaseGeometry.h"

namespace GPECore
{
	class Cube : public BaseGeometry
	{
	public:
		Cube();
		virtual ~Cube();
	};
};

#endif