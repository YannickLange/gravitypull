#ifndef SPHERE_H_
#define SPHERE_H_

#include "BaseGeometry.h"

namespace GPECore
{
	class Sphere : public BaseGeometry
	{
	public:
		Sphere();
		virtual ~Sphere();
	};
};

#endif