#include "BaseGeometry.h"

namespace GT
{
	BaseGeometry::BaseGeometry() :
		BaseComponent(ComponentType::GEOMETRY),
		mpVertexBuffer(NULL),
		mpIndexBuffer(NULL),
		mOrigin(Vector3(.0f)),
		mRadius(.0f),
		mIndexCount(0),
		mVertexCount(0),
		mTriangleCount(0)
	{
		mBoundingBoxA = Vector3(0.0f, 0.0f, 0.0f);
		mBoundingBoxB = mBoundingBoxA;
	}

	BaseGeometry::~BaseGeometry()
	{
		mIndexCount = 0;
		mVertexCount = 0;
		mTriangleCount = 0;

		SAFE_RELEASE(mpVertexBuffer);
		SAFE_RELEASE(mpIndexBuffer);
	}

	bool BaseGeometry::Init() 
	{
		return true;
	}

	void BaseGeometry::PreDraw(ID3D11DeviceContext* apD3D11DeviceContext)
	{
		UINT stride, offset;

		// Set vertex buffer stride and offset.
		stride = sizeof(Vertex); 
		offset = 0;

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		apD3D11DeviceContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);

		// Set the index buffer to active in the input assembler so it can be rendered.
		apD3D11DeviceContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
		apD3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}

	const UINT BaseGeometry::GetIndexCount() const
	{
		return mIndexCount;
	}

	const UINT BaseGeometry::GetVertexCount() const 
	{
		return mVertexCount;
	}

	const UINT BaseGeometry::GetTriangleCount() const
	{
		return mTriangleCount;
	}

	const FLOAT BaseGeometry::GetRadius() const
	{
		return mRadius;
	}

	const Vector3 BaseGeometry::GetOrigin() const
	{
		return mOrigin;
	}
	
	bool BaseGeometry::InitIndexBuffer(UINT*& aIndices, ID3D11Device* apD3D11Device, const D3D11_USAGE& aBufferUsage)
	{
		HRESULT result;
		D3D11_BUFFER_DESC bufferDesc;
		D3D11_SUBRESOURCE_DATA data;

		// Set up the description of the static index buffer.
		bufferDesc.Usage = aBufferUsage;
		bufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
		bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bufferDesc.CPUAccessFlags = 0;
		bufferDesc.MiscFlags = 0;
		bufferDesc.StructureByteStride = 0;

		// Give the subresource structure a pointer to the index data.
		data.pSysMem = aIndices;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		// Create the index buffer.
		result = apD3D11Device->CreateBuffer(&bufferDesc, &data, &mpIndexBuffer);
		if(FAILED(result))
		{
			return false;
		}

		return true;
	}
};