#include "Mesh.h"

#include "Managers/DeviceManager.h"
#include "Actors/Components/ComponentTypes.h"
#include "Actors/Components/BaseComponent.h"

#include <assimp/Importer.hpp>
#include <assimp/PostProcess.h>
#include <assimp/Scene.h>
#include <assimp/DefaultLogger.hpp>
#include <assimp/LogStream.hpp>

namespace GT
{
	Mesh::Mesh() :
		BaseGeometry(),
		mMaxBBPoint(Vector3(.0f)),
		mMinBBpoint(Vector3(.0f))
	{
	}

	Mesh::~Mesh()
	{

	}

	bool Mesh::Init(std::wstring aFile, ID3D11Device* apD3D11Device, bool abFlipUVs)
	{
		Assimp::Importer importer;
		
		//Flags for loading the scene
		UINT flags = aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | 
			aiProcess_SortByPType | aiProcess_FlipWindingOrder;
		if (abFlipUVs)
        {
            flags |= aiProcess_FlipUVs;
        }

		const aiScene* scene = importer.ReadFile(WStringToString(aFile), flags);
		if(scene == NULL)
		{
			throw GTException(importer.GetErrorString());
		}

		//TODO materials

		if(scene->HasMeshes())
		{
			mMeshParts.reserve(scene->mNumMeshes);
			for(UINT i = 0; i < scene->mNumMeshes; i++)
			{
				MeshPartPtr mesh(new MeshPart);
				if(mesh->Init(GetThis(), scene->mMeshes[i], apD3D11Device))
				{
					mMeshParts.push_back(mesh);
				}
				else
				{
					PRINT_WARNING("MeshPart didn't load properly and is not included");
				}
			}
		}

		Vertex* vertices;
		UINT* indices;
		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
		D3D11_SUBRESOURCE_DATA vertexData, indexData;
		HRESULT result;

		for(MeshPartPtrVec::iterator it = mMeshParts.begin(); it != mMeshParts.end(); it++)
		{
			mVertexCount += it->get()->GetVertices().size();
			mIndexCount += it->get()->GetIndices().size();
		}

		//vertices = new Vertex[mVertexCount];
		//indices = new UINT[mIndexCount];

		UINT index = 0;
		UINT startIndex = 0;
		for(MeshPartPtrVec::iterator it = mMeshParts.begin(); it != mMeshParts.end(); it++)
		{
			MeshPart* mesh = it->get();
			Vertex* meshVertices = new Vertex[mesh->GetVertices().size()];
			for (UINT i = 0 ; i < mesh->GetVertices().size() ; i++) 
			{
				
				UINT j = index + i;
				//vertices[j].mPos = mesh->GetVertices().at(i);
				//vertices[j].mNormal = Vector3(0, 0, 0);
				meshVertices[i].mPos = mesh->GetVertices().at(i);
				
				
				//Check if this is the lowest vertex for the bounding box
				//TODO

				//Check if this is the highest vertex for the bounding box
				//TODO

				Vector3 tex = mesh->GetTextureCoords().at(0)->at(i);
				//vertices[j].mTexture = Vector2(tex.x, tex.y);
				meshVertices[i].mTexture = Vector2(tex.x, tex.y);
			}

			for (UINT i = 0 ; i < mesh->GetNormals().size() ; i++) 
			{
				meshVertices[i].mNormal = mesh->GetNormals().at(i);
			}
			
			InitVertexBuffer<Vertex>(&mesh->mpVertexBuffer, meshVertices, mesh->GetVertices().size(), apD3D11Device);
			SAFE_ARRAY_DELETE(meshVertices);

			mesh->SetStartIndex(startIndex);

			/*for (UINT i = 0; i < mesh->GetIndices().size(); i++)
			{
				indices[startIndex] = mesh->GetIndices().at(i);
				startIndex++;
			}*/

			index += mesh->GetVertices().size();
		}

		//InitVertexBuffer(&mpVertexBuffer, vertices, mVertexCount, apD3D11Device);
		//InitIndexBuffer(indices, apD3D11Device);

		// Release the arrays now that the vertex and index buffers have been created and loaded.
		//SAFE_ARRAY_DELETE(vertices);
		//SAFE_ARRAY_DELETE(indices);

		return true;
		//return LoadModel(aFile, apD3D11Device);
	}

	Vector3 Mesh::GetMaxBoundingBox()
	{
		return mMaxBBPoint;
	}

	Vector3 Mesh::GetMinBoundingBox()
	{
		return mMinBBpoint;
	}

	MeshPartPtrVec& Mesh::GetMeshParts()
	{
		return mMeshParts;
	}

	MeshPtr Mesh::GetThis()
	{
		return static_pointer_cast<Mesh, BaseComponent>(shared_from_this());
	}

	bool Mesh::LoadModel(std::wstring aFile, ID3D11Device* apD3D11Device)
	{
		Vertex* vertices;
		UINT* indices;

		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
		D3D11_SUBRESOURCE_DATA vertexData, indexData;
		HRESULT result;

		Assimp::Importer Importer;
		const aiScene* pScene = Importer.ReadFile(WStringToString(aFile),  0
			//| aiProcessPreset_TargetRealtime_Quality 
			//| aiProcess_ConvertToLeftHanded //TODO 
			//| aiProcess_GenUVCoords
			| aiProcess_PreTransformVertices
			);

		if(pScene == NULL)
		{
			PRINT_WARNING("Could not find and read mesh");
			return false;
		}

		for(int k = 0; k < pScene->mNumMeshes; k++)
		{
			aiMesh* pMesh = pScene->mMeshes[k];
			mVertexCount += pMesh->mNumVertices;
			mIndexCount += (pMesh->mNumFaces * 3);
		}

		vertices = new Vertex[mVertexCount];
		indices = new UINT[mIndexCount];

		DWORD index = 0;
		for(UINT i = 0; i < pScene->mNumMeshes; i++)
		{
			aiMesh* pMesh = pScene->mMeshes[i];
			for (UINT j = 0 ; j < pMesh->mNumVertices ; j++) 
			{
				
				const aiVector3D* pPos = &(pMesh->mVertices[j]);
				vertices[j].mPos = Vector3(pPos->x, pPos->y, pPos->z);
				vertices[j].mTexture = Vector2(pMesh->mTextureCoords[0][j].x, 1 - pMesh->mTextureCoords[0][j].y);
				
				//Check if this is the lowest vertex for the bounding box
				//TODO

				//Check if this is the highest vertex for the bounding box
				//TODO
			}

			//Init the normals of this mesh
			if(pMesh->HasNormals())
			{
				for (UINT j = 0 ; j < pMesh->mNumVertices ; j++) 
				{
					const aiVector3D* pNor = &(pMesh->mNormals[j]);
					vertices[j].mNormal = Vector3(pNor->x, pNor->y, pNor->z);
				}
			}

			for (UINT face = 0; face < pMesh->mNumFaces; face++)
			{
				for (UINT idx = 0; idx < 3; idx++)
				{
					DWORD nextindex = index+(face*3+idx);
					indices[nextindex] = pMesh->mFaces[face].mIndices[idx];
				}
			}

			index += (pMesh->mNumFaces * 3);
		}

		//InitVertexBuffer(vertices, apD3D11Device);
		//InitIndexBuffer(indices, apD3D11Device);

		// Release the arrays now that the vertex and index buffers have been created and loaded.
		SAFE_ARRAY_DELETE(vertices);
		SAFE_ARRAY_DELETE(indices);

		return true;
	}

	//*************************************************************************
	// MeshPart
	//*************************************************************************
	MeshPart::~MeshPart()
	{
		for (Vector3Vec* textureCoordinates : mTextureCoords)
        {
            delete textureCoordinates;
        }

        for (Vector4Vec* vertexColors : mVertexColors)
        {
            delete vertexColors;
        }

		mVertices.clear();
		mNormals.clear();
		mTangents.clear();
		mBiNormals.clear();
		mTextureCoords.clear();
		mVertexColors.clear();
		mIndices.clear();
	}
	
	bool MeshPart::Init(MeshPtr apMesh, aiMesh* apAssimpMesh, ID3D11Device* apD3D11Device)
	{
		mpMesh = apMesh;
		aiMesh* mesh = apAssimpMesh;

		//Vertices
		if(mesh->mNumVertices <= 0)
		{
			return false;
		}

		mVertices.reserve(mesh->mNumVertices);
        for (UINT i = 0; i < mesh->mNumVertices; i++)
        {
           mVertices.push_back(Vector3(reinterpret_cast<const float*>(&mesh->mVertices[i])));			
        }

		//Normals
		if (mesh->HasNormals())
        {
			mNormals.reserve(mesh->mNumVertices);
            for (UINT i = 0; i < mesh->mNumVertices; i++)
            {
                mNormals.push_back(Vector3(reinterpret_cast<const float*>(&mesh->mNormals[i])));
            }
        }
		
// Tangents and Binormals
        if (mesh->HasTangentsAndBitangents())
        {
            mTangents.reserve(mesh->mNumVertices);
            mBiNormals.reserve(mesh->mNumVertices);
            for (UINT i = 0; i < mesh->mNumVertices; i++)
            {
                mTangents.push_back(Vector3(reinterpret_cast<const float*>(&mesh->mTangents[i])));
                mBiNormals.push_back(Vector3(reinterpret_cast<const float*>(&mesh->mBitangents[i])));
            }
        }

        // Texture Coordinates
        UINT uvChannelCount = mesh->GetNumUVChannels();
        for (UINT i = 0; i < uvChannelCount; i++)
        {
			Vector3Vec* textureCoordinates = new Vector3Vec();
            textureCoordinates->reserve(mesh->mNumVertices);
            mTextureCoords.push_back(textureCoordinates);

            aiVector3D* aiTextureCoordinates = mesh->mTextureCoords[i];
            for (UINT j = 0; j < mesh->mNumVertices; j++)
            {
                textureCoordinates->push_back(Vector3(reinterpret_cast<const float*>(&aiTextureCoordinates[j])));
            }
        }

        // Vertex Colors
        UINT colorChannelCount = mesh->GetNumColorChannels();
        for (UINT i = 0; i < colorChannelCount; i++)
        {
			Vector4Vec* vertexColors = new Vector4Vec();
            vertexColors->reserve(mesh->mNumVertices);
            mVertexColors.push_back(vertexColors);

            aiColor4D* aiVertexColors = mesh->mColors[i];
            for (UINT j = 0; j < mesh->mNumVertices; j++)
            {
                vertexColors->push_back(Vector4(reinterpret_cast<const float*>(&aiVertexColors[j])));
            }
        }

        // Faces (note: could pre-reserve if we limit primitive types)
        if (mesh->HasFaces())
        {
            mFaceCount = mesh->mNumFaces;
            for (UINT i = 0; i < mFaceCount; i++)
            {
                aiFace* face = &mesh->mFaces[i];
				for (UINT j = 0; j < face->mNumIndices; j++)
                {		
                    mIndices.push_back(face->mIndices[j]);
                }
            }
        }
		//mName = mesh->mName; //TODO
		CreateIndexBuffer(&mpIndexBuffer, apD3D11Device);

		return true;
	}

	void MeshPart::Draw(ID3D11DeviceContext* apD3D11DeviceContext)
	{
		// Set the index buffer to active in the input assembler so it can be rendered.
		//apD3D11DeviceContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		UINT stride, offset;

		// Set vertex buffer stride and offset.
		stride = sizeof(Vertex); 
		offset = 0;

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		apD3D11DeviceContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);

		// Set the index buffer to active in the input assembler so it can be rendered.
		apD3D11DeviceContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
		apD3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		mpMaterial->Draw(mIndices.size(), apD3D11DeviceContext,  0);
	}

	void MeshPart::SetStartIndex(const UINT aStartIndex)
	{
		mStartIndex = aStartIndex;
	}

	void MeshPart::SetStartVertex(const UINT aStartVertex)
	{
		mStartVertex = aStartVertex;
	}

	void MeshPart::SetMaterial(const MaterialPtr& apMaterial)
	{
		mpMaterial = apMaterial;
	}

	const MeshPtr MeshPart::GetMesh() const
	{
		return mpMesh.lock();
	}

	const Vector3Vec& MeshPart::GetVertices() const
	{
		return mVertices;
	}

	const Vector3Vec& MeshPart::GetNormals() const
	{
		return mNormals;
	}

	const Vector3Vec& MeshPart::GetTangents() const
	{
		return mTangents;
	}

	const Vector3Vec& MeshPart::GetBiNormals() const
	{
		return mBiNormals;
	}

	const vector<Vector3Vec*>& MeshPart::GetTextureCoords() const
	{
		return mTextureCoords;
	}

	const vector<Vector4Vec*>& MeshPart::GetVertexColors() const
	{
		return mVertexColors;
	}

	const UINTVec& MeshPart::GetIndices() const
	{
		return mIndices;
	}

	const UINT MeshPart::GetFaceCount() const
	{
		return mFaceCount;
	}

	const UINT MeshPart::GetStartIndex() const
	{
		return mStartIndex;
	}

	const UINT MeshPart::GetStartVertex() const
	{
		return mStartVertex;
	}

	ID3D11Buffer* MeshPart::GetVertexBuffer() const
	{
		return mpVertexBuffer;
	}

	ID3D11Buffer* MeshPart::GetIndexBuffer()  const
	{
		return mpIndexBuffer;
	}

	void MeshPart::CreateIndexBuffer(ID3D11Buffer** apIndexBuffer, ID3D11Device* apD3D11Device)
    {
        assert(apIndexBuffer != nullptr);

        D3D11_BUFFER_DESC indexBufferDesc;
        ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));
        indexBufferDesc.ByteWidth = sizeof(UINT) * mIndices.size();
        indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;		
        indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

        D3D11_SUBRESOURCE_DATA indexSubResourceData;
        ZeroMemory(&indexSubResourceData, sizeof(indexSubResourceData));
        indexSubResourceData.pSysMem = &mIndices[0];
        apD3D11Device->CreateBuffer(&indexBufferDesc, &indexSubResourceData, apIndexBuffer);
    }
};