#ifndef MESH_H_
#define MESH_H_

#include "BaseGeometry.h"
#include <assimp/Scene.h>
#include "Actors/Components/Material/Materials.h"

namespace GT
{
	class Mesh;
	class MeshPart;

	typedef weak_ptr<Mesh> MeshWPtr;
	typedef shared_ptr<MeshPart> MeshPartPtr;
	typedef vector<MeshPartPtr> MeshPartPtrVec;

	class GT_API Mesh : public BaseGeometry
	{
	public:
		Mesh();
		virtual ~Mesh();

		virtual bool Init(std::wstring aFile, ID3D11Device* apD3D11Device, bool abFlipUVs = false);

		Vector3 GetMaxBoundingBox();
		Vector3 GetMinBoundingBox();

		MeshPartPtrVec& GetMeshParts();
	private:
		MeshPtr GetThis();
		bool LoadModel(std::wstring aFile, ID3D11Device* apD3D11Device);
		
		Vector3 mMaxBBPoint;
		Vector3 mMinBBpoint;

		MeshPartPtrVec mMeshParts;
	};
	
	class GT_API MeshPart
	{
	public:
		virtual ~MeshPart();
		bool Init(MeshPtr apMesh, aiMesh* apAssimpMesh, ID3D11Device* apD3D11Device);

		void Draw(ID3D11DeviceContext* apD3D11DeviceContext);

		//Setters
		void SetStartIndex(const UINT aStartIndex);
		void SetStartVertex(const UINT aStartVertex);
		void SetMaterial(const MaterialPtr& apMaterial);

		//Getters
		const MeshPtr GetMesh() const;
		const Vector3Vec& GetVertices() const;
		const Vector3Vec& GetNormals() const;
		const Vector3Vec& GetTangents() const;
		const Vector3Vec& GetBiNormals() const;
		const vector<Vector3Vec*>& GetTextureCoords() const;
		const vector<Vector4Vec*>& GetVertexColors() const;
		const UINTVec& GetIndices() const;
		const UINT GetFaceCount() const;
		const UINT GetStartIndex() const;
		const UINT GetStartVertex() const;
		ID3D11Buffer* GetVertexBuffer() const;
		ID3D11Buffer* GetIndexBuffer()  const;
		void CreateIndexBuffer(ID3D11Buffer** apIndexBuffer, ID3D11Device* apD3D11Device);
		ID3D11Buffer* mpVertexBuffer;
	private:
		string mName;
		MeshWPtr mpMesh;
		Vector3Vec mVertices;
		Vector3Vec mNormals;
		Vector3Vec mTangents;
		Vector3Vec mBiNormals; //?
		vector<Vector3Vec*> mTextureCoords;
		vector<Vector4Vec*> mVertexColors;
		UINT mFaceCount;
		UINTVec mIndices;
		ID3D11Buffer* mpIndexBuffer;
		
		UINT mStartIndex;  //Start index of the total index buffer for this meshpart
		UINT mStartVertex; //Start vertex of the total vertex buffer for this meshpart

		MaterialPtr mpMaterial;
	};
};

#endif