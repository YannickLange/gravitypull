#ifndef PLANEMESH_H_
#define PLANEMESH_H_

#include "BaseGeometry.h"

namespace GT
{
	class GT_API PlaneMesh : public BaseGeometry
	{
	public:
		PlaneMesh();
		virtual ~PlaneMesh();
		virtual bool Init(ID3D11Device* apD3D11Device, Vector2 aHalfSize);
	};
};

#endif