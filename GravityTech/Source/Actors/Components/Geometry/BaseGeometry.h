#ifndef BASEGEOMETRY_H_
#define BASEGEOMETRY_H_

#include "GPECore.h"
#include "Actors/Components/BaseComponent.h"

namespace GT
{
	/**
	 * struct: VertexPosTex
	 * description: A default vertex for a geometry with position, text coordination and normal vector.
	 */
	struct GT_API VertexPosTex
	{
		Vector3 mPos;
		Vector2 mTexture;

		VertexPosTex() {}
		VertexPosTex(const Vector3 aPos, const Vector2 aTexture) :
			mPos(aPos),
			mTexture(aTexture)
		{}
	};

	/**
	 * struct: Vertex
	 * description: A default vertex for a geometry with position, text coordination and normal vector.
	 */
	struct GT_API Vertex
	{
		Vector3 mPos;
		Vector2 mTexture;
		Vector3 mNormal;
		
		Vertex(){}
		Vertex(const Vector3 aPos, const Vector2 aTexture, const Vector3 aNormal) :
			mPos(aPos),
			mTexture(aTexture),
			mNormal(aNormal)
		{}
	};

	/**
	 * struct: AABB
	 * description: Axis aligned bounding box.
	 */
	struct GT_API AABB
	{

	};

	/**
	 * struct: OBB
	 * description: object/oriented bounding box.
	 */
	struct GT_API OBB
	{

	};

	/**
	 * class: BaseGeometry
	 * description: The base class for all the 3d models (meshes, cubes, spheres, etc.). 
	 *				It holds for example the vertices and indices in DirectX buffers.
	 */
	class GT_API BaseGeometry : public BaseComponent
	{
	public:
		BaseGeometry();
		virtual ~BaseGeometry();
		
		virtual bool Init();
		virtual void PreDraw(ID3D11DeviceContext* apD3D11DeviceContext);
		
		const UINT GetIndexCount() const;
		const UINT GetVertexCount() const;
		const UINT GetTriangleCount() const;
		const FLOAT GetRadius() const;
		const Vector3 GetOrigin() const;
		void CreateIndexBuffer(ID3D11Buffer** apIndexBuffer);
		
	protected:
		template<typename VertexType>
		bool InitVertexBuffer(ID3D11Buffer** apBuffer, VertexType*& aVertices, UINT aVertexCount, ID3D11Device* apD3D11Device, const D3D11_USAGE& aBufferUsage = D3D11_USAGE::D3D11_USAGE_DEFAULT)
		{
			HRESULT result;
			D3D11_BUFFER_DESC bufferDesc;
			D3D11_SUBRESOURCE_DATA data;

			// Set up the description of the static vertex buffer.
			bufferDesc.Usage = aBufferUsage;
			bufferDesc.ByteWidth = sizeof(VertexType) * aVertexCount;
			bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bufferDesc.CPUAccessFlags = 0;
			bufferDesc.MiscFlags = 0;
			bufferDesc.StructureByteStride = 0;

			// Give the subresource structure a pointer to the vertex data.
			data.pSysMem = aVertices;
			data.SysMemPitch = 0;
			data.SysMemSlicePitch = 0;

			// Now create the vertex buffer.
			result = apD3D11Device->CreateBuffer(&bufferDesc, &data, apBuffer);
			if(FAILED(result))
			{
				return false;
			}

			return true;
		}

		bool InitIndexBuffer(UINT*& aIndices, ID3D11Device* apD3D11Device, const D3D11_USAGE& aBufferUsage = D3D11_USAGE::D3D11_USAGE_DEFAULT);

		ID3D11Buffer* mpVertexBuffer;	//TODO: Don't give every mesh its own vertex buffer. Use one vertex buffer for multiple meshes
		ID3D11Buffer* mpIndexBuffer;	//TODO: Don't give every mesh its own index buffer. Use one index buffer for multiple meshes

		Vector3 mOrigin;
		FLOAT mRadius;

		UINT mIndexCount;
		UINT mVertexCount;
		UINT mTriangleCount;

		Vector3 mBoundingBoxA;
		Vector3 mBoundingBoxB;
	};
};

#endif