#include "Material.h"
#include "Managers/DeviceManager.h"
#include "Managers/RenderManager.h"

namespace GT
{
	Material::Material() :
		BaseComponent(ComponentType::MATERIAL),
		mpInputLayout(NULL)
	{
	}

	Material::~Material()
	{
		SAFE_RELEASE(mpInputLayout);
	}
		
	bool Material::Init(const DeviceManagerPtr& apDeviceManager, const EffectPtr apEffect)
	{
		bool result = true;

		mpEffect = apEffect;
		result = InitTechnique();
		
		InitVariables(apDeviceManager);
		InitInputLayout(apDeviceManager);

		return result;
	}

	bool Material::Init(const DeviceManagerPtr& apDeviceManager, const RenderManagerPtr& apRenderManager, const wstring& aFileName)
	{
		bool result = true;

		//mpEffect = apRenderManager->GetEffect(aFileName); // TODO
		result = InitTechnique();
		
		InitVariables(apDeviceManager);

		return result;
	}

	void Material::Draw(UINT aMeshIndexCount,  ID3D11DeviceContext*  apDeviceContext, UINT aStartMeshIndexLocation)
	{
		apDeviceContext->IASetInputLayout(mpInputLayout);

		for(UINT p = 0; p < mpCurrentTechnique->GetTechniqueDesc().Passes; ++p)
		{
			mpCurrentTechnique->GetTechnique()->GetPassByIndex(p)->Apply(0, apDeviceContext);
		}
		
		apDeviceContext->DrawIndexed(aMeshIndexCount, aStartMeshIndexLocation, 0);
	}

	void Material::PreDraw(ID3D11DeviceContext* apDeviceContext)
	{
		apDeviceContext->IASetInputLayout(mpInputLayout);

		for(UINT p = 0; p < mpCurrentTechnique->GetTechniqueDesc().Passes; ++p)
		{
			mpCurrentTechnique->GetTechnique()->GetPassByIndex(p)->Apply(0, apDeviceContext);
		}
	}

	//Getters
	const EffectPtr& Material::GetEffect() const
	{
		return mpEffect;
	}

	const EffectTechniquePtr& Material::GetCurrentTechnique() const
	{
		return mpCurrentTechnique;
	}

	const ID3D11InputLayout* Material::GetCurrentLayout() const
	{
		return mpInputLayout;
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, Matrix& aValue)
	{
		apVariable->AsMatrix()->SetMatrix(reinterpret_cast<float*>(&aValue));
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, Vector2& aValue)
	{
		apVariable->AsVector()->SetFloatVector(reinterpret_cast<float*>(&aValue));
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, Vector3& aValue)
	{
		apVariable->AsVector()->SetFloatVector(reinterpret_cast<float*>(&aValue));
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, Vector4& aValue)
	{
		apVariable->AsVector()->SetFloatVector(reinterpret_cast<float*>(&aValue));
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, float aValue)
	{
		apVariable->AsScalar()->SetFloat(aValue);
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, int aValue)
	{
		apVariable->AsScalar()->SetInt(aValue);
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, bool aValue)
	{
		apVariable->AsScalar()->SetBool(aValue);
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, ID3D11ShaderResourceView* apValue)
	{
		apVariable->AsShaderResource()->SetResource(apValue);
	}

	void Material::UpdateVar(ID3DX11EffectVariable* apVariable, ID3D11SamplerState* apValue)
	{
		apVariable->AsSampler()->SetSampler(0, apValue);
	}

	void Material::CreateInputLayout(const DeviceManagerPtr& apDeviceManager, const string& aTechniqueName, 
		const string& aPassName, D3D11_INPUT_ELEMENT_DESC* apInputElementDesc, UINT aInputElementDescCount)
    {
		EffectPassPtr pass = mpEffect->GetTechnique(aTechniqueName)->GetPass(aPassName);
        assert(pass != nullptr);

		apDeviceManager->GetDevice()->CreateInputLayout(apInputElementDesc, aInputElementDescCount,  
			pass->GetPassDesc().pIAInputSignature, pass->GetPassDesc().IAInputSignatureSize, &mpInputLayout);
    }

	bool Material::InitTechnique(const string& aTechniqueName)
	{
		//Check if the technique is already initialized or if there is not effect
		if(mpCurrentTechnique || !mpEffect)
		{
			return false;
		}

		//Return if their are no techniques
		if(mpEffect->GetTechniqueMap().size() < 0)
		{
			return false;
		}

		if(!aTechniqueName.empty())
		{
			mpCurrentTechnique = mpEffect->GetTechnique(aTechniqueName);
		}
		else
		{
			mpCurrentTechnique = mpEffect->GetTechniqueMap().begin()->second;
		}

		return true;
	}
};