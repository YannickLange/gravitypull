#ifndef MATERIAL_H_
#define MATERIAL_H_

#include "Graphics/Effects/Effect.h"
#include "GPECore.h"
#include "Actors/Components/BaseComponent.h"
#include "Graphics/Effects/Effect.h"
#include "Graphics/Effects/EffectPass.h"
#include "Graphics/Effects/EffectTechnique.h"
#include "Graphics/Effects/EffectVariable.h"

namespace GT
{
	class GT_API Material : public BaseComponent
	{
	public:
		Material();
		virtual ~Material();
		
		virtual bool Init(const DeviceManagerPtr& apDeviceManager, const EffectPtr apEffect);
		virtual bool Init(const DeviceManagerPtr& apDeviceManager, const RenderManagerPtr& apRenderManager, const wstring& aFileName);
		virtual void InitVariables(const DeviceManagerPtr& apDeviceManager) = 0;
		virtual void InitInputLayout(const DeviceManagerPtr& apDeviceManager) = 0;
		
		virtual void Update(){};
		void Draw(UINT aMeshIndexCount, ID3D11DeviceContext* apDeviceContext, UINT aStartMeshIndexLocation = 0);
		void PreDraw(ID3D11DeviceContext* apDeviceContext);

		//Getters
		const EffectPtr& GetEffect() const;
		const EffectTechniquePtr& GetCurrentTechnique() const;
		const ID3D11InputLayout* GetCurrentLayout() const;
		
		void UpdateVar(ID3DX11EffectVariable* apVariable, Matrix& aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, Vector2& aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, Vector3& aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, Vector4& aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, float aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, int aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, bool aValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, ID3D11ShaderResourceView* apValue);
		void UpdateVar(ID3DX11EffectVariable* apVariable, ID3D11SamplerState* apValue);

	protected:
		void CreateInputLayout(const DeviceManagerPtr& apDeviceManager,const string& aTechniqueName, 
			const string& aPassName, D3D11_INPUT_ELEMENT_DESC* apInputElementDesc, UINT aInputElementDescCount);
		bool InitTechnique(const string& aTechniqueName = "");

		EffectPtr mpEffect;
		EffectTechniquePtr mpCurrentTechnique;
		ID3D11InputLayout* mpInputLayout;
	};

	#define MATERIAL_VAR_DECLARE(VariableName)				\
		public:												\
			EffectVariablePtr& Get ## VariableName()		\
			{												\
				return mp ## VariableName;					\
			}												\
															\
			template<typename Type>							\
			void Set ## VariableName(Type aValue)			\
			{                                               \
				UpdateVar(mp ## VariableName->GetVariable(), aValue);\
			}												\
		private:											\
			EffectVariablePtr mp ## VariableName;

	#define MATERIAL_VAR_INIT(VariableName)					\
		mp ## VariableName = mpEffect.get()->GetVariable(#VariableName);

	#define MATERIAL_VAR_CONSTRUCT(VariableName) mp ## VariableName(NULL)
};

#endif