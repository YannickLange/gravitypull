#ifndef MATERIALS_H_
#define MATERIALS_H_

#include "Material.h"

namespace GT
{
	//Forward declarations
	class BaseMaterial ;
	class TextureMaterial;
	class SkyboxMaterial;
	class SpriteMaterial;

	//Typedef Smart pointers
	typedef shared_ptr<BaseMaterial> BaseMaterialPtr;
	typedef shared_ptr<TextureMaterial> TextureMaterialPtr;
	typedef shared_ptr<SkyboxMaterial> SkyboxMaterialPtr;
	typedef shared_ptr<SpriteMaterial> SpriteMaterialPtr;

	const static wstring ENGINE_EFFECT_FOLER = L"../GPEngine/GT/Assets/Effects/";

	/**
	 * Class:	BaseMaterial
	 * Desc:	This is the most default material. It uses the worldviewprojection with a
	 */
	class GT_API BaseMaterial : public Material
	{
		MATERIAL_VAR_DECLARE(WorldViewProjection);
		MATERIAL_VAR_DECLARE(Color);
		MATERIAL_VAR_DECLARE(World);

	public:
		BaseMaterial();
		virtual void InitVariables(const DeviceManagerPtr& apDeviceManager);
		virtual void InitInputLayout(const DeviceManagerPtr& apDeviceManager);
		virtual void Update(Matrix& aWorldViewProjection);
	};

	/**
	 * Class:	TextureMaterial
	 * Desc:	Material with a texture
	 */
	class GT_API TextureMaterial : public BaseMaterial
	{
		MATERIAL_VAR_DECLARE(Texture);
		MATERIAL_VAR_DECLARE(SampleType);
	public:
		TextureMaterial();
		virtual void InitVariables(const DeviceManagerPtr& apDeviceManager);
		virtual void InitInputLayout(const DeviceManagerPtr& apDeviceManager);
	};

	/**
	 * Class:	SkyboxMaterial
	 * Desc:	Material specific for skybox usage
	 */
	class GT_API SkyboxMaterial : public BaseMaterial
	{
		MATERIAL_VAR_DECLARE(SkyboxTextureCube)
	public:
		SkyboxMaterial();
		virtual void InitVariables(const DeviceManagerPtr& apDeviceManager);
		virtual void InitInputLayout(const DeviceManagerPtr& apDeviceManager);
	};

	/**
	 * Class:	SpriteMaterial
	 * Desc:	Material specific for skybox usage
	 */
	class GT_API SpriteMaterial : public BaseMaterial
	{
		MATERIAL_VAR_DECLARE(TextureMap)
		MATERIAL_VAR_DECLARE(TextureSample)
	public:
		SpriteMaterial();
		virtual void InitVariables(const DeviceManagerPtr& apDeviceManager);
		virtual void InitInputLayout(const DeviceManagerPtr& apDeviceManager);
	};
};

#endif