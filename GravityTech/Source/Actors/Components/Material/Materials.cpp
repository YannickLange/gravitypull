#include "Materials.h"
#include "Graphics/DXInclude.h"
#include "Managers/DeviceManager.h"
#include "Managers/RenderManager.h"

namespace GT
{
	//*************************************************************************
	// BaseMaterial
	//*************************************************************************
	BaseMaterial::BaseMaterial() :
		Material(),
		mpWorldViewProjection(nullptr)
	{
	}

	void BaseMaterial::InitVariables(const DeviceManagerPtr& apDeviceManager)
	{
		MATERIAL_VAR_INIT(WorldViewProjection);
		MATERIAL_VAR_INIT(Color);
		MATERIAL_VAR_INIT(World);
	}

	void BaseMaterial::InitInputLayout(const DeviceManagerPtr& apDeviceManager)
	{
		D3D11_INPUT_ELEMENT_DESC inputElementDescriptions[] =
        {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };

        CreateInputLayout(apDeviceManager, "BaseTechnique", "P0", inputElementDescriptions, ARRAYSIZE(inputElementDescriptions));
	}

	void BaseMaterial::Update(Matrix& aWorldViewProjection)
	{
		UpdateVar(GetWorldViewProjection()->GetVariable(), aWorldViewProjection);
	}

	//*************************************************************************
	// Texture material
	//*************************************************************************
	TextureMaterial::TextureMaterial() :
		BaseMaterial(),
		mpTexture(nullptr)
	{
	}

	void TextureMaterial::InitVariables(const DeviceManagerPtr& apDeviceManager)
	{
		MATERIAL_VAR_INIT(Texture);
		MATERIAL_VAR_INIT(SampleType);

		BaseMaterial::InitVariables(apDeviceManager);
	}

	void TextureMaterial::InitInputLayout(const DeviceManagerPtr& apDeviceManager)
	{
		D3D11_INPUT_ELEMENT_DESC inputElementDescriptions[] =
        {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },  
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },  
			{ "NORMAL",	 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0}
        };

        CreateInputLayout(apDeviceManager, "BaseTechnique", "P0", inputElementDescriptions, ARRAYSIZE(inputElementDescriptions));
	}

	//*************************************************************************
	// Texture material
	//*************************************************************************
	SkyboxMaterial::SkyboxMaterial() :
		BaseMaterial(),
		mpSkyboxTextureCube(nullptr)
	{

	}

	void SkyboxMaterial::InitVariables(const DeviceManagerPtr& apDeviceManager)
	{
		MATERIAL_VAR_INIT(SkyboxTextureCube);

		BaseMaterial::InitVariables(apDeviceManager);
	}

	void SkyboxMaterial::InitInputLayout(const DeviceManagerPtr& apDeviceManager)
	{
		D3D11_INPUT_ELEMENT_DESC inputElementDescriptions[] =
        {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };

        CreateInputLayout(apDeviceManager, "SkyboxTechnique", "P0", inputElementDescriptions, ARRAYSIZE(inputElementDescriptions));
	}

	//*************************************************************************
	// Sprite material
	//*************************************************************************
	SpriteMaterial::SpriteMaterial() :
		BaseMaterial(),
		mpTextureMap(nullptr),
		mpTextureSample(nullptr)
	{

	}

	void SpriteMaterial::InitVariables(const DeviceManagerPtr& apDeviceManager)
	{
		MATERIAL_VAR_INIT(TextureMap);
		MATERIAL_VAR_INIT(TextureSample);

		BaseMaterial::InitVariables(apDeviceManager);
	}

	void SpriteMaterial::InitInputLayout(const DeviceManagerPtr& apDeviceManager)
	{
		D3D11_INPUT_ELEMENT_DESC inputElementDescriptions[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 } //,
			//{ "NORMAL",	 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0}
		};

		CreateInputLayout(apDeviceManager, "TechniqueSprite", "P0", inputElementDescriptions, ARRAYSIZE(inputElementDescriptions));
	}
};