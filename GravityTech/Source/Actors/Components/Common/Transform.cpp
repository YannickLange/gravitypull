#include "Transform.h"

namespace GT
{
	Transform::Transform() :
		BaseComponent(ComponentType::TRANSFORM)
	{
		mScale = Matrix::Identity();
		mRotation = Matrix::Identity();
		mTranslation = Matrix::Identity();
	}

	Transform::~Transform()
	{

	}

	void Transform::SetPosition(Vector3 aPosition)
	{
		mTranslation = XMMatrixTranslationFromVector(aPosition);
	}

	void Transform::SetPosition(float aX, float aY, float aZ)
	{
		mTranslation = XMMatrixTranslation(aX, aY, aZ);
	}

	void Transform::SetRotation(const Matrix& aRotation)
	{
		mRotation = aRotation;
	}

	void Transform::SetRotation(Vector3 aRotationAxis, float aAngle)
	{
		mRotation = XMMatrixRotationAxis(aRotationAxis, aAngle);
	}

	void Transform::SetRotation(float aXAxis, float aYAxis, float aZAxis, float aAngle)
	{
		mRotation = XMMatrixRotationAxis(Vector3(aXAxis, aYAxis, aZAxis), aAngle);
	}

	void Transform::SetScale(const float aScale)
	{
		mScale = XMMatrixScalingFromVector(Vector3(aScale));
	}

	void Transform::SetScale(Vector3 aScale)
	{
		mScale = XMMatrixScalingFromVector(aScale);
	}

	void Transform::SetScale(float aX, float aY, float aZ)
	{
		mScale = XMMatrixScaling(aX, aY, aZ);
	}

	void Transform::ApplyPosition(Vector3 aPosition)
	{
		//mMatrix = XMMatrixTranslationFromVector(aPosition);
	}

	void Transform::ApplyPosition(float aX, float aY, float aZ)
	{
		mTranslation.Translation(Vector3(aX, aY, aZ));
	}

	void Transform::ApplyRotation(Vector3 aRotation)
	{

	}

	void Transform::ApplyRotation(float aPitch, float aRoll, float aYaw)
	{

	}

	void Transform::ApplyScale(Vector3 aScale)
	{

	}

	void Transform::ApplyScale(float aX, float aY, float aZ)
	{

	}

	Matrix Transform::GetTransformMatrix() const
	{
		return mScale * mRotation * mTranslation;
	}

	Matrix Transform::GetTranslationMatrix() const
	{
		return mTranslation;
	}

	Matrix Transform::GetScaleMatrix() const
	{
		return mScale;
	}

	Matrix Transform::GetRotationMatrix() const
	{
		return mRotation;
	}

	Vector3 Transform::GetPosition() const
	{
		return Vector3();
	}

	Vector3 Transform::GetRotation() const
	{
		return Vector3(); // mMatrix.Right; //TODO
	}

	Vector3 Transform::GetScale() const
	{
		return Vector3(); //TODO
	}
};