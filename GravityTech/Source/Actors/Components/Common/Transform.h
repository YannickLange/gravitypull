#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#include "Actors/Components/BaseComponent.h"
#include "GPECore.h"

namespace GT
{
	class GT_API Transform : public BaseComponent
	{
	public:
		Transform();
		virtual ~Transform();

		void SetPosition(Vector3 aPosition);
		void SetPosition(float aX, float aY, float aZ);
		void SetRotation(const Matrix& aRotation);
		void SetRotation(Vector3 aRotationAxis, float aAngle);
		void SetRotation(float aXAxis, float aYAxis, float aZAxis, float aAngle);
		void SetScale(const float aScale);
		void SetScale(Vector3 aScale);
		void SetScale(float aX, float aY, float aZ);

		//Adding
		void ApplyPosition(Vector3 aPosition);
		void ApplyPosition(float aX, float aY, float aZ);
		void ApplyRotation(Vector3 aRotation);
		void ApplyRotation(float aPitch, float aRoll, float aYaw);
		void ApplyScale(Vector3 aScale);
		void ApplyScale(float aX, float aY, float aZ);

		Matrix GetTransformMatrix() const;
		Matrix GetTranslationMatrix() const;
		Matrix GetScaleMatrix() const;
		Matrix GetRotationMatrix() const;

		Vector3 GetPosition() const;
		Vector3 GetRotation() const;
		Vector3 GetEulerAngles() const;
		Vector3 GetScale() const;

		float GetRotationX() const;
		float GetRotationY() const;
		float GetRotationZ() const;
	private:
		Matrix mTranslation;
		Matrix mScale;
		Matrix mRotation;
	};
};

#endif