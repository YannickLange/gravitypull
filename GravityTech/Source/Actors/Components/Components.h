#ifndef COMPONENTS_H_
#define COMPONENTS_H_

#include "Actors/Components/ComponentTypes.h"

//Include Common components
#include "Actors/Components/BaseComponent.h"
#include "Actors/Components/Common/Transform.h"

//Include Geometry components
#include "Actors/Components/Geometry/BaseGeometry.h"
#include "Actors/Components/Geometry/Mesh.h"
#include "Actors/Components/Renderers/MeshRender.h"

//Include Helpers components
#include "Actors/Components/Material/Materials.h"

//Include Material components

//Include Audio components

//Include Light components

//Include Animation components

//Include Script components

//Include Physics components

//Include Particle components

#endif