#ifndef BASECOMPONENT_H_
#define BASECOMPONENT_H_

#include "GPECore.h"
#include "Actors/Components/ComponentTypes.h"

namespace GT
{
	class GT_API BaseComponent : public enable_shared_from_this<BaseComponent>
	{
	public:
		BaseComponent();
		BaseComponent(const ComponentType aType);
		virtual ~BaseComponent();

		//Setters
		void SetActor(const BaseActorPtr& apActor);
		void SetEnabled(const bool abEnabled);
		void Enable();
		void Disable();

		//Getters
		const ComponentType GetType() const;
		BaseActorPtr GetActor() const;
		BaseComponentPtr GetThis();
		bool GetEnabled() const;

	private:
		ComponentType mType;
		BaseActorWPtr mpActor;
		bool mbEnabled;
	};
};

#endif