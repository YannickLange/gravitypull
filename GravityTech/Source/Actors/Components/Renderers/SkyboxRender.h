#ifndef SKYBOXRENDER_H_
#define SKYBOXRENDER_H_

#include "GPECore.h"
#include "MeshRender.h"

namespace GT
{
	class SkyboxMaterial;
	typedef shared_ptr<SkyboxMaterial> SkyboxMaterialPtr;

	class GT_API SkyboxRender : public MeshRender
	{
	public:
		virtual void Init(const MeshPtr& apMesh, const SkyboxMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager);
		virtual void Draw(const DeviceManagerPtr& apDeviceManager);

		SkyboxMaterialPtr GetMaterial() const;

		ID3D11DepthStencilState* apDSLessEqual;
		ID3D11RasterizerState* apRSCullNone;
	};
};

#endif