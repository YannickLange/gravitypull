#include "MeshRender.h"
#include "Actors/Components/Geometry/Mesh.h"
#include "Actors/Components/Material/Materials.h"
#include "Managers/DeviceManager.h"

namespace GT
{
	MeshRender::MeshRender() :
		RenderComponent()
	{

	}

	MeshRender::~MeshRender()
	{

	}

	void MeshRender::Init(const MeshPtr& apMesh, const BaseMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager)
	{
		
		RenderComponent::Init((BaseGeometryPtr)apMesh, (MaterialPtr)apMaterial);
		
		MeshPartPtrVec& meshparts = GetMesh()->GetMeshParts();
		for(MeshPartPtrVec::iterator it = meshparts.begin(); it != meshparts.end(); it++)
		{
			it->get()->SetMaterial(mpMaterial);
		}

	}

	void MeshRender::Draw(const DeviceManagerPtr& apDeviceManager)
	{
		//UINT i = 0;
		MeshPartPtrVec& meshparts = GetMesh()->GetMeshParts();
		for(MeshPartPtrVec::iterator it = meshparts.begin(); it != meshparts.end(); it++)
		{
			it->get()->Draw(apDeviceManager->GetDeviceContext());
		}
	}

	MeshPtr MeshRender::GetMesh() const
	{
		return static_pointer_cast<Mesh, BaseGeometry>(mpGeometry);
	}

	BaseMaterialPtr MeshRender::GetMaterial() const
	{
		return static_pointer_cast<BaseMaterial, Material>(mpMaterial);
	}
};