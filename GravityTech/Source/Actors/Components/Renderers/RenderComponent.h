#ifndef RENDERCOMPONENT_H_
#define RENDERCOMPONENT_H_

#include "Actors/Components/BaseComponent.h"
#include "Actors/Components/Geometry/BaseGeometry.h"
#include "Actors/Components/Material/Materials.h"

namespace GT
{
	class GT_API RenderComponent : public BaseComponent
	{
	public:
		RenderComponent();
		virtual ~RenderComponent();
		
		virtual void Init(const BaseGeometryPtr& apGeomety, const MaterialPtr& apMaterial);
		virtual void Draw(const DeviceManagerPtr& apDeviceManager);

		BaseGeometryPtr GetGeometry() const;
		MaterialPtr GetMaterial() const;

	protected:
		BaseGeometryPtr mpGeometry;
		MaterialPtr mpMaterial;
	};
};

#endif