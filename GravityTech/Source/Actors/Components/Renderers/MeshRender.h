#ifndef MESHRENDER_H_
#define MESHRENDER_H_

#include "GPECore.h"
#include "Actors/Components/Renderers/RenderComponent.h"

namespace GT
{
	typedef vector<MeshPtr> MeshPtrVec;
	typedef vector<BaseMaterialPtr> BaseMaterialPtrVec;

	class GT_API MeshRender : public RenderComponent
	{
	public:
		MeshRender();
		virtual ~MeshRender();

		virtual void Init(const MeshPtr& apMesh, const BaseMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager);
		virtual void Draw(const DeviceManagerPtr& apDeviceManager);

		virtual MeshPtr GetMesh() const;
		BaseMaterialPtr GetMaterial() const;
	private:
		MeshPtrVec mMeshes;
		BaseMaterialPtrVec mMaterials;
	};
};

#endif