#include "RenderComponent.h"
#include "Managers/DeviceManager.h"

namespace GT
{
	RenderComponent::RenderComponent() :
		BaseComponent(ComponentType::RENDER)
	{

	}

	RenderComponent::~RenderComponent()
	{
	
	}

	void RenderComponent::Init(const BaseGeometryPtr& apGeomety, const MaterialPtr& apMaterial)
	{
		mpGeometry = apGeomety;
		mpMaterial = apMaterial;
	}

	void RenderComponent::Draw(const DeviceManagerPtr& apDeviceManager)
	{
		mpGeometry->PreDraw(apDeviceManager->GetDeviceContext());
		//const Matrix& aWorld, const Matrix& aView, const Matrix& aProjection,
		//aWorld * GetActor()->GetTransform()->GetTransformMatrix() * aView * aProjection
		mpMaterial->Draw(mpGeometry->GetIndexCount(), apDeviceManager->GetDeviceContext());
	}

	BaseGeometryPtr RenderComponent::GetGeometry() const
	{
		return mpGeometry;
	}

	MaterialPtr RenderComponent::GetMaterial() const
	{
		return mpMaterial;
	}
};