#include "SkyboxRender.h"
#include "Graphics/Texture2D.h"
#include "Actors/Components/Material/Materials.h"
#include "Actors/Components/Geometry/Mesh.h"
#include "Managers/DeviceManager.h"

namespace GT
{
	void SkyboxRender::Init(const MeshPtr& apMesh, const SkyboxMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager)
	{
		D3D11_DEPTH_STENCIL_DESC dssDesc;
		ZeroMemory(&dssDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
		dssDesc.DepthEnable = true;
		dssDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		dssDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
		apDeviceManager->GetDevice()->CreateDepthStencilState(&dssDesc, &apDSLessEqual);

		MeshRender::Init(apMesh, (BaseMaterialPtr)apMaterial, apDeviceManager);
	}

	void SkyboxRender::Draw(const DeviceManagerPtr& apDeviceManager)
	{
		apDeviceManager->GetDeviceContext()->OMSetDepthStencilState(apDSLessEqual, 0);
		
		MeshRender::Draw(apDeviceManager);
	}

	SkyboxMaterialPtr SkyboxRender::GetMaterial() const
	{
		return static_pointer_cast<SkyboxMaterial, Material>(mpMaterial);
	}
};
