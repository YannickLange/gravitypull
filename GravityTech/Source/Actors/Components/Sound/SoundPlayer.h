#ifndef SOUNDPLAYER_H_
#define SOUNDPLAYER_H_

#include "GPECore.h"
#include "Actors/Components/BaseComponent.h"

namespace GT
{
	class GT_API SoundPlayer : public BaseComponent
	{
	public:
		SoundPlayer();
		virtual ~SoundPlayer();

		void Init(const SoundPtr apSound);
		void Play();
		//TODO: multiple sounds
	private:
		SoundPtr mpSound;
	};
};

#endif