#include "Sound.h"
#include "Managers/SoundManager.h"

namespace GT
{
	Sound::Sound() :
		mpSecondaryBuffer(NULL)
	{

	}

	Sound::~Sound()
	{
		SAFE_RELEASE(mpSecondaryBuffer);
	}

	bool Sound::Init(const wstring aFile, SoundManagerPtr apSoundManager)
	{
		//TODO check wav, mp3 etc...
		return LoadWaveFile(aFile, apSoundManager);
	}

	bool Sound::Play()
	{
		HRESULT result;

		// Set position at the beginning of the sound buffer.
		result = mpSecondaryBuffer->SetCurrentPosition(0);
		if(FAILED(result)) {return false;}

		// Set volume of the buffer to 100%.
		result = mpSecondaryBuffer->SetVolume(DSBVOLUME_MAX);
		if(FAILED(result)) {return false;}

		// Play the contents of the secondary sound buffer.
		result = mpSecondaryBuffer->Play(0, 0, 0);
		if(FAILED(result)) { return false;}

		return true;

	}

	bool Sound::LoadWaveFile(const wstring aFile, SoundManagerPtr apSoundManager)
	{
		HRESULT result;
		bool error;
		FILE* pFile;
		UINT count;
		SoundManager::WaveFormat waveFile;
		WAVEFORMATEX waveFormat;
		DSBUFFERDESC bufferDesc;
		IDirectSoundBuffer* pTempBuffer;
		UCHAR* waveData;
		UCHAR* pBuffer;
		ULONG bufferSize;

		//Open wave file in binary
		error = fopen_s(&pFile, WStringToString(aFile).c_str(), "rb");
		if(error != 0) {return false;}

		//Read in the wave file
		count = fread(&waveFile, sizeof(waveFile), 1, pFile);
		if(count != 1) {return false;}

		// Check that the chunk ID is the RIFF format.
		if((waveFile.mChunkID[0] != 'R') || (waveFile.mChunkID[1] != 'I') || 
		   (waveFile.mChunkID[2] != 'F') || (waveFile.mChunkID[3] != 'F'))
		{
			return false;
		}

		// Check that the file format is the WAVE format.
		if((waveFile.mFormat[0] != 'W') || (waveFile.mFormat[1] != 'A') ||
		   (waveFile.mFormat[2] != 'V') || (waveFile.mFormat[3] != 'E'))
		{
			return false;
		}

		// Check that the sub chunk ID is the fmt format.
		if((waveFile.mSubChunkID[0] != 'f') || (waveFile.mSubChunkID[1] != 'm') ||
		   (waveFile.mSubChunkID[2] != 't') || (waveFile.mSubChunkID[3] != ' '))
		{
			return false;
		}

		// Check that the audio format is WAVE_FORMAT_PCM.
		if(waveFile.mAudioFormat != WAVE_FORMAT_PCM)
		{
			return false;
		}

		// Check that the wave file was recorded in stereo format.
		//if(waveFile.mNumChannels != 2)
		//{
		//	return false;
		//}

		// Check that the wave file was recorded at a sample rate of 44.1 KHz.
		if(waveFile.mSampleRate != 44100)
		{
			return false;
		}

		// Ensure that the wave file was recorded in 16 bit format.
		if(waveFile.mBitsPerSample != 16)
		{
			return false;
		}

		// Check for the data chunk header.
		if((waveFile.mDataChunkID[0] != 'd') || (waveFile.mDataChunkID[1] != 'a') ||
		   (waveFile.mDataChunkID[2] != 't') || (waveFile.mDataChunkID[3] != 'a'))
		{
			return false;
		}

		// Set the wave format of secondary buffer that this wave file will be loaded onto.
		waveFormat.wFormatTag = WAVE_FORMAT_PCM;
		waveFormat.nSamplesPerSec = 44100;
		waveFormat.wBitsPerSample = 16;
		waveFormat.nChannels = 2;
		waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
		waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
		waveFormat.cbSize = 0;

		// Set the buffer description of the secondary sound buffer that the wave file will be loaded onto.
		bufferDesc.dwSize = sizeof(DSBUFFERDESC);
		bufferDesc.dwFlags = DSBCAPS_CTRLVOLUME;
		bufferDesc.dwBufferBytes = waveFile.mDataSize;
		bufferDesc.dwReserved = 0;
		bufferDesc.lpwfxFormat = &waveFormat;
		bufferDesc.guid3DAlgorithm = GUID_NULL;

		// Create a temporary sound buffer with the specific buffer settings.
		result = apSoundManager->GetIDirectSound()->CreateSoundBuffer(&bufferDesc, &pTempBuffer, NULL);
		if(FAILED(result))
		{
			return false;
		}

		// Test the buffer format against the direct sound 8 interface and create the secondary buffer.
		result = pTempBuffer->QueryInterface(IID_IDirectSoundBuffer8, (void**)&mpSecondaryBuffer);
		if(FAILED(result))
		{
			return false;
		}

		// Release the temporary buffer.
		SAFE_RELEASE(pTempBuffer);

		// Move to the beginning of the wave data which starts at the end of the data chunk header.
		fseek(pFile, sizeof(SoundManager::WaveFormat), SEEK_SET);

		// Create a temporary buffer to hold the wave file data.
		waveData = new unsigned char[waveFile.mDataSize];
		if(!waveData)
		{
			return false;
		}

		// Read in the wave file data into the newly created buffer.
		count = fread(waveData, 1, waveFile.mDataSize, pFile);
		if(count != waveFile.mDataSize)
		{
			return false;
		}

		// Close the file once done reading.
		error = fclose(pFile);
		if(error != 0)
		{
			return false;
		}

		// Lock the secondary buffer to write wave data into it.
		result = mpSecondaryBuffer->Lock(0, waveFile.mDataSize, (void**)&pBuffer, (DWORD*)&bufferSize, NULL, 0, 0);
		if(FAILED(result))
		{
			return false;
		}

		// Copy the wave data into the buffer.
		memcpy(pBuffer, waveData, waveFile.mDataSize);

		// Unlock the secondary buffer after the data has been written to it.
		result = mpSecondaryBuffer->Unlock((void*)pBuffer, bufferSize, NULL, 0);
		if(FAILED(result))
		{
			return false;
		}
	
		// Release the wave data since it was copied into the secondary buffer.
		SAFE_ARRAY_DELETE(waveData);

		return true;

	}
};