#ifndef SOUND_H_
#define SOUND_H_

#include "GPECore.h"
#include <dsound.h>

namespace GT
{
	enum SoundFileType
	{
		NOTYPE = 0,
		WAVE = 1,
		MP3 = 2
	};

	class GT_API Sound
	{
	public:
		Sound();
		Sound(const Sound&);
		virtual ~Sound();

		bool Init(const wstring aFile, SoundManagerPtr apSoundManager);
		virtual bool Play();

	private:
		bool LoadWaveFile(const wstring aFile, SoundManagerPtr apSoundManager);
		void PlayWave();

		IDirectSoundBuffer8* mpSecondaryBuffer;
	};
};

#endif