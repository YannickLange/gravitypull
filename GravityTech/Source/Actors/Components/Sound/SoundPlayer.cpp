#include "SoundPlayer.h"
#include "Sound.h"

namespace GT
{
	SoundPlayer::SoundPlayer() :
		mpSound(NULL)
	{

	}

	SoundPlayer::~SoundPlayer()
	{
		mpSound = NULL;
	}

	void SoundPlayer::Init(const SoundPtr apSound)
	{
		mpSound = apSound;
	}

	void SoundPlayer::Play()
	{
		mpSound->Play();
	}
};