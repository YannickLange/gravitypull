#ifndef COMPONENTTYPES_H_
#define COMPONENTTYPES_H_

namespace GT
{
	enum ComponentType
	{
		NONE = 0,
		TRANSFORM = 1,
		GEOMETRY = 2,
		MATERIAL = 3,
		RENDER = 4,
		RENDERMESH = 5
	};
}

#endif