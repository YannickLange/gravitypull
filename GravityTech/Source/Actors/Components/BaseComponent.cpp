#include "BaseComponent.h"

namespace GT
{
	BaseComponent::BaseComponent() :
		mType(ComponentType::NONE),
		mbEnabled(true)
	{
	}

	BaseComponent::BaseComponent(const ComponentType aType) :
		mType(aType)
	{
		
	}

	BaseComponent::~BaseComponent()
	{
		mType = ComponentType::NONE;
	}

	void BaseComponent::SetActor(const BaseActorPtr& apActor)
	{
		mpActor = apActor;
	}

	void BaseComponent::SetEnabled(const bool abEnabled)
	{
		mbEnabled = abEnabled;
	}

	void BaseComponent::Enable()
	{
		mbEnabled = true;
	}

	void BaseComponent::Disable()
	{
		mbEnabled = false;
	}

	const ComponentType BaseComponent::GetType() const
	{
		return mType;
	}

	BaseActorPtr BaseComponent::GetActor() const
	{
		return mpActor.lock();
	}

	BaseComponentPtr BaseComponent::GetThis()
	{
		return shared_from_this();
	}
};