#include "SpriteRender.h"
#include "Actors/BaseActor.h"
#include "Managers/DeviceManager.h"
#include "Actors/Components/Common/Transform.h"
#include "Actors/Components/Geometry/PlaneMesh.h"
#include "Graphics/Texture2D.h"

namespace GT
{
	SpriteRender::SpriteRender() :
		BaseComponent()
	{

	}

	SpriteRender::~SpriteRender()
	{

	}
		
	bool SpriteRender::Init(const SpriteMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager)
	{
		mpMaterial = apMaterial;
		mpPlane.reset(new PlaneMesh());
		mpPlane->Init(apDeviceManager->GetDevice(), Vector2(200));

		D3D11_BLEND_DESC blendDesc;
		ZeroMemory( &blendDesc, sizeof( blendDesc ) );
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;

		apDeviceManager->GetDevice()->CreateBlendState( &blendDesc, &mpAlphaBlendState );

		return true;
	}

	void SpriteRender::Draw(const Matrix& aOrthoGraphic, const DeviceManagerPtr& apDeviceManager)
	{
		mpPlane->PreDraw(apDeviceManager->GetDeviceContext());
		
		ID3D11BlendState* pPrevBlendState;
		UINT nSampleMask;
		float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
		apDeviceManager->GetDeviceContext()->OMGetBlendState(&pPrevBlendState, blendFactor, &nSampleMask);  
		apDeviceManager->GetDeviceContext()->OMSetBlendState(mpAlphaBlendState, blendFactor, 0xFFFFFFFF);
		
		Matrix translation = XMMatrixTranslation(0.0f, 0.0f, 0.0f);
		Matrix rotationZ = XMMatrixRotationZ(0.0f);
		Matrix scale = XMMatrixScaling(1.0f , 1.0f, 0.0f);
		Matrix transform = scale * rotationZ * translation;

		mpMaterial->SetWorld(aOrthoGraphic);
		mpMaterial->SetWorldViewProjection(aOrthoGraphic * transform);
		mpMaterial->Draw(6, apDeviceManager->GetDeviceContext());

		apDeviceManager->GetDeviceContext()->OMSetBlendState(pPrevBlendState, blendFactor, nSampleMask);
	}
};