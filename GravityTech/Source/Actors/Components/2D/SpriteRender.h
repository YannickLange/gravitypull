#ifndef SPRITERENDER_H_
#define SPRITERENDER_H_

#include "GPECore.h"
#include "Actors/Components/BaseComponent.h"
#include "Actors/Components/Material/Materials.h"

namespace GT
{
	class GT_API SpriteRender : public BaseComponent
	{
	public:
		SpriteRender();
		virtual ~SpriteRender();
		
		virtual bool Init(const SpriteMaterialPtr& apMaterial, const DeviceManagerPtr& apDeviceManager);
		virtual void Draw(const Matrix& aOrthoGraphic, const DeviceManagerPtr& apDeviceManager);

	private:
		SpriteMaterialPtr mpMaterial;
		PlaneMeshPtr mpPlane;
		ID3D11BlendState* mpAlphaBlendState;
	};
};

#endif