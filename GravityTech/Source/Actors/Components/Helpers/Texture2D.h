#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include <D3D11.h>
#include <string>

#include "Scene/BaseScene.h"

namespace GT
{
	class Texture2D
	{
	public:
		Texture2D();
		virtual ~Texture2D();
		bool Init(std::wstring a_File, BaseScene* a_Scene);

	private:
		ID3D11ShaderResourceView* m_pD3D11Texture;
		ID3D11SamplerState* m_pD3D11TexSamplerState;

	};
};

#endif