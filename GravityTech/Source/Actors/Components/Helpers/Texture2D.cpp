#include "Texture2D.h"
#include "Utility/Utils.h"
#include <d3dx11.h>

namespace GPECore
{
	Texture2D::Texture2D() :
		m_pD3D11Texture(nullptr),
		m_pD3D11TexSamplerState(nullptr)
	{

	}

	Texture2D::~Texture2D()
	{
		SAFE_RELEASE(m_pD3D11Texture);
		SAFE_RELEASE(m_pD3D11TexSamplerState);
	}

	bool Texture2D::Init(std::wstring a_File, BaseScene* a_Scene)
	{
		HRESULT result;
		ID3D11Device* device = a_Scene->GetDeviceManager()->GetDevice();
		result = D3DX11CreateShaderResourceViewFromFile(device,
			WStringToLPCWSTR(a_File), NULL, NULL, &m_pD3D11Texture, NULL);

		if(FAILED(result))
		{
			return false;
		}

		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    
		//Create the Sample State
		result = device->CreateSamplerState(&sampDesc, &m_pD3D11TexSamplerState);

		if(FAILED(result))
		{
			return false;
		}

		return true;
	}
};