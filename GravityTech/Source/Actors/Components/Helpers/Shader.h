#ifndef SHADER_H_
#define SHADER_H_

#include <string>
//#include <d3dx11Effect.h>

namespace GPECore
{
	class Shader
	{
	public:
		Shader();
		virtual ~Shader();

		bool Init(std::wstring a_File, ID3D11Device* a_Device);

		//ID3DX11Effect* GetFX();

	private:
		//ID3DX11Effect* m_FX;
	};
};

#endif