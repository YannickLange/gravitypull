#ifndef BASEACTOR_H_
#define BASEACTOR_H_

#include "GPECore.h"
#include "Actors/Components/Common/Transform.h"

namespace GT
{

	class GT_API BaseActor : public enable_shared_from_this<BaseActor>
	{
	public:
		BaseActor();
		BaseActor(wstring aName, UINT aID);
		virtual ~BaseActor();

		virtual bool Init(wstring aName, UINT aID);
		virtual void Update();
		virtual void Release();

		void AddChild(const BaseActorPtr& aChild);
		void AddComponent(const BaseComponentPtr& aComponent);
		void RemoveComponent(const BaseComponentPtr& aComponent);

		//Setters
		void SetID(const UINT aID);
		void SetName(const wstring aName);
		void SetTransform(const TransformPtr apTransform);
		void SetParent(const BaseActorPtr& apParent);

		//Getters
		const UINT GetID() const;
		wstring GetName() const;
		TransformPtr GetTransform() const;
		BaseActorPtr GetParent() const;
		BaseActorPtrVec& GetChilds();

		//template<class CompType> //TODO
		//weak_ptr<CompType> 

	protected:
		BaseComponentPtrVec mComponents;
		BaseActorPtrVec mChilds;
		BaseActorPtr mpParent;

		wstring mName;
		UINT mID;
		TransformPtr mpTransform;

	private:
		void InitTransform();
		BaseActorPtr GetThis();
	};
};

#endif