#include "BaseActor.h"
#include "Components/BaseComponent.h"

namespace GT
{
	BaseActor::BaseActor() :
		mID(UINT_MAX),
		mName(L"")
	{
	}

	BaseActor::BaseActor(wstring aName, UINT aID) :
		mID(aID),
		mName(aName)
	{
	}

	BaseActor::~BaseActor()
	{
		//TODO remove all the components
	}

	bool BaseActor::Init(wstring aName, UINT aID)
	{
		mName = aName;
		mID = aID;
		InitTransform();
		return true;
	}

	void BaseActor::Update()
	{
		for(BaseActorPtrVec::iterator it = mChilds.begin(); it != mChilds.end(); it++)
		{
			it->get()->Update();
		}
	}

	void BaseActor::Release()
	{
	}
	
	void BaseActor::AddChild(const BaseActorPtr& aChild)
	{
		mChilds.push_back(aChild);
	}
	
	void BaseActor::AddComponent(const BaseComponentPtr& aComponent)
	{
		aComponent->SetActor(GetThis());
		mComponents.push_back(aComponent);
	}

	void BaseActor::RemoveComponent(const BaseComponentPtr& aComponent)
	{
		//mComponents.clear( //TODO
	}

	void BaseActor::SetID(const UINT aID)
	{
		mID = aID;
	}

	void BaseActor::SetName(const wstring aName)
	{
		mName = aName;
	}

	void BaseActor::SetTransform(const TransformPtr apTransform)
	{
		mpTransform = apTransform;
	}

	void BaseActor::SetParent(const BaseActorPtr& apParent)
	{
		mpParent = apParent;
	}

	//Getters
	const UINT BaseActor::GetID() const
	{
		return mID;
	}

	wstring BaseActor::GetName() const
	{
		return mName;
	}

	TransformPtr BaseActor::GetTransform() const
	{
		return mpTransform;
	}

	BaseActorPtr BaseActor::GetParent() const
	{
		return mpParent;
	}

	BaseActorPtrVec& BaseActor::GetChilds() 
	{
		return mChilds;
	}

	BaseActorPtr BaseActor::GetThis()
	{
		return shared_from_this();
	}

	void BaseActor::InitTransform()
	{
		mpTransform.reset(new Transform);
		AddComponent(mpTransform);
	}
}