#include "BaseCamera.h"

namespace GT
{
	BaseCamera::BaseCamera()
	{
		mPosition = Vector3(0.0f);
		mRotation = Vector3(0.0f);
	}

	BaseCamera::~BaseCamera()
	{

	}

	void BaseCamera::Init(Vector3 aStartPosition, Vector3 aStartRotation)
	{
		mPosition = aStartPosition;
		mRotation = aStartRotation;
	}

	void BaseCamera::Update()
	{
		Vector3 up, position, lookat, rotation;
		Matrix rotationMatrix;

		//Setup the vector that points upwards
		up.x = 0.0f;
		up.y = 1.0f;
		up.z = 0.0f;

		//Setup the position of the camera in the world
		position = mPosition;

		// Setup where the camera is looking by default.
		lookat.x = 0.0f;
		lookat.y = 0.0f;
		lookat.z = 1.0f;

		// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
		rotation.x = mRotation.x * 0.0174532925f;
		rotation.y = mRotation.y * 0.0174532925f;
		rotation.z = mRotation.z * 0.0174532925f;

		// Create the rotation matrix from the yaw, pitch, and roll valu
		rotationMatrix = XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);

		// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
		lookat = XMVector3TransformCoord(lookat, rotationMatrix);
		up = XMVector3TransformCoord(up, rotationMatrix);

		// Translate the rotated camera position to the location of the viewer.
		lookat = position + lookat;

		// Finally create the view matrix from the three updated vectors.
		mViewMatrix = XMMatrixLookAtLH(position, lookat, up);
	}

	void BaseCamera::SetPostion(const Vector3 aPosition)
	{
		mPosition = aPosition;
	}

	void BaseCamera::SetRotation(const Vector3 aRotation)
	{
		mRotation = aRotation;
	}

	Vector3 BaseCamera::GetPosition()
	{
		return mPosition;
	}

	Vector3 BaseCamera::GetRotation()
	{
		return mRotation;
	}

	Matrix BaseCamera::GetViewMatrix()
	{
		return mViewMatrix;
	}
};