#ifndef FIRSTPERSONCAMERA_H_
#define FIRSTPERSONCAMERA_H_

#include "Actors/BaseActor.h"
#include "GPECore.h"

namespace GT
{
	class GT_API FirstPersonCamera : public BaseActor
	{
	public:
		FirstPersonCamera();
		virtual ~FirstPersonCamera();
		
		bool Init(BaseScenePtr apScene, Vector3 aStartPosition, Vector3 aStartRotation);
		void Update();
		
		void SetMoveSpeed(const float aMoveSpeed);

		const Matrix GetViewMatrix() const;
	private:
		Vector3 mPosition;
		Vector3 mRotation;
		BaseScenePtr mpScene;
		Matrix mViewMatrix;
		float mMoveSpeed;
		float mMoveLeftRight;
		float mMoveBackForward;
		float mPitch;
		float mYaw;
		Vector3 mForward;
		Vector3 mRight;
		Vector3 mUp;
	};
};

#endif