#ifndef BASECAMERA_H_
#define BASECAMERA_H_

#include "Actors/BaseActor.h"
#include "GPECore.h"

namespace GT
{
	class GT_API BaseCamera : public BaseActor
	{
	public:
		BaseCamera();
		virtual ~BaseCamera();

		virtual void Init(Vector3 aStartPosition, Vector3 aStartRotation);
		virtual void Update();

		virtual void SetPostion(const Vector3 aPosition);
		virtual void SetRotation(const Vector3 aRotation);

		virtual Vector3 GetPosition();
		virtual Vector3 GetRotation();
		virtual Matrix GetViewMatrix();

	protected:
		Vector3 mPosition;
		Vector3 mRotation;
		Matrix mViewMatrix;
	};
};

#endif