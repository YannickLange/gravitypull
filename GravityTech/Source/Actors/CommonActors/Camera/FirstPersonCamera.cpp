#include "FirstPersonCamera.h"
#include "Scene/BaseScene.h"
#include "Managers/InputManager.h"



namespace GT
{
	const static Vector3 DefaultForward = Vector3(.0f, .0f, 1.0f);
	const static Vector3 DefaultRight = Vector3(1.0f, .0f, .0f);
	const static Vector3 DefaultUp = Vector3(.0f, 1.0f, .0f);

	FirstPersonCamera::FirstPersonCamera() :
		BaseActor(),
		mMoveSpeed(1.0f)
	{
		mRight = Vector3(DefaultRight);
		mForward = Vector3(DefaultForward);
		mUp = Vector3(DefaultUp);
		mYaw = 0.0f;
		mPitch = 0.0f;
		mMoveSpeed = 0.1f;
		mMoveBackForward = 0.0f;
		mMoveLeftRight = 0.0f;
	}

	FirstPersonCamera::~FirstPersonCamera()
	{

	}
		
	bool FirstPersonCamera::Init(BaseScenePtr apScene, Vector3 aStartPosition, Vector3 aStartRotation)
	{
		bool result; 
		result = BaseActor::Init(L"FPSCam", 666);
		mpScene = apScene;

		mPosition = aStartPosition;
		mRotation = aStartRotation;

		mpTransform->SetPosition(aStartPosition);
		mpTransform->SetRotation(0, 1, 0, 0);
		//mpTransform->SetScale(3.0f);
		return result;
	}

	void FirstPersonCamera::Update()
	{
		InputManagerPtr inputmanager = mpScene->GetInputManager();
		float movespeed = mMoveSpeed;
		
		if(inputmanager->ButtonDown(KeyBoardKey::KEY_LSHIFT) || inputmanager->ButtonDown(KeyBoardKey::KEY_RSHIFT))
		{
			movespeed *= 4;
		}

		//Move left
		if(inputmanager->ButtonDown(KeyBoardKey::KEY_A) || inputmanager->ButtonDown(KeyBoardKey::KEY_LEFT))
		{
			mMoveLeftRight -= movespeed;
		}

		//Move right
		if(inputmanager->ButtonDown(KeyBoardKey::KEY_D) || inputmanager->ButtonDown(KeyBoardKey::KEY_RIGHT))
		{
			mMoveLeftRight += movespeed;
		}

		//Move forward
		if(inputmanager->ButtonDown(KeyBoardKey::KEY_W) || inputmanager->ButtonDown(KeyBoardKey::KEY_UP))
		{
			mMoveBackForward+= movespeed;
		}

		//Move back
		if(inputmanager->ButtonDown(KeyBoardKey::KEY_S) || inputmanager->ButtonDown(KeyBoardKey::KEY_DOWN))
		{
			mMoveBackForward -= movespeed;
		}

		/*MousePtr mouse = inputmanager->GetMouse();
		if(mouse->MovedMouse())
		{
			mYaw += mouse->GetPositionXDelta() * 0.001f;
			mPitch += mouse->GetPositionYDelta() * 0.001f;
		}*/

		Matrix rotation;
		Vector3 target;

		rotation = XMMatrixRotationRollPitchYaw(mPitch, mYaw, 0);
		target = XMVector3TransformCoord(DefaultForward, rotation);
		target = XMVector3Normalize(target);

		XMMATRIX RotateYTempMatrix;
		RotateYTempMatrix = XMMatrixRotationY(mYaw);	//Fill in yaw here

		mRight = XMVector3TransformCoord(DefaultRight, rotation);
		mUp = XMVector3TransformCoord(DefaultUp, rotation);
		mForward = XMVector3TransformCoord(DefaultForward, rotation);

		mPosition += mMoveLeftRight * mRight;
		mPosition += mMoveBackForward * mForward;

		mMoveLeftRight = 0.0f;
		mMoveBackForward = 0.0f;

		target = mPosition + target;	

		mViewMatrix = XMMatrixLookAtLH(mPosition, target, mUp);
		
		mpTransform->SetPosition(mPosition);
		/*

		Vector3 up, position, lookat, rotation;
		Matrix rotationMatrix;

		//Setup the vector that points upwards
		up.x = 0.0f;
		up.y = 1.0f;
		up.z = 0.0f;

		//Setup the position of the camera in the world
		position.x = mPosition.x;
		position.y = mPosition.y;
		position.z = mPosition.z;

		// Setup where the camera is looking by default.
		lookat.x = 0.0f;
		lookat.y = 0.0f;
		lookat.z = 1.0f;

		// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
		rotation.x = mRotation.x * 0.0174532925f;
		rotation.y = mRotation.y * 0.0174532925f;
		rotation.z = mRotation.z * 0.0174532925f;

		// Create the rotation matrix from the yaw, pitch, and roll valu
		rotationMatrix = XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);

		// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
		lookat = XMVector3TransformCoord(lookat, rotationMatrix);
		up = XMVector3TransformCoord(up, rotationMatrix);

		// Translate the rotated camera position to the location of the viewer.
		lookat += position;

		// Finally create the view matrix from the three updated vectors.
		mViewMatrix = XMMatrixLookAtLH(position, lookat, up);*/
	}

	void FirstPersonCamera::SetMoveSpeed(const float aMoveSpeed)
	{
		mMoveSpeed = aMoveSpeed;
	}

	const Matrix FirstPersonCamera::GetViewMatrix() const
	{
		return mViewMatrix;
	}
};