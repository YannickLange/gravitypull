#include "BaseScene.h"
#include "Utility/Utils.h"
#include "Actors/Components/Renderers/SkyboxRender.h"
#include "Graphics/Texture2D.h"
#include <D3DX11.h>
#include "Managers/Managers.h"
#include "Actors/Actors.h"
#include "Actors/Components/Components.h"

namespace GT
{

	BaseScene::BaseScene() :
		BaseActor()
	{
	}

	BaseScene::~BaseScene()
	{
	}

	bool BaseScene::PreInit(ManagersPtr apManagers)
	{
		mpManagers = apManagers;
		return true;
	}

	bool BaseScene::Init()
	{
		BaseActor::Init(L"Scene", mActorsCurrentID);
		mActorsCurrentID++;

		mpCamera.reset(new FirstPersonCamera);
		mpCamera->Init(GetThis(), Vector3(0, 0, -100), Vector3(0, 0, 0));
		return true;
	}

	void BaseScene::Update(double aDeltaTime, float aElapsedTime)
	{
		BaseActor::Update();

		mpCamera->Update();
		
		mViewMatrix = mpCamera->GetViewMatrix();
		mProjectionMatrix = GetDeviceManager()->GetProjectionMatrix();
		mWorldMatrix = GetDeviceManager()->GetWorldMatrix();
		mOrthoGraphicMatrix = GetDeviceManager()->GetOrthoMatrix();
	}

	void BaseScene::Draw(double aDeltaTime, float aElapsedTime)
	{
		GetDeviceManager()->Clear(Color(0.0f, 0.0f, 0.0f, 0.0f));
		
		Matrix transform = mpCamera->GetTransform()->GetTransformMatrix();
		mpSkybox->GetMaterial()->SetWorld<Matrix>(transform);
		mpSkybox->GetMaterial()->SetWorldViewProjection<Matrix>(transform * mViewMatrix * mProjectionMatrix);
		mpSkybox->Draw(GetDeviceManager());

		GetRenderManager()->Draw(mWorldMatrix, mViewMatrix, mProjectionMatrix, GetDeviceManager());

		GetDeviceManager()->Present();
	}

	void BaseScene::Release()
	{
		GetRenderManager()->Release();

		for(BaseActorPtrMap::iterator it = mActors.begin(); it != mActors.end(); ++it)
		{
			it->second->Release();
		}
		mActors.clear();
	}

	BaseActorPtr BaseScene::CreateActor(const wstring& aName, const BaseActorPtr& apParent)
	{
		mActorsCurrentID++;

		BaseActorPtr actor(new BaseActor);
		actor->Init(aName, mActorsCurrentID);
		
		//Set parent
		if(apParent)
		{
			actor->SetParent(apParent);
			apParent->AddChild(actor);
		}
		else
		{
			actor->SetParent(GetThis());
			AddChild(actor);
		}

		mActors.insert(make_pair(mActorsCurrentID, actor));
		
		return actor;
	}

	void BaseScene::SetSkybox(const wstring& aFileName)
	{
		if(!mpSkybox)
		{
			mpSkybox.reset(new SkyboxRender);
			MeshPtr mesh(new Mesh);
			mesh->Init(L"../GravityTech/Assets/Geometry/Sphere.obj", GetDeviceManager()->GetDevice());
			EffectPtr effect(new Effect);
			effect->Init(L"../GravityTech/Assets/Effects/Skybox.fxo", GetDeviceManager());
			SkyboxMaterialPtr material(new SkyboxMaterial);
			material->Init(GetDeviceManager(), effect);
			mpSkybox->Init(mesh, material, GetDeviceManager());
			
		}

		//Texture2DPtr texture(new Texture2D);
		//texture->Init(aFileName, GetDeviceManager());
		//Tell D3D we will be loading a cube texture
		ID3D11ShaderResourceView* smrv;
		D3DX11_IMAGE_LOAD_INFO loadSMInfo;
		loadSMInfo.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

		//Load the texture
		ID3D11Texture2D* SMTexture = 0;
		D3DX11CreateTextureFromFile(GetDeviceManager()->GetDevice(), L"Assets\\Textures\\skymap.dds",	//TODO
			&loadSMInfo, 0, (ID3D11Resource**)&SMTexture, 0);

		//Create the textures description
		D3D11_TEXTURE2D_DESC SMTextureDesc;
		SMTexture->GetDesc(&SMTextureDesc);

		//Tell D3D We have a cube texture, which is an array of 2D textures
		D3D11_SHADER_RESOURCE_VIEW_DESC SMViewDesc;
		SMViewDesc.Format = SMTextureDesc.Format;
		SMViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		SMViewDesc.TextureCube.MipLevels = SMTextureDesc.MipLevels;
		SMViewDesc.TextureCube.MostDetailedMip = 0;

		//Create the Resource view
		GetDeviceManager()->GetDevice()->CreateShaderResourceView(SMTexture, &SMViewDesc, &smrv);

		mpSkybox->GetMaterial()->SetSkyboxTextureCube<ID3D11ShaderResourceView*>(smrv);
	}

	ManagersPtr BaseScene::GetManagers() const
	{
		return mpManagers;
	}

	DeviceManagerPtr BaseScene::GetDeviceManager() const
	{
		return mpManagers->GetDeviceManager();
	}

	GeometryManagerPtr BaseScene::GetGeometryManager() const
	{
		return mpManagers->GetGeometryManager();
	}

	RenderManagerPtr BaseScene::GetRenderManager() const
	{
		return mpManagers->GetRenderManager();
	}

	SceneManagerPtr BaseScene::GetSceneManager() const
	{
		return mpManagers->GetSceneManager();
	}

	InputManagerPtr BaseScene::GetInputManager() const
	{
		return mpManagers->GetInputManager();
	}

	SoundManagerPtr BaseScene::GetSoundManager() const
	{
		return mpManagers->GetSoundManager();
	}

	BaseScenePtr BaseScene::GetThis()
	{
		return static_pointer_cast<BaseScene, BaseActor>(shared_from_this());
	}
};