#ifndef BASESCENE_H_
#define BASESCENE_H_

#include "GPECore.h"
//#include "Managers/Managers.h"
#include "Actors/BaseActor.h"
#include "Actors/Actors.h"
#include "Actors/Components/Components.h"

namespace GT
{
	class GT_API BaseScene : public BaseActor
	{
	public:
		BaseScene();
		virtual ~BaseScene();

		bool PreInit(ManagersPtr apManagers);
		virtual bool Init();
		virtual void Update(double aDeltaTime, float aElapsedTime);
		virtual void Draw(double aDeltaTime, float aElapsedTime);
		virtual void Release();

		//Create functions
		BaseActorPtr CreateActor(const wstring& aName = L"Actor", const BaseActorPtr& apParent = NULL);
	
		//Setters
		void SetSkybox(const wstring& aFileName);

		//Getters
		ManagersPtr GetManagers() const;
		DeviceManagerPtr GetDeviceManager() const;
		GeometryManagerPtr GetGeometryManager() const;
		RenderManagerPtr GetRenderManager() const;
		SceneManagerPtr GetSceneManager() const;
		InputManagerPtr GetInputManager() const;
		SoundManagerPtr GetSoundManager() const;

		
	protected:
		ManagersPtr mpManagers;
		FirstPersonCameraPtr mpCamera;

		Matrix mViewMatrix;
		Matrix mProjectionMatrix;
		Matrix mWorldMatrix;
		Matrix mOrthoGraphicMatrix;
	
	private:
		BaseScenePtr GetThis();
		BaseActorPtrMap mActors;
		UINT mActorsCurrentID; //This is not the size of the actor list
		SkyboxRenderPtr mpSkybox;
	};
};

#endif