#ifndef SYSTEMVIEW_H_
#define SYSTEMVIEW_H_

#include "../Export.h"
#include "../Common.h"
#include "SystemComponent.h"
#include "GPECore.h"

namespace GT
{
	//Forward declarations
	class ISystemView;
	class SystemView;
	
	//Typedefinitions
	typedef std::shared_ptr<ISystemView> ISystemViewPtr;
	typedef std::shared_ptr<SystemView> SystemViewPtr;

	//-----------------------------------------------------------------------------
	// Interface view
	//-----------------------------------------------------------------------------
	class GT_API ISystemView
	{
	public:
		virtual bool Init(const GravityTechAppPtr& apApp) = 0;
		virtual void Update(const GTTime& aTime) = 0;
	};

	//-----------------------------------------------------------------------------
	// BaseView
	// This holds all the components and updates them in order that they are added
	//-----------------------------------------------------------------------------
	class GT_API SystemView : public ISystemView
	{
	public:
		virtual bool Init(const GravityTechAppPtr& apApp);
		virtual void Update(const GTTime& aTime);
		virtual void AddViewComponent(const ISystemComponentPtr& apComponent);

	protected:
		ISystemComponentPtrVec mViewComponents;
	};

	//-----------------------------------------------------------------------------
	// HumanView 
	//-----------------------------------------------------------------------------
	class GT_API HumanView : public SystemView
	{
	public:
		virtual bool Init(const GravityTechAppPtr& apApp, const BaseScenePtr& apScene);
		virtual void Update(const GTTime& aTime);
	private:
		ManagersPtr mpManagers;
	};
};

#endif
