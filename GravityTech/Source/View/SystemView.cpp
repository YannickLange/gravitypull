#include "SystemView.h"

#include "GravityTech/GravityTechApp.h"
#include "Managers/Managers.h"
#include "Scene/BaseScene.h"

namespace GT
{
	//-----------------------------------------------------------------------------
	// BaseView
	//-----------------------------------------------------------------------------
	bool SystemView::Init(const GravityTechAppPtr& apApp)
	{
		return true;
	}

	void SystemView::Update(const GTTime& aTime)
	{
		for(ISystemComponentPtrVec::iterator it = mViewComponents.begin(); it != mViewComponents.end(); it++)
		{
			it->get()->Update(aTime);
		}
	}

	void SystemView::AddViewComponent(const ISystemComponentPtr& apComponent)
	{
		mViewComponents.push_back(apComponent);
	}

	//-----------------------------------------------------------------------------
	// HumanView 
	//-----------------------------------------------------------------------------
	bool HumanView::Init(const GravityTechAppPtr& apApp, const BaseScenePtr& apScene)
	{
		bool result = true; 

		mpManagers.reset(new Managers());
		result = mpManagers->Init();
		if(!result) return result;

		result = mpManagers->GetDeviceManager()->Init(1280, 720, true, apApp->mWindowHandle);
		if(!result) return result;

		result = mpManagers->GetGeometryManager()->Init(mpManagers->GetDeviceManager());
		if(!result) return result;

		result = mpManagers->GetRenderManager()->Init(mpManagers);
		if(!result) return result;

		result = mpManagers->GetSceneManager()->Init(mpManagers);
		if(!result) return result;

		result = mpManagers->GetInputManager()->Init(apApp->mInstance, apApp->mWindowHandle);
		if(!result) return result;

		result = mpManagers->GetSoundManager()->Init(apApp->mWindowHandle);
		if(!result) return result;

		//Finally initialize the scene
		mpManagers->GetSceneManager()->SetCurrentScene(apScene);

		return result;
	}

	void HumanView::Update(const GTTime& aTime)
	{
		mpManagers->GetInputManager()->Update();
		mpManagers->GetSceneManager()->GetCurrentScene()->Update(0, 0);
		mpManagers->GetInputManager()->UpdatePreviousKeys();

		mpManagers->GetDeviceManager()->Clear(Color(0.5f, 0.5f, 0.5f, 1.0f));
		mpManagers->GetSceneManager()->GetCurrentScene()->Draw(0.0f, 0.0f);
		mpManagers->GetDeviceManager()->Present();
	}
};