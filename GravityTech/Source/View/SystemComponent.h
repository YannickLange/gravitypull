#ifndef VIEWCOMPONENT_H_
#define VIEWCOMPONENT_H_

#include "../Common.h"
#include "../Export.h"

namespace GT
{
	//Forward declarations
	class ISystemComponent;

	//Typedefinitions
	typedef std::shared_ptr<ISystemComponent> ISystemComponentPtr;
	typedef std::vector<ISystemComponentPtr> ISystemComponentPtrVec;

	//-----------------------------------------------------------------------------
	// ISystemComponent 
	//-----------------------------------------------------------------------------
	class GT_API ISystemComponent
	{
	public:
		virtual bool Init() = 0;
		virtual void Update(const GTTime& aTime) = 0;
	};
};

#endif