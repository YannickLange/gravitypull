#ifndef GT_H_
#define GT_H_

#ifdef GT_EXPORTS
#define GT_API __declspec(dllexport)
#else
#define GT_API __declspec(dllimport)
#endif

#include <memory>
#include <vector>
#include <string>
#include <map>
#include <Windows.h>
//#include "Graphics/DXInclude.h"
//#include <D3DX11.h>

#include "Utility/Utils.h"
#include "Utility/SimpleMath.h"
#include "Utility/GTException.h"
#include "Utility/GTTime.h"

using namespace std;

namespace GT
{
	//GPEngine
	class GPEngine;

	//Graphics
	class Font;
	class Texture2D;
	class Shader;

	//Managers
	class Managers;
	class IManager;
	class DeviceManager;
	class GeometryManager;
	class RenderManager;
	class SceneManager;
	class InputManager;
	class SoundManager;

	//Actors
	class BaseActor;

	//Actors components
	enum ComponentTypes;
	class BaseComponent;
	class Transform;
	class BaseGeometry;
	class Mesh;
	class RenderComponent;
	class SkyboxRender;
	class MeshRender;
	class PlaneMesh;
	class LightComponentPtr;

	//Materials
	class Material;
	class TextureMaterial;

	//Scene
	class BaseScene;

	//Sound
	class SoundPlayer;
	class Sound;
	class Sound2D;
	class Sound3D;
	
	//2D
	class SpriteRender;
	
	//Input
	class Mouse;

	//Common
	class Asset;

	class Scene; //TODO
	typedef shared_ptr<Scene> ScenePtr;
};

namespace GT
{
	//typedefinitions

	//Graphics
	typedef shared_ptr<Font> FontPtr;
	typedef shared_ptr<Texture2D> Texture2DPtr;
	typedef shared_ptr<Shader> ShaderPtr;

	//Managers
	typedef shared_ptr<Managers> ManagersPtr;
	typedef weak_ptr<Managers> ManagersWPtr;
	typedef shared_ptr<IManager> IManagerPtr;
	typedef shared_ptr<DeviceManager> DeviceManagerPtr;
	typedef shared_ptr<GeometryManager> GeometryManagerPtr;
	typedef shared_ptr<RenderManager> RenderManagerPtr;
	typedef shared_ptr<SceneManager> SceneManagerPtr;
	typedef shared_ptr<InputManager> InputManagerPtr;
	typedef shared_ptr<SoundManager> SoundManagerPtr;

	//Actors
	typedef shared_ptr<BaseActor> BaseActorPtr;
	typedef weak_ptr<BaseActor> BaseActorWPtr;

	//Actor components
	typedef shared_ptr<BaseComponent> BaseComponentPtr;
	typedef shared_ptr<Transform> TransformPtr;
	typedef shared_ptr<BaseGeometry> BaseGeometryPtr;
	typedef shared_ptr<Mesh> MeshPtr;
	typedef shared_ptr<RenderComponent> RenderComponentPtr;
	typedef shared_ptr<SkyboxRender> SkyboxRenderPtr;
	typedef shared_ptr<MeshRender> MeshRenderPtr;
	typedef shared_ptr<PlaneMesh> PlaneMeshPtr;
	
	//Materials
	typedef shared_ptr<Material> MaterialPtr;
	typedef shared_ptr<TextureMaterial> TextureMaterialPtr;

	//Scene
	typedef shared_ptr<BaseScene> BaseScenePtr;

	//Sound
	typedef shared_ptr<SoundPlayer> SoundPlayerPtr;
	typedef shared_ptr<Sound> SoundPtr;
	typedef shared_ptr<Sound2D> Sound2DPtr;
	typedef shared_ptr<Sound3D> Sound3DPtr;
	typedef vector<Sound> SoundPtrVec;
	typedef map<wstring, SoundPtr> SoundPtrMap;

	//2D
	typedef shared_ptr<SpriteRender> SpriteRenderPtr;

	//Input
	typedef shared_ptr<Mouse> MousePtr;

	//DirectX
	typedef shared_ptr<ID3D11Device> DevicePtr;
	typedef shared_ptr<ID3D11DeviceContext> DeviceContextPtr;

	/*
	 * Typedefintions for vectors
	 */
	typedef vector<float> FloatVec;
	typedef vector<UINT> UINTVec;
	typedef vector<int> IntVec;
	typedef vector<Vector2> Vector2Vec;
	typedef vector<Vector3> Vector3Vec;
	typedef vector<Vector4> Vector4Vec;

	typedef vector<BaseComponentPtr> BaseComponentPtrVec;
	typedef vector<BaseActorPtr> BaseActorPtrVec;
	typedef vector<MeshRenderPtr> MeshRenderPtrVec;
	typedef vector<RenderComponentPtr> RenderComponentPtrVec;
	typedef vector<LightComponentPtr> LightComponentPtrVec;
	/*
	 * Typedefinitions for maps
	 */
	typedef map<UINT, BaseComponentPtr> BaseComponentPtrMap;
	typedef map<UINT, BaseActorPtr> BaseActorPtrMap;
	typedef map<wstring, MeshPtr> MeshPtrMap;
	typedef shared_ptr<MeshPtrMap> MeshMapPtr;
};
#endif