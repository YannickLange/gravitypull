#ifndef MANAGERS_H_
#define MANAGERS_H_

#include "GPECore.h"

//Manager includes
#include "Managers/DeviceManager.h"
#include "Managers/GeometryManager.h"
#include "Managers/RenderManager.h"
//#include "Managers/FontManager.h"
//#include "Managers/TextureManager.h"
#include "Managers/SceneManager.h"
#include "Managers/InputManager.h"
#include "Managers/SoundManager.h"

namespace GT
{
	class GT_API Managers
	{
	public:
		Managers();
		virtual ~Managers();

		bool Init();

		void SetSkybox(const int string);
		//Manager getters
		DeviceManagerPtr GetDeviceManager() const;
		GeometryManagerPtr GetGeometryManager() const;
		RenderManagerPtr GetRenderManager() const;
		//FontManagerPtr GetFontManager() const;
		//TextureManagerPtr GetTextureManager() const;
		SceneManagerPtr GetSceneManager() const;
		InputManagerPtr GetInputManager() const;
		SoundManagerPtr GetSoundManager() const;

	private:
		DeviceManagerPtr mpDeviceManager;
		GeometryManagerPtr mpGeometryManager;
		RenderManagerPtr mpRenderManager;
		//FontManagerPtr mpFontManager;
		//TextureManagerPtr mpTextureManager;
		SceneManagerPtr mpSceneManager;
		InputManagerPtr mpInputManager;
		SoundManagerPtr mpSoundManager;
	};
};

#endif