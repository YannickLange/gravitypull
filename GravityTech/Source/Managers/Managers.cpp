#include "Managers.h"

namespace GT
{
	void Managers::SetSkybox(const int string)
	{

	}

	Managers::Managers() :
		mpDeviceManager(NULL),
		mpGeometryManager(NULL),
		mpRenderManager(NULL),
		//mpFontManager(NULL),
		//mpTextureManager(NULL),
		mpSceneManager(NULL),
		mpInputManager(NULL),
		mpSoundManager(NULL)
	{
		
	}

	Managers::~Managers()
	{
		mpDeviceManager.reset();
		mpGeometryManager.reset();
		mpRenderManager.reset();
		//mpFontManager.reset();
		//mpTextureManager.reset();
		mpSceneManager.reset();
		mpInputManager.reset();
		mpSoundManager.reset();
	}

	bool Managers::Init()
	{
		mpDeviceManager.reset(new DeviceManager);
		mpGeometryManager.reset(new GeometryManager);
		mpRenderManager.reset(new RenderManager);
		//mpFontManager.reset(new FontManager());
		//mpTextureManager.reset(new TextureManager());
		mpSceneManager.reset(new SceneManager);
		mpInputManager.reset(new InputManager);
		mpSoundManager.reset(new SoundManager);

		return true;
	}

	DeviceManagerPtr Managers::GetDeviceManager() const
	{
		return mpDeviceManager;
	}

	GeometryManagerPtr Managers::GetGeometryManager() const
	{
		return mpGeometryManager;
	}

	RenderManagerPtr Managers::GetRenderManager() const
	{
		return mpRenderManager;
	}

	/*FontManagerPtr Managers::GetFontManager() const
	{
		return mpFontManager;
	}

	TextureManagerPtr Managers::GetTextureManager() const
	{
		return mpTextureManager;
	}*/

	SceneManagerPtr Managers::GetSceneManager() const
	{
		return mpSceneManager;
	}

	InputManagerPtr Managers::GetInputManager() const
	{
		return mpInputManager;
	}

	SoundManagerPtr Managers::GetSoundManager() const
	{
		return mpSoundManager;
	}
}