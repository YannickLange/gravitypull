#include "DeviceManager.h"
#include "Utility/Utils.h"
#include "Actors/Components/Geometry/BaseGeometry.h"

namespace GT
{
	//
	// Public
	//
	DeviceManager::DeviceManager() :
		mpDevice11(NULL),
		mpDeviceContext11(NULL),
		mpSwapChain(NULL),
		mpDepthStencilBuffer(NULL),
		mpRenderTargetView(NULL),
		mpDepthStencilView(NULL),
		mbIsFullScreen(false)
	{
	}

	DeviceManager::~DeviceManager()
	{
		SAFE_RELEASE(mpSwapChain);
		SAFE_RELEASE(mpDepthStencilView);
		SAFE_RELEASE(mpRenderTargetView);
		SAFE_RELEASE(mpDepthStencilBuffer);
		SAFE_RELEASE(mpDevice11);
		SAFE_RELEASE(mpDeviceContext11);
	}

	/**
	 * function:	Init
	 * description:	Initializes the devicemanager.
	 * param[in]:	UINT aWidth			- The width of the window
	 * param[in]:	UINT aHeight		- The height of the window
	 * param[in]:	BOOL abFullscreen	- If fullscreen	//TODO
	 * param[in]:	HWND aHandle		- //TODO: investigate
	 * return		BOOL if succeeded
	 */
	bool DeviceManager::Init(const UINT aWidth, const UINT aHeight, const bool abFullscreen, const HWND aHandle)
	{
		mbIsFullScreen = false;
		mScreenHeight = aHeight;
		mScreenWidth = aWidth;
		mFrameRate = 60;
		mbMultiSamplingEnabled = false;
		mMultiSamplingCount = 4;
		mbDepthStencilBufferEnabled = true;
		mFeatureLevel = D3D_FEATURE_LEVEL_11_0;

		HRESULT result;
		/*UINT createDeviceFlags = 0;

		#if defined(DEBUG) || defined(_DEBUG)  
			createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
		#endif

		D3D_FEATURE_LEVEL featureLevels[] = {
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1
        };*/

		/*ID3D11Device* direct3DDevice = nullptr;
        ID3D11DeviceContext* direct3DDeviceContext = nullptr;

		if (FAILED(result = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevels, 
			ARRAYSIZE(featureLevels), D3D11_SDK_VERSION, &direct3DDevice, &mFeatureLevel, &direct3DDeviceContext)))
        {
            throw GTException("D3D11CreateDevice() failed", result);
        }

        if (FAILED(result = direct3DDevice->QueryInterface(__uuidof(ID3D11Device), reinterpret_cast<void**>(&mpDevice11))))
        {
            throw GTException("ID3D11Device::QueryInterface() failed", result);
        }

        if (FAILED(result = direct3DDeviceContext->QueryInterface(__uuidof(ID3D11DeviceContext), reinterpret_cast<void**>(&mpDeviceContext11))))
        {
            throw GTException("ID3D11Device::QueryInterface() failed", result);
		}

		SAFE_RELEASE(direct3DDevice);
        SAFE_RELEASE(direct3DDeviceContext);

		mpDevice11->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, mMultiSamplingCount, &mMultiSamplingQualityLevels);
        if (mMultiSamplingQualityLevels == 0)
        {
            throw GTException("Unsupported multi-sampling quality");
        }*/

		//Describe our SwapChain Buffer
		/*DXGI_MODE_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));
		bufferDesc.Width = mScreenWidth;
		bufferDesc.Height = mScreenHeight;
		bufferDesc.RefreshRate.Numerator = 60;
		bufferDesc.RefreshRate.Denominator = 1;
		bufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		//Describe our SwapChain
		DXGI_SWAP_CHAIN_DESC swapChainDesc; 
		ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
		swapChainDesc.BufferDesc = bufferDesc;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = 1;
		swapChainDesc.OutputWindow = aHandle; 
		swapChainDesc.Windowed = TRUE; 
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

       
        IDXGIFactory1* dxgiFactory = nullptr;		
		result = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&dxgiFactory);
        if (FAILED(result))
        {
            SAFE_RELEASE(dxgiFactory);
            throw GTException("IDXGIAdapter::GetParent() failed retrieving factory.", result);
        }
		
		IDXGIAdapter1 *dxgiAdapter = nullptr;
		result = dxgiFactory->EnumAdapters1(0, &dxgiAdapter);
        if (FAILED(result))
        {
			SAFE_RELEASE(dxgiAdapter);
			SAFE_RELEASE(dxgiFactory);
            throw GTException("IDXGIDevice::GetParent() failed retrieving adapter.", result);
        }
		
		result = D3D11CreateDeviceAndSwapChain(dxgiAdapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, D3D11_CREATE_DEVICE_DEBUG | D3D11_CREATE_DEVICE_BGRA_SUPPORT,
			NULL, NULL,	D3D11_SDK_VERSION, &swapChainDesc, &mpSwapChain, &mpDevice11, NULL, &mpDeviceContext11);
		if (FAILED(result) || mpSwapChain == NULL || mpDevice11 == NULL || mpDeviceContext11 == NULL )
        {
            SAFE_RELEASE(dxgiAdapter);
            SAFE_RELEASE(dxgiFactory);
            throw GTException("D3D11CreateDeviceAndSwapChain() failed.", result);
        }

        SAFE_RELEASE(dxgiAdapter);
        SAFE_RELEASE(dxgiFactory);

		// Creating backbuffer
		result = mpSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&mpBackBuffer11));
		if (FAILED(result))
        {
            throw GTException("IDXGISwapChain::GetBuffer() failed.", result);
        }

        //mpBackBuffer11->GetDesc(&mBackBufferDesc);
		result = mpDevice11->CreateRenderTargetView(mpBackBuffer11, nullptr, &mpRenderTargetView);
		if (FAILED(result))
        {
            SAFE_RELEASE(mpBackBuffer11);
            throw GTException("IDXGIDevice::CreateRenderTargetView() failed.", result);
        }
		
		CreateDepthStencilView(mScreenWidth, mScreenHeight);*/

		//Describe our SwapChain Buffer
		DXGI_MODE_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));
		bufferDesc.Width = mScreenWidth;
		bufferDesc.Height = mScreenHeight;
		bufferDesc.RefreshRate.Numerator = 60;
		bufferDesc.RefreshRate.Denominator = 1;
		bufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		//Describe our SwapChain
		DXGI_SWAP_CHAIN_DESC swapChainDesc; 
		ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
		swapChainDesc.BufferDesc = bufferDesc;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = 1;
		swapChainDesc.OutputWindow = aHandle; 
		swapChainDesc.Windowed = TRUE; 
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

		// Create DXGI factory to enumerate adapters///////////////////////////////////////////////////////////////////////////
		IDXGIFactory1 *DXGIFactory;

		HRESULT hr = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&DXGIFactory);	

		// Use the first adapter	
		IDXGIAdapter1 *Adapter;

		hr = DXGIFactory->EnumAdapters1(0, &Adapter);

		DXGIFactory->Release();	

		//Create our Direct3D 11 Device and SwapChain//////////////////////////////////////////////////////////////////////////
		hr = D3D11CreateDeviceAndSwapChain(Adapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, D3D11_CREATE_DEVICE_DEBUG |	D3D11_CREATE_DEVICE_BGRA_SUPPORT,
			NULL, NULL,	D3D11_SDK_VERSION, &swapChainDesc, &mpSwapChain, &mpDevice11, NULL, &mpDeviceContext11);

		Init2D(Adapter);

		//Release the Adapter interface
		Adapter->Release();

		//Create our BackBuffer and Render Target
		hr = mpSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (void**)&mpBackBuffer11 );
		hr = mpDevice11->CreateRenderTargetView( mpBackBuffer11, NULL, & mpRenderTargetView );

		//Describe our Depth/Stencil Buffer
		D3D11_TEXTURE2D_DESC depthStencilDesc;

		depthStencilDesc.Width     = mScreenWidth;
		depthStencilDesc.Height    = mScreenHeight;
		depthStencilDesc.MipLevels = 1;
		depthStencilDesc.ArraySize = 1;
		depthStencilDesc.Format    = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilDesc.SampleDesc.Count   = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
		depthStencilDesc.Usage          = D3D11_USAGE_DEFAULT;
		depthStencilDesc.BindFlags      = D3D11_BIND_DEPTH_STENCIL;
		depthStencilDesc.CPUAccessFlags = 0; 
		depthStencilDesc.MiscFlags      = 0;

		//Create the Depth/Stencil View
		mpDevice11->CreateTexture2D(&depthStencilDesc, NULL, &mpDepthStencilBuffer);
		mpDevice11->CreateDepthStencilView(mpDepthStencilBuffer, NULL, &mpDepthStencilView);

		SetViewport(mScreenWidth, mScreenHeight);

		// Initialize the world matrix to the identity matrix.
		mWorldMatrix = XMMatrixIdentity();

		// Create an orthographic projection matrix for 2D rendering.
		SetOrthoMatrix();
		SetProjectionMatrix();
		
		D3D11_BLEND_DESC blendDesc;
		ZeroMemory( &blendDesc, sizeof(blendDesc) );
		D3D11_RENDER_TARGET_BLEND_DESC rtbd;
		ZeroMemory( &rtbd, sizeof(rtbd) );
		rtbd.BlendEnable			 = true;
		rtbd.SrcBlend				 = D3D11_BLEND_SRC_COLOR;
		rtbd.DestBlend				 = D3D11_BLEND_INV_SRC_ALPHA;
		rtbd.BlendOp				 = D3D11_BLEND_OP_ADD;
		rtbd.SrcBlendAlpha			 = D3D11_BLEND_ONE;
		rtbd.DestBlendAlpha			 = D3D11_BLEND_ZERO;
		rtbd.BlendOpAlpha			 = D3D11_BLEND_OP_ADD;
		rtbd.RenderTargetWriteMask	 = D3D10_COLOR_WRITE_ENABLE_ALL;

		blendDesc.AlphaToCoverageEnable = false;
		blendDesc.RenderTarget[0] = rtbd;

		// Describe the Sample State
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory( &sampDesc, sizeof(sampDesc) );
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

		//Create the Sample State
		hr = mpDevice11->CreateSamplerState( &sampDesc, &mpSamplerState2D );
		hr = mpDevice11->CreateBlendState(&blendDesc, &mpTransparency);

		D3D11_RASTERIZER_DESC cmdesc;
		ZeroMemory(&cmdesc, sizeof(D3D11_RASTERIZER_DESC));
		cmdesc.FillMode = D3D11_FILL_SOLID;
		cmdesc.CullMode = D3D11_CULL_BACK;
		cmdesc.FrontCounterClockwise = false;
		hr = mpDevice11->CreateRasterizerState(&cmdesc, &mpCWCullMode);

		InitD2DScreenTexture();

		return true;
	}

	/**
	 * function:	Resize	
	 * description: When the user resizes the window
	 * param[in]	UINT aWidth		- The new width of the window
	 * param[in]	UINT aHeight	- The new height of the window
	 * return		BOOL if succeeded
	 */
	bool DeviceManager::Resize(const UINT aWidth, const UINT aHeight)
	{
		if(mbIsFullScreen)
		{
			return true;
		}

		mScreenWidth = aWidth;
		mScreenHeight = aHeight;

		mpRenderTargetView->Release();
		mpDepthStencilView->Release();

		mpSwapChain->ResizeBuffers(1, aWidth, aHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 2);
		CreateTargetView();
		CreateDepthStencilView(aWidth, aHeight);
		SetViewport(aWidth, aHeight);

		mpDeviceContext11->OMSetRenderTargets( 1, &mpRenderTargetView, mpDepthStencilView);

		return true;
	}

	
	/**
	 * function:	SetViewport	
	 * description: Sets the new size for the viewport, to correctly render everything.
	 * param[in]	UINT aWidth		- The new width of the window
	 * param[in]	UINT aHeight	- The new height of the window
	 */
	void DeviceManager::SetViewport(const UINT aWidth, const UINT aHeight)
	{
		D3D11_VIEWPORT vp;
		vp.Width = (float)mScreenWidth;
		vp.Height = (float)mScreenHeight;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;

		mpDeviceContext11->RSSetViewports( 1, &vp );
	}


	void DeviceManager::RestoreTarget()
	{
		mpDeviceContext11->OMSetRenderTargets( 1, &mpRenderTargetView , mpDepthStencilView);
		SetViewport(mScreenWidth, mScreenHeight);
	}

	void DeviceManager::Clear(const Color& aColor)
	{
		mpDeviceContext11->ClearRenderTargetView(mpRenderTargetView, aColor);
		mpDeviceContext11->ClearDepthStencilView(mpDepthStencilView, D3D11_CLEAR_DEPTH, 1, 0);
		
		//Set our Render Target
		mpDeviceContext11->OMSetRenderTargets( 1, &mpRenderTargetView, mpDepthStencilView );

		//Set the default blend state (no blending) for opaque objects
		mpDeviceContext11->OMSetBlendState(0, 0, 0xffffffff);
	}

	void DeviceManager::Present()
	{
		mpSwapChain->Present(0,0);
	}

	void DeviceManager::SetProjectionMatrix(const float aScreenNear, const float aScreenDepth)
	{
		// Setup the projection matrix.
		float fieldOfView = (float)XM_PI  / 4.0f;
		float screenAspect = (float)mScreenWidth / (float)mScreenHeight;

		// Create the projection matrix for 3D rendering.
		mProjectionMatrix = XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, aScreenNear, aScreenDepth);
	}

	void DeviceManager::SetOrthoMatrix(const float aScreenNear, const float aScreenDepth)
	{
		mOrthoMatrix = XMMatrixOrthographicLH((float) mScreenWidth, (float) mScreenHeight, aScreenNear, aScreenDepth);
	}

	UINT DeviceManager::GetWidth() const 
	{
		return mScreenWidth;
	}

	UINT DeviceManager::GetHeight() const 
	{
		return mScreenHeight;
	}

	ID3D11Device* DeviceManager::GetDevice() const 
	{
		return mpDevice11;
	}

	ID3D11DeviceContext* DeviceManager::GetDeviceContext() const 
	{
		return mpDeviceContext11;
	}

	Matrix DeviceManager::GetProjectionMatrix() const 
	{
		return mProjectionMatrix;
	}

	Matrix DeviceManager::GetWorldMatrix() const 
	{
		return mWorldMatrix;
	}

	Matrix DeviceManager::GetOrthoMatrix() const 
	{
		return mOrthoMatrix;
	}

	//
	// Private
	//
	bool DeviceManager::CreateTargetView()
	{
		ID3D11Texture2D *pBackBuffer;
		mpSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer);
		mpDevice11->CreateRenderTargetView( pBackBuffer, NULL, &mpRenderTargetView);
		pBackBuffer->Release();

		return true;
	}

	bool DeviceManager::CreateDepthStencilView(UINT aWidth, UINT aHeight)
	{
		HRESULT result;

		//Creating depth stencil buffer
		if(true)
		{
			D3D11_TEXTURE2D_DESC depthStencilDesc;
            ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
            depthStencilDesc.Width = aWidth;
            depthStencilDesc.Height = aHeight;
            depthStencilDesc.MipLevels = 1;
            depthStencilDesc.ArraySize = 1;
            depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
            depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
            depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;            

            if (mbMultiSamplingEnabled)
            {
                depthStencilDesc.SampleDesc.Count = mMultiSamplingCount;
                depthStencilDesc.SampleDesc.Quality = mMultiSamplingQualityLevels - 1;
            }
            else
            {
                depthStencilDesc.SampleDesc.Count = 1;
                depthStencilDesc.SampleDesc.Quality = 0;
            }

            if (FAILED(result = mpDevice11->CreateTexture2D(&depthStencilDesc, nullptr, &mpDepthStencilBuffer)))
            {
                throw GTException("IDXGIDevice::CreateTexture2D() failed.", result);
            }

            if (FAILED(result = mpDevice11->CreateDepthStencilView(mpDepthStencilBuffer, nullptr, &mpDepthStencilView)))
            {
                throw GTException("IDXGIDevice::CreateDepthStencilView() failed.", result);
            }
		}

		mpDeviceContext11->OMSetRenderTargets(1, &mpRenderTargetView, mpDepthStencilView);

		return true;
	}

	const ID3D11SamplerState* DeviceManager::GetSamplerState() const
	{
		return NULL;
	}

	const ID3D11BlendState* DeviceManager::GetBlendState() const
	{
		return NULL;
	}

	IDXGIAdapter1* DeviceManager::GetAdapter()
	{
		HRESULT result;

		IDXGIFactory1* dxgiFactory = nullptr;		
        if (FAILED(result = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&dxgiFactory)))
        {
            SAFE_RELEASE(dxgiFactory);
			return NULL;
        }
		
		IDXGIAdapter1 *dxgiAdapter = nullptr;
        if (FAILED(result = dxgiFactory->EnumAdapters1(0, &dxgiAdapter)))
        {
			SAFE_RELEASE(dxgiAdapter);
			SAFE_RELEASE(dxgiFactory);
			return NULL;
        }

		SAFE_RELEASE(dxgiFactory);

		return dxgiAdapter;
	}

	bool DeviceManager::Init2D(IDXGIAdapter1 *Adapter)
	{
		HRESULT hr;

			//Create our Direc3D 10.1 Device///////////////////////////////////////////////////////////////////////////////////////
		hr = D3D10CreateDevice1(Adapter, D3D10_DRIVER_TYPE_HARDWARE, NULL,D3D10_CREATE_DEVICE_DEBUG |	D3D10_CREATE_DEVICE_BGRA_SUPPORT,
			D3D10_FEATURE_LEVEL_9_3, D3D10_1_SDK_VERSION, &mpDevice10	);	

		//Create Shared Texture that Direct3D 10.1 will render on//////////////////////////////////////////////////////////////
		D3D11_TEXTURE2D_DESC sharedTexDesc;	

		ZeroMemory(&sharedTexDesc, sizeof(sharedTexDesc));

		sharedTexDesc.Width = mScreenWidth;
		sharedTexDesc.Height = mScreenHeight;	
		sharedTexDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		sharedTexDesc.MipLevels = 1;	
		sharedTexDesc.ArraySize = 1;
		sharedTexDesc.SampleDesc.Count = 1;
		sharedTexDesc.Usage = D3D11_USAGE_DEFAULT;
		sharedTexDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;	
		sharedTexDesc.MiscFlags = D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;	

		hr = mpDevice11->CreateTexture2D(&sharedTexDesc, NULL, &mpSharedTex11);	

		// Get the keyed mutex for the shared texture (for D3D11)///////////////////////////////////////////////////////////////
		hr = mpSharedTex11->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)& mpKeyedMutex11);	

		// Get the shared handle needed to open the shared texture in D3D10.1///////////////////////////////////////////////////
		IDXGIResource *sharedResource10;
		HANDLE sharedHandle10;	

		hr = mpSharedTex11->QueryInterface(__uuidof(IDXGIResource), (void**)&sharedResource10);

		hr = sharedResource10->GetSharedHandle(&sharedHandle10);	

		sharedResource10->Release();

		// Open the surface for the shared texture in D3D10.1///////////////////////////////////////////////////////////////////
		IDXGISurface1 *sharedSurface10;	

		hr = mpDevice10->OpenSharedResource(sharedHandle10, __uuidof(IDXGISurface1), (void**)(&sharedSurface10));

		hr = sharedSurface10->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&mpKeyedMutex10);	

		// Create D2D factory///////////////////////////////////////////////////////////////////////////////////////////////////
		ID2D1Factory *D2DFactory;	
		hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory), (void**)&D2DFactory);	

		D2D1_RENDER_TARGET_PROPERTIES renderTargetProperties;

		ZeroMemory(&renderTargetProperties, sizeof(renderTargetProperties));

		renderTargetProperties.type = D2D1_RENDER_TARGET_TYPE_HARDWARE;
		renderTargetProperties.pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED);	

		hr = D2DFactory->CreateDxgiSurfaceRenderTarget(sharedSurface10, &renderTargetProperties, &mpRenderTarget2D);

		sharedSurface10->Release();
		D2DFactory->Release();	

		// Create a solid color brush to draw something with		
		hr = mpRenderTarget2D->CreateSolidColorBrush(D2D1::ColorF(1.0f, 1.0f, 0.0f, 1.0f), &mpBrush);

		//DirectWrite///////////////////////////////////////////////////////////////////////////////////////////////////////////
		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory),
			reinterpret_cast<IUnknown**>(&mpDWriteFactory));
		if(FAILED(hr))
		{
			return false;
		}

		hr = mpDWriteFactory->CreateTextFormat(
			L"Script",
			NULL,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			24.0f,
			L"en-us",
			&mpTextFormat
			);

		hr = mpTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		hr = mpTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);

		mpDevice10->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);	
		return true;
	}

	void DeviceManager::InitD2DScreenTexture()
	{
		HRESULT hr;

		//Create the vertex buffer
		VertexPosTex v[] =
		{
			// Front Face
			VertexPosTex(Vector3(-1.0f, -1.0f, -1.0f), Vector2(0.0f, 1.0f)),
			VertexPosTex(Vector3(-1.0f,  1.0f, -1.0f), Vector2(0.0f, 0.0f)),
			VertexPosTex(Vector3( 1.0f,  1.0f, -1.0f), Vector2(1.0f, 0.0f)),
			VertexPosTex(Vector3( 1.0f, -1.0f, -1.0f), Vector2(1.0f, 1.0f)),
		};

		DWORD indices[] = {
			// Front Face
			0,  1,  2,
			0,  2,  3,
		};

		D3D11_BUFFER_DESC indexBufferDesc;
		ZeroMemory( &indexBufferDesc, sizeof(indexBufferDesc) );

		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(DWORD) * 2 * 3;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA iinitData;

		iinitData.pSysMem = indices;
		mpDevice11->CreateBuffer(&indexBufferDesc, &iinitData, &mpIndexBuffer2D);


		D3D11_BUFFER_DESC vertexBufferDesc;
		ZeroMemory( &vertexBufferDesc, sizeof(vertexBufferDesc) );

		vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		vertexBufferDesc.ByteWidth = sizeof( VertexPosTex ) * 4;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA vertexBufferData; 

		ZeroMemory( &vertexBufferData, sizeof(vertexBufferData) );
		vertexBufferData.pSysMem = v;
		hr = mpDevice11->CreateBuffer( &vertexBufferDesc, &vertexBufferData, &mpVertBuffer2D);

		//Create A shader resource view from the texture D2D will render to,
		//So we can use it to texture a square which overlays our scene
		mpDevice11->CreateShaderResourceView(mpSharedTex11, NULL, &mpTexture2D);
	}

	void DeviceManager::RenderText(std::wstring text, SpriteMaterial* apSpriteMaterial)
	{
		//Release the D3D 11 Device
		mpKeyedMutex11->ReleaseSync(0);

		//Use D3D10.1 device
		mpKeyedMutex10->AcquireSync(0, 5);			

		//Draw D2D content		
		mpRenderTarget2D->BeginDraw();	

		//Clear D2D Background
		mpRenderTarget2D->Clear(D2D1::ColorF(0.0f, 0.0f, 0.0f, 0.0f));

		//Create our string
		std::wostringstream printString; 
		printString << text;
		wstring printText = printString.str();

		//Set the Font Color
		D2D1_COLOR_F FontColor = D2D1::ColorF(1.0f, 1.0f, 1.0f, 1.0f);

		//Set the brush color D2D will use to draw with
		mpBrush->SetColor(FontColor);	

		//Create the D2D Render Area
		D2D1_RECT_F layoutRect = D2D1::RectF(0, 0, mScreenWidth, mScreenHeight);

		//Draw the Text
		mpRenderTarget2D->DrawText(
			printText.c_str(),
			wcslen(printText.c_str()),
			mpTextFormat,
			layoutRect,
			mpBrush
			);
		mpRenderTarget2D->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(100, 100), 20, 20), mpBrush, 3.0f);
		
		mpRenderTarget2D->EndDraw();	

		//Release the D3D10.1 Device
		mpKeyedMutex10->ReleaseSync(1);

		//Use the D3D11 Device
		mpKeyedMutex11->AcquireSync(1, 5);

		//Use the shader resource representing the direct2d render target
		//to texture a square which is rendered in screen space so it
		//overlays on top of our entire scene. We use alpha blending so
		//that the entire background of the D2D render target is "invisible",
		//And only the stuff we draw with D2D will be visible (the text)

		//Set the blend state for D2D render target texture objects
		mpDeviceContext11->OMSetBlendState(mpTransparency, NULL, 0xffffffff);

		//Set the d2d Index buffer
		mpDeviceContext11->IASetIndexBuffer( mpIndexBuffer2D, DXGI_FORMAT_R32_UINT, 0);
		//Set the d2d vertex buffer
		UINT stride = sizeof( VertexPosTex );
		UINT offset = 0;
		mpDeviceContext11->IASetVertexBuffers( 0, 1, &mpVertBuffer2D, &stride, &offset );

		mpDeviceContext11->PSSetSamplers( 0, 1, &mpSamplerState2D );
		
		apSpriteMaterial->SetWorld<Matrix>(mProjectionMatrix);
		apSpriteMaterial->SetWorldViewProjection<Matrix>(mProjectionMatrix);
		apSpriteMaterial->SetTextureMap<ID3D11ShaderResourceView*>(mpTexture2D);
		apSpriteMaterial->SetTextureSample<ID3D11SamplerState*>(mpSamplerState2D);
		apSpriteMaterial->PreDraw(mpDeviceContext11);

		mpDeviceContext11->RSSetState(mpCWCullMode);
		//Draw the second cube
		mpDeviceContext11->DrawIndexed( 6, 0, 0 );	
	}
};