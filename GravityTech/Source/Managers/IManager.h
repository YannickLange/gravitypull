#ifndef IMANAGER_H_
#define IMANAGER_H_

#include "GPECore.h"

namespace GT
{
	class GT_API IManager
	{
	public:
		virtual ~IManager(){};
		virtual bool Init(const ManagersPtr& apManagers)
		{
			mpManagers = apManagers;
			return true;
		}

		ManagersPtr GetManagers() const
		{
			return mpManagers.lock();
		}

	private:
		ManagersWPtr mpManagers;
	};
};

#endif