#ifndef SCENEMANAGER_H_
#define SCENEMANAGER_H_

#include "Managers/IManager.h"

namespace GT
{
	class GT_API SceneManager : public IManager
	{
	public:
		SceneManager();
		virtual ~SceneManager();

		bool Init(const ManagersPtr& apManagers);
		virtual void Release();

		//TODO: Template function to set current scene.
		bool SetCurrentScene(BaseScenePtr apScene);
		
		BaseScenePtr GetCurrentScene() const;

	private:
		BaseScenePtr mpScene;	//TODO: Multiple scenes with layers
	};
};

#endif