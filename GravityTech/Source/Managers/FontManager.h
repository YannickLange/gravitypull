#ifndef FONTMANAGER_H_
#define FONTMANAGER_H_

#include "Managers/BaseManager.h"
#include "Graphics/Font.h"

namespace GPECore
{
	typedef std::map<std::wstring, FontPtr> FontMapPtr;

	class GPECORE_API FontManager
	{
	public:
		FontManager();
		virtual ~FontManager();
	
		BOOL Init();
		VOID Release();

		FontPtr GetFont();

	private:
		FontMapPtr mFonts;
	};
};

#endif