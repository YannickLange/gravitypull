#include "InputManager.h"

#define KEYDOWN( name, key ) ( name[key] & 0x80 )
#define KEYUP( name, key ) ( !name[key] )

namespace GT
{
	InputManager::InputManager() :
		mpDIKeyboard(NULL),
		mpMouse(NULL),
		mDeviceState(DeviceType::KEYBOARD_MOUSE)
	{

	}

	InputManager::~InputManager()
	{
		mpDIKeyboard->Unacquire();
		SAFE_RELEASE(mpDIKeyboard);
	}

	bool InputManager::Init(HINSTANCE aInstance, HWND ahwnd)
	{
		HRESULT result;

		ZeroMemory(mKeyboardKeys, sizeof(mKeyboardKeys));
		ZeroMemory(mPrevKeyboardKeys, sizeof(mPrevKeyboardKeys));

		result = DirectInput8Create(aInstance, DIRECTINPUT_VERSION, IID_IDirectInput8,
			(void**)&mDirectInput, NULL);
		if(FAILED(result)) {return false;}

		result = mDirectInput->CreateDevice(GUID_SysKeyboard, &mpDIKeyboard, NULL);
		if(FAILED(result)) {return false;}

		result = mpDIKeyboard->SetDataFormat(&c_dfDIKeyboard);
		if(FAILED(result)) {return false;}

		result = mpDIKeyboard->SetCooperativeLevel(ahwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE  | DISCL_NOWINKEY);
		if(FAILED(result)) {return false;}

		mpMouse.reset(new Mouse);
		if(!mpMouse->Init(mDirectInput, ahwnd))
		{
			return false;
		}

		return true;
	}

	void InputManager::Update()
	{
		switch (mDeviceState)
		{
		case DeviceType::KEYBOARD_MOUSE:
			UpdateKeyBoard();
			mpMouse->Update();
			break;
		case DeviceType::KEYBOARD:
			UpdateKeyBoard();
			break;
		case DeviceType::MOUSE:
			mpMouse->Update();
			break;
		case DeviceType::XBOX_CONTROLLER:
			break;
		default:
			break;
		}
	}

	void InputManager::UpdateKeyBoard()
	{
		mpDIKeyboard->Acquire();
		mpDIKeyboard->GetDeviceState(sizeof(mKeyboardKeys), (LPVOID)&mKeyboardKeys);
	}

	void InputManager::UpdatePreviousKeys()
	{
		memcpy(mPrevKeyboardKeys, mKeyboardKeys, sizeof(mKeyboardKeys));
		mpMouse->UpdatePrevious();
	}

	bool InputManager::ButtonDown(KeyBoardKey aKey)
	{
		return KEYDOWN(mKeyboardKeys, aKey);
	}

	bool InputManager::ButtonUp(KeyBoardKey aKey)
	{
		return KEYUP(mKeyboardKeys, aKey);
	}

	bool InputManager::ButtonReleased(KeyBoardKey aKey)
	{
		return KEYDOWN(mPrevKeyboardKeys, aKey) && !KEYDOWN(mKeyboardKeys, aKey);
	}

	bool InputManager::ButtonPressed(KeyBoardKey aKey)
	{
		//return (mPrevKeyboardKeys[aKey] & 0x80) && !(mKeyboardKeys[aKey]);
		return !KEYDOWN(mPrevKeyboardKeys, aKey) && KEYDOWN(mKeyboardKeys, aKey);
	}

	MousePtr InputManager::GetMouse() const
	{
		return mpMouse;
	}

};