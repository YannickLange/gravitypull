#include "SoundManager.h"
#include "Actors/Components/Sound/Sound.h"

namespace GT
{
	SoundManager::SoundManager()
	{

	}

	SoundManager::~SoundManager()
	{
		SAFE_RELEASE(mpPrimaryBuffer);
		SAFE_RELEASE(mpDirectSound);
	}

	bool SoundManager::Init(HWND ahwnd)
	{
		HRESULT result;
		DSBUFFERDESC bufferDesc;

		//Initialize the direct sound interface pointer for the default sound device
		result = DirectSoundCreate8(NULL, &mpDirectSound, NULL);
		if(FAILED(result)) {return false;}

		//Set the cooperative level to priority so the format of the primary sound buffer can be modified 
		result = mpDirectSound->SetCooperativeLevel(ahwnd, DSSCL_PRIORITY);
		if(FAILED(result)) {return false;}

		//Setup the primary buffer description
		bufferDesc.dwSize = sizeof(DSBUFFERDESC);
		bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
		bufferDesc.dwBufferBytes = 0;
		bufferDesc.dwReserved = 0;
		bufferDesc.lpwfxFormat = NULL;
		bufferDesc.guid3DAlgorithm = GUID_NULL;

		//Get control of the primary sound buffer on the default sound device
		result = mpDirectSound->CreateSoundBuffer(&bufferDesc, &mpPrimaryBuffer, NULL);
		if(FAILED(result)) {return false;}

		return InitWaveFormat();
	}

	SoundPtr SoundManager::GetSound(const wstring aFile)
	{
		if(!mSounds.count(aFile))
		{
			SoundPtr sound(new Sound);
			sound->Init(aFile, GetThis());
			mSounds.insert(make_pair(aFile, sound));
			return sound;
		}

		return mSounds.at(aFile);
	}

	bool SoundManager::InitWaveFormat()
	{
		HRESULT result;
		WAVEFORMATEX waveFormat;

		//Setup the format of the primary sound buffer
		//Setup as: .WAV file, recorded at 44,100  samples per second in 16-bit stereo (cd audio format)
		waveFormat.wFormatTag = WAVE_FORMAT_PCM;
		waveFormat.nSamplesPerSec = 44100;
		waveFormat.wBitsPerSample = 16;
		waveFormat.nChannels = 2;
		waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
		waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
		waveFormat.cbSize = 0;

		// Set the primary buffer to be the wave format specified.
		result = mpPrimaryBuffer->SetFormat(&waveFormat);
		if(FAILED(result)) { return false; }

		return true;
	}

	IDirectSound8*  SoundManager::GetIDirectSound() const
	{
		return mpDirectSound;
	}

	SoundManagerPtr SoundManager::GetThis()
	{
		return shared_from_this();
	}
};