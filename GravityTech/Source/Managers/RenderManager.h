#ifndef RENDERMANAGER_H_
#define RENDERMANAGER_H_

#include "IManager.h"
#include "Actors/Actors.h"
#include "Graphics/Effects/Effect.h"
#include "Actors/Components/Material/Materials.h"
#include "RenderManager2D.h"

namespace GT
{
	typedef map<wstring, MaterialPtr> MaterialMapPtr;
	typedef map<wstring, Texture2DPtr> Texture2DMapPtr;
	typedef map<wstring, ShaderPtr> ShaderMapPtr;
	typedef map<wstring, FontPtr> FontMapPtr;
	typedef map<wstring, EffectPtr> EffectMapPtr;

	//Manages materials, textures and shaders
	class GT_API RenderManager : public IManager
	{
	public:
		RenderManager();
		virtual ~RenderManager();

		virtual bool Init(const ManagersPtr& apManagers);
		virtual void Draw(const Matrix& aWorld, const Matrix& aView, const Matrix& aProjection, 
			const DeviceManagerPtr apDeviceManager);
		virtual void Release();

		//Setters
		void SetOcclusionCulling(const bool abEnableOcclusionCulling);
		void SetFrustumCulling(const bool abEnableFrustumCulling);

		//Getters
		Texture2DPtr GetTexture2D(const wstring aFile);
		EffectPtr GetEffect(const wstring& aFile);
		MeshRenderPtr GetMeshRender(const wstring& aMeshFile, const BaseMaterialPtr& mpMaterial);

		const bool GetOcclusionCulling() const;
		const bool GetFrustumCulling() const;

	private:
		void ApplyFrustumCulling();
		void ApplyOcclusionCulling();

		MaterialMapPtr mMaterials;
		Texture2DMapPtr mTextures;
		ShaderMapPtr mShaders;
		FontMapPtr mFonts;
		
		RenderComponentPtrVec mRenderComponents;
		EffectMapPtr mEffects;
		
		bool mbOcclusionCulling;	//TODO
		bool mbFrustumCulling;		//TODO

		RenderManager2D* mpRenderManager2D;

		SpriteMaterial* mpMaterial2D; //Temp
	};
};


#endif