#include "GeometryManager.h"

#include "Managers/DeviceManager.h"
//#include "Objects/Components/Geometry/Cube.h"
#include "Actors/BaseActor.h"
#include "Actors/Components/ComponentTypes.h"
#include "Actors/Components/Geometry/Mesh.h"
#include "Actors/Components/Geometry/PlaneMesh.h"
//#include "Objects/Components/Geometry/Plane.h"
//#include "Objects/Components/Geometry/Sphere.h"

namespace GT
{
		GeometryManager::GeometryManager() //:
			//mpCube(NULL),
			//mpPlane(NULL),
			//mpSphere(NULL)
		{
			mMeshes.reset(new MeshPtrMap());
		}

		GeometryManager::~GeometryManager()
		{
			/*mpCube = NULL;
			mpPlane = NULL;
			mpSphere = NULL;*/
			mpDevice.reset();
		}

		bool GeometryManager::Init(DeviceManagerPtr apDevice)
		{
			mpDevice = apDevice;

			PRINT_MESSAGE("Initialized GeometryManager");
			return true;
		}

		//Create
		/*CubePtr GeometryManager::GetCube()
		{
			if(!mpCube)
			{

			}
			
			return mpCube;
		}*/

		/**
		 * function:	GetMesh
		 * description:	Returns a mesh, it checks if the mesh is already existing and creates it if it isn't.
		 * param[in]:	std::wstring aFileName	- A string to the file location with filename and type //TODO: This should be a File type!
		 */
		MeshPtr GeometryManager::GetMesh(std::wstring aFileName)
		{
			if(!mMeshes->count(aFileName))
			{
				MeshPtr mesh;
				mesh.reset(new Mesh());
				mesh->Init(aFileName, mpDevice->GetDevice());
				mMeshes->insert(std::make_pair(aFileName, mesh));
				return mesh;
			}

			return mMeshes->at(aFileName);
		}

		MeshMapPtr GeometryManager::GetMeshes() const
		{
			return mMeshes;
		}

		PlaneMeshPtr GeometryManager::GetPlane()
		{
			if(!mpPlane)
			{
				mpPlane.reset(new PlaneMesh);
				mpPlane->Init(mpDevice->GetDevice(), Vector2(6, 6));
			}
			
			return mpPlane;
		}

		/*SpherePtr GeometryManager::GetSphere()
		{
			if(!mpSphere)
			{

			}

			return mpSphere;
		}*/
};