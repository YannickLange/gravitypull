#ifndef DEVICEMANAGER_H_
#define DEVICEMANAGER_H_

//#include <windows.h>
#include "Common.h"
//#include "GPECore.h"
#include "Graphics/DXInclude.h"

#include <windows.h>
#include <D3D11.h>
#include <D3DX11.h>
#include <D3DX10.h>
#include <D3D10_1.h>
#include <DXGI.h>
#include <D2D1.h>
#include <sstream>
#include <dwrite.h>
#include "Actors/Components/Material/Materials.h"

namespace GT
{
	class GT_API DeviceManager 
	{
	public:
		DeviceManager();
		virtual ~DeviceManager();
		
		bool Init(const UINT aWidth, const UINT aHeight, const bool abFullscreen, const HWND aHandle);
		bool Resize(const UINT aWidth, const UINT aHeight);
		void SetViewport(const UINT aWidth, const UINT aHeight);
		void RestoreTarget();
		void Clear(const Color& aColor);
		void Present();

		void SetProjectionMatrix(const float aScreenNear = 0.1f, const float aScreenDepth = 10000.0f);
		void SetOrthoMatrix(const float aScreenNear = 0.1f, const float aScreenDepth = 10000.0f);
		
		//Getters
		UINT GetWidth() const;
		UINT GetHeight() const;

		ID3D11Device* GetDevice() const;
		ID3D11DeviceContext* GetDeviceContext() const;

		Matrix GetProjectionMatrix() const;
		Matrix GetWorldMatrix() const;
		Matrix GetOrthoMatrix() const;

		const ID3D11SamplerState* GetSamplerState() const;
		const ID3D11BlendState* GetBlendState() const;
		IDXGIAdapter1* GetAdapter();
		
		void RenderText(std::wstring text, SpriteMaterial* apSpriteMaterial);
	private:
		bool CreateTargetView();
		bool CreateDepthStencilView(UINT aWidth, UINT aHeight);

		UINT mScreenWidth;
		UINT mScreenHeight;

		static const UINT DefaultScreenWidth;
        static const UINT DefaultScreenHeight;
		static const UINT DefaultFrameRate;
        static const UINT DefaultMultiSamplingCount;

		D3D_FEATURE_LEVEL mFeatureLevel;
        ID3D11Device* mpDevice11;
        ID3D11DeviceContext* mpDeviceContext11;
        IDXGISwapChain* mpSwapChain;

		UINT mFrameRate;
        bool mbIsFullScreen;
        bool mbDepthStencilBufferEnabled;
        bool mbMultiSamplingEnabled;
        UINT mMultiSamplingCount;
        UINT mMultiSamplingQualityLevels;

		ID3D11Texture2D* mpDepthStencilBuffer;
        D3D11_TEXTURE2D_DESC mBackBufferDesc;
        ID3D11RenderTargetView* mpRenderTargetView;
        ID3D11DepthStencilView* mpDepthStencilView;
        D3D11_VIEWPORT mViewport;

		Matrix mProjectionMatrix;
		Matrix mWorldMatrix;
		Matrix mOrthoMatrix;

		
		//New 2D stuff
		
		bool Init2D(IDXGIAdapter1 *Adapter);
		void InitD2DScreenTexture();
		

		ID3D11RasterizerState* mpCWCullMode;
		ID3D10Device1* mpDevice10;
		IDXGIKeyedMutex *mpKeyedMutex11;
		IDXGIKeyedMutex *mpKeyedMutex10;	
		ID2D1RenderTarget *mpRenderTarget2D;	
		ID2D1SolidColorBrush *mpBrush;
		ID3D11Texture2D *mpBackBuffer11;
		ID3D11Texture2D *mpSharedTex11;	
		ID3D11Buffer *mpVertBuffer2D;
		ID3D11Buffer *mpIndexBuffer2D;
		ID3D11ShaderResourceView *mpTexture2D;
		IDWriteFactory *mpDWriteFactory;
		IDWriteTextFormat *mpTextFormat;
		ID3D11BlendState* mpTransparency;
		ID3D11SamplerState* mpSamplerState2D;
	};
};

#endif