#include "Managers/TextureManager.h"
#include "Graphics/Texture2D.h"

namespace GPECore
{
	
	TextureManager::TextureManager()
	{

	}

	TextureManager::~TextureManager()
	{

	}

	BOOL TextureManager::Init(ID3D11Device* apD3D11Device)
	{
		mpD3D11Device = apD3D11Device;

		return true;
	}

	VOID TextureManager::Release()
	{
		mpD3D11Device = NULL;

		//TODO: Release all the textures in the map
	}

	Texture2DPtr TextureManager::GetTexture2D(std::wstring aFile)
	{
		if(!mTextures.count(aFile))
		{
			Texture2DPtr texture;
			texture.reset(new Texture2D());
			//texture->Init(aFile, mpD3D11Device); //TODO
			mTextures.insert(std::make_pair(aFile, texture));
			return texture;
		}

		return mTextures.at(aFile);
	}
};