#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_

#include "GPECore.h"
#include "IManager.h"
#include <dsound.h>

namespace GT
{
	class GT_API SoundManager : public IManager, 
									 public enable_shared_from_this<SoundManager>
	{
	public:
		struct WaveFormat;

		SoundManager();
		virtual ~SoundManager();
		bool Init(HWND ahwnd);

		SoundPtr GetSound(const wstring aFile);
		IDirectSound8*  GetIDirectSound() const;
		SoundManagerPtr GetThis();
	private:
		bool InitWaveFormat();

		SoundPtrMap mSounds;

		IDirectSound8* mpDirectSound;
		IDirectSoundBuffer* mpPrimaryBuffer;

	public:
		struct WaveFormat
		{
			char mChunkID[4];
			ULONG mChunkSize;
			char mFormat[4];
			char mSubChunkID[4];
			ULONG mSubChunkSize;
			USHORT mAudioFormat;
			USHORT mNumChannels;
			ULONG mSampleRate;
			ULONG mBytesPerSecond;
			USHORT mBlockAlign;
			USHORT mBitsPerSample;
			char mDataChunkID[4];
			ULONG mDataSize;
		};
	};
};

#endif