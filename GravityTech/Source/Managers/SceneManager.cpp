#include "Managers/SceneManager.h"
#include "Scene/BaseScene.h"

namespace GT
{
	SceneManager::SceneManager() :
		mpScene(NULL)
	{

	}

	SceneManager::~SceneManager()
	{
	}

	bool SceneManager::Init(const ManagersPtr& apManagers)
	{
		IManager::Init(apManagers);
		PRINT_MESSAGE("Initialized SceneManager");
		return true;
	}

	void SceneManager::Release()
	{
		mpScene = NULL;
	}

	bool SceneManager::SetCurrentScene(BaseScenePtr apScene)
	{
		bool result = true;
		
		//When there is a scene already
		if(mpScene)
		{
			mpScene->Release();
		}

		mpScene = apScene;

		result = apScene->PreInit(GetManagers());
		if(!result) return result;

		result = apScene->Init();
		if(!result) return result;

		return result;
	}
		
	BaseScenePtr SceneManager::GetCurrentScene() const
	{
		return mpScene;
	}
};