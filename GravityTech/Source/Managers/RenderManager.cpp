#include "RenderManager.h"

#include "Actors/Components/Material/Materials.h"
#include "Actors/Components/Helpers/Texture2D.h"
#include "Actors/Components/Helpers/Shader.h"
#include "Actors/Components/Renderers/MeshRender.h"
#include "Graphics/Effects/Effect.h"
#include "Managers.h"

namespace GT
{

	RenderManager::RenderManager() :
		IManager(),
		mbFrustumCulling(false),
		mbOcclusionCulling(false)
	{

	}

	RenderManager::~RenderManager()
	{

	}

	bool RenderManager::Init(const ManagersPtr& apManagers)
	{
		PRINT_MESSAGE("Initialized RenderManager");

		IManager::Init(apManagers);

		//SpriteMaterialPtr pSpriteMaterial(new SpriteMaterial);
		//pSpriteMaterial->Init(apManagers->GetDeviceManager(), GetEffect(L"../GravityTech/Assets/Effects/Sprite.fxo"));

		mpMaterial2D = new SpriteMaterial;
		mpMaterial2D->Init(apManagers->GetDeviceManager(), GetEffect(L"../GravityTech/Assets/Effects/Sprite.fxo"));

		//mpRenderManager2D = new RenderManager2D();
		//mpRenderManager2D->Init(apManagers->GetDeviceManager(), pSpriteMaterial);

		return true;
	}

	void RenderManager::Draw(const Matrix& aWorld, const Matrix& aView, const Matrix& aProjection, 
			const DeviceManagerPtr apDeviceManager)
	{
		/*if(mbFrustumCulling)
		{
			ApplyFrustumCulling();
		}

		if(mbOcclusionCulling)
		{
			ApplyOcclusionCulling();
		}*/
		
		Matrix viewProjection = aView * aProjection;
		for(RenderComponentPtrVec::iterator it = mRenderComponents.begin(); it != mRenderComponents.end(); ++it)
		{
			BaseMaterial* material = static_cast<BaseMaterial*>(it->get()->GetMaterial().get()); //TODO make this cast safe!
			Matrix transform = it->get()->GetActor()->GetTransform()->GetTransformMatrix();
			material->SetWorld<Matrix>(transform);
			material->SetWorldViewProjection<Matrix>(transform * viewProjection);
			it->get()->Draw(apDeviceManager);
		}

		//mpRenderManager2D->Draw(apDeviceManager->GetDeviceContext(), viewProjection);
		apDeviceManager->RenderText(L"HelloWorld", mpMaterial2D);
	}

	void RenderManager::Release()
	{
		mRenderComponents.clear();
		mTextures.clear();
		mMaterials.clear();
	}

	void RenderManager::SetOcclusionCulling(const bool abEnableOcclusionCulling)
	{
		mbOcclusionCulling = abEnableOcclusionCulling;
	}

	void RenderManager::SetFrustumCulling(const bool abEnableFrustumCulling)
	{
		mbFrustumCulling = abEnableFrustumCulling;
	}
	
	EffectPtr RenderManager::GetEffect(const wstring& aFile)
	{
		if(!mEffects.count(aFile))
		{
			EffectPtr effect(new Effect);
			effect->Init(aFile, GetManagers()->GetDeviceManager());
			mEffects.insert(make_pair(aFile, effect));
			return effect;
		}

		return mEffects.at(aFile);
	}

	MeshRenderPtr RenderManager::GetMeshRender(const wstring& aMeshFile, const BaseMaterialPtr& mpMaterial)
	{
		MeshRenderPtr tempptr(new MeshRender());
		MeshPtr mesh = GetManagers()->GetGeometryManager()->GetMesh(aMeshFile); 
		tempptr->Init(mesh,  mpMaterial, GetManagers()->GetDeviceManager());
		mRenderComponents.push_back(tempptr);
		return tempptr;
	}

	const bool RenderManager::GetOcclusionCulling() const
	{
		return mbOcclusionCulling;
	}

	const bool RenderManager::GetFrustumCulling() const
	{
		return mbFrustumCulling;
	}

	void RenderManager::ApplyFrustumCulling()
	{

	}

	void RenderManager::ApplyOcclusionCulling()
	{

	}
};