#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include "Managers/BaseManager.h"

namespace GPECore
{
	//Forward declarations
	class Texture2D;

	typedef std::shared_ptr<Texture2D> Texture2DPtr;
	typedef std::map<std::wstring, Texture2DPtr> Texture2DMapPtr;

	class TextureManager : public BaseManager
	{
	public:
		TextureManager();
		virtual ~TextureManager();

		virtual BOOL Init(ID3D11Device* apD3D11Device);
		virtual VOID Release();

		Texture2DPtr GetTexture2D(std::wstring aFile);

	private:
		Texture2DMapPtr mTextures;
		ID3D11Device* mpD3D11Device;
	};
};

#endif