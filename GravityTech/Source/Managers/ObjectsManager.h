#ifndef OBJECTSMANAGER_H_
#define OBJECTSMANAGER_H_

#include "BaseManager.h"

namespace GPECore
{
	class ObjectsManager : public BaseManager
	{
	public:
		ObjectsManager();
		virtual ~ObjectsManager();

		virtual bool Init(BaseScene * a_pScene);
		virtual void Release();


	};
};

#endif