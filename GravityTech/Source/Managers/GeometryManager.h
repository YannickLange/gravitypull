#ifndef GEOMETRYMANAGER_H_
#define GEOMETRYMANAGER_H_

#include "GPECore.h"
#include "IManager.h"

namespace GT
{
	class GT_API GeometryManager : public IManager
	{
	public:
		GeometryManager();
		virtual ~GeometryManager();

		virtual bool Init(DeviceManagerPtr apDevice);

		//Create
		//CubePtr GetCube();
		MeshPtr GetMesh(std::wstring aFileName);
		MeshMapPtr GetMeshes() const;
		PlaneMeshPtr GetPlane();
		//SpherePtr GetSphere();

	private:
		MeshMapPtr mMeshes;

		//CubePtr mpCube;
		PlaneMeshPtr mpPlane;
		//SpherePtr mpSphere;

		//Used to create resources
		DeviceManagerPtr mpDevice;
	};
};

#endif