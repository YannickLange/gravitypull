#include "GravityTechApp.h"

namespace GT
{
	GravityTechApp::GravityTechApp() :
		mpView(NULL),
		//mpManagers(NULL),
		mClock(),
		mTime(),
		mWindowHandle(),
		mAppName(L"")
	{
		//mpManagers.reset(new Managers());
	}

	GravityTechApp::~GravityTechApp()
	{

	}

	/**
	 * Name: Init
	 * Desc: Initializes the core of the app using the view
	 * Return: Bool
	 */
	bool GravityTechApp::Init()
	{
		Utils::OpenConsole();
		return true;
	}

	/**
	 * Name: Update
	 * Desc: Updates the main viewer
	 * Return: Void
	 */
	void GravityTechApp::Update()
	{
		mpView->Update(mTime);
	}

	/**
	 * Name: Destroy
	 * Desc: Destroys the entire application
	 * Return: Void
	 */
	void GravityTechApp::Destroy()
	{

	}

	/**
	 * Name: Run
	 * Desc: Runs the application using the messages from windows
	 * Return: int
	 */
	int GravityTechApp::Run()
	{
		MSG msg;
		ZeroMemory(&msg, sizeof(MSG));
		
		//Reset the clock before running
		mClock.Reset();
		
		while(msg.message != WM_QUIT)
		{
			BOOL PeekMessageL(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMsg);

			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);	
				DispatchMessage(&msg);
			}
			else
			{
				mClock.UpdateTime(mTime);
				Update();
			}
		}

		return msg.wParam;
	}

	/**
	 * Name: SetWinMainValues
	 * Desc: Sets all the parameter values that came from the main function
	 * Return: void
	 */
	void GravityTechApp::SetWinMainValues(HINSTANCE aInstance, HINSTANCE aPrevInstance, LPWSTR aCmdLine, int aShowCmd)
	{
		mInstance = aInstance;
		mPrevInstance = aPrevInstance;
		mCmdLine = aCmdLine;
		mShowCmd = aShowCmd;
	}

	const HWND GravityTechApp::GetWindowHandle() const
	{
		return mWindowHandle;
	}

	/*ManagersPtr GravityTechApp::GetManagers() const
	{
		return mpManagers;
	}*/

	ISystemViewPtr GravityTechApp::GetView() const
	{
		return mpView;
	}

	GravityTechAppPtr GravityTechApp::GetThis()
	{
		return shared_from_this();
	}
	
};