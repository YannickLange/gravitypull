#ifndef GRAVITYTECHAPP_H_
#define GRAVITYTECHAPP_H_

#include "Export.h"
#include "Common.h"
#include "Utility/GTTime.h"
#include "Utility/GTClock.h"
#include "View/SystemView.h"

namespace GT
{
	//Forward declarations
	class GravityTechApp;

	//Typedefinitions
	typedef std::shared_ptr<GravityTechApp> GravityTechAppPtr;

	//-----------------------------------------------------------------------------
	// Interface for a gravity tech application
	//-----------------------------------------------------------------------------
	class GT_API IGravityTechApp
	{
		virtual bool Init() = 0;
		virtual void Update() = 0;
		virtual void Destroy() = 0;
		virtual int Run() = 0;
	};

	//-----------------------------------------------------------------------------
	// Default app for engine
	//-----------------------------------------------------------------------------
	class GT_API GravityTechApp : public IGravityTechApp, public std::enable_shared_from_this<GravityTechApp>
	{
	public:
		GravityTechApp();
		virtual ~GravityTechApp();

		virtual bool Init();
		virtual void Update();
		virtual void Destroy(); 
		virtual int Run();
		
		//Setters
		void SetWinMainValues(HINSTANCE aInstance, HINSTANCE aPrevInstance, LPWSTR aCmdLine, int aShowCmd);
		
		//Getters
		const HWND GetWindowHandle() const;
		//ManagersPtr GetManagers() const;
		ISystemViewPtr GetView() const;
		
		HINSTANCE mInstance;
		HINSTANCE mPrevInstance;
		LPWSTR mCmdLine;
		int mShowCmd;
		HWND mWindowHandle;

	protected:
		GravityTechAppPtr GetThis();
		ISystemViewPtr mpView;
		//ManagersPtr mpManagers;
		
	

		std::wstring mAppName;

		GTTime mTime;
		GTClock mClock;
	};
};

#endif