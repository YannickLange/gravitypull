#ifndef COMMON_H_
#define COMMON_H_

#ifdef GT_EXPORTS
	#define GT_API __declspec(dllexport)
#else
	#define GT_API __declspec(dllimport)
#endif

#include <memory>
#include <vector>
#include <string>
#include <map>
#include "Graphics/DXInclude.h"


#include "Utility/Utils.h"
#include "Utility/SimpleMath.h"
#include "Utility/GTException.h"
#include "Utility/GTTime.h"

namespace GT
{
	//Forward declarations
	class GravityTechApp;

	//Typedefinitions
	typedef std::shared_ptr<GravityTechApp> GravityTechAppPtr;
};

#endif