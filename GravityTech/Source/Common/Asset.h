#ifndef ASSET_H_
#define ASSET_H_

#include "GPECore.h"

namespace GT
{
	class GT_API Asset
	{
	public:
		Asset();
		virtual ~Asset();

		BOOL Init(wstring aPath, wstring aFile, wstring aType);
		//VOID SetFileName(wstring aFileName);	//TODO: set the name of the file also in windows folder
		//VOID SetPath(wstring aPath);			//TODO: set the path of the file also in windows folder

		wstring GetName() const;
		wstring GetFileName() const;
		wstring GetPath() const;
		wstring GetFileType() const;
		wstring Get() const;

	private:
		wstring mFileName;			//Example: MyModel
		wstring mPath;				//Example: C:/Users/You/Desktop/
		wstring mFileTypeName;		//Example: .fbx

	};
};

#endif