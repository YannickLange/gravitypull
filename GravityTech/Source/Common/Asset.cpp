#include "Common/Asset.h"

namespace GT
{
	Asset::Asset() :
		mFileName(L""),
		mPath(L""),
		mFileTypeName(L"")
	{

	}

	Asset::~Asset()
	{
		mFileName = L"";
		mPath = L"" ;
		mFileTypeName = L"";
	}

	BOOL Asset::Init(wstring aPath, wstring aFile, wstring aType)
	{
		mPath = aPath;
		mFileName = aFile;
		mFileTypeName = aType;

		return true;
	}

	wstring Asset::GetName() const
	{
		return mFileName + mFileTypeName;
	}

	wstring Asset::GetFileName() const
	{
		return mFileName;
	}

	wstring Asset::GetPath() const
	{
		return mPath;
	}

	wstring Asset::GetFileType() const
	{
		return mFileTypeName;
	}

	wstring Asset::Get() const
	{
		return mPath + mFileName + mFileTypeName;
	}

};