#include "Mouse.h"

#define BUTTONDOWN(device, key) ( device.rgbButtons[key] & 0x80 )

namespace GT
{
	Mouse::Mouse() :
		mPosX(0),
		mPosY(0),
		mMouseWheel(0)
	{

	}

	Mouse::~Mouse()
	{
		mpDIMouse->Unacquire();
		SAFE_RELEASE(mpDIMouse);
	}

	bool Mouse::Init(const LPDIRECTINPUT8& aDirectInput, const HWND ahwnd)
	{
		HRESULT result;
		
		result = aDirectInput->CreateDevice(GUID_SysMouse, &mpDIMouse, NULL);
		if(FAILED(result)) {return false;}
		
		result = mpDIMouse->SetDataFormat(&c_dfDIMouse);
		if(FAILED(result)) {return false;}

		result = mpDIMouse->SetCooperativeLevel(ahwnd, DISCL_EXCLUSIVE | DISCL_NOWINKEY | DISCL_FOREGROUND);
		if(FAILED(result)) {return false;}

		return true;
	}

	void Mouse::Update()
	{
		mpDIMouse->Acquire();
		mpDIMouse->GetDeviceState(sizeof(mCurrentState), (LPVOID)&mCurrentState);

		mPosX += mCurrentState.lX;
		mPosY += mCurrentState.lY;
		mMouseWheel += mCurrentState.lZ;
	}

	void Mouse::UpdatePrevious()
	{
		memcpy(&mPreviousState, &mCurrentState, sizeof(mCurrentState));
	}

	const bool Mouse::ButtonPressed(const MouseButtons aMouseButton) const
	{
		return !BUTTONDOWN(mPreviousState, aMouseButton) && BUTTONDOWN(mCurrentState, aMouseButton);
	}

	const bool Mouse::ButtonReleased(const MouseButtons aMouseButton) const
	{
		return BUTTONDOWN(mPreviousState, aMouseButton) && !BUTTONDOWN(mCurrentState, aMouseButton);
	}

	const bool Mouse::ButtonUp(const MouseButtons aMouseButton) const
	{
		return !BUTTONDOWN(mCurrentState, aMouseButton);
	}

	const bool Mouse::ButtonDown(const MouseButtons aMouseButton) const
	{
		return BUTTONDOWN(mCurrentState, aMouseButton);
	}

	const long Mouse::GetPositionX() const
	{
		return mPosX;
	}

	const long Mouse::GetPositionY() const
	{
		return mPosY;
	}

	const Vector2 Mouse::GetPosition() const
	{
		return Vector2(mPosX, mPosY);
	}

	const long Mouse::GetMouseWheel() const
	{
		return mMouseWheel;
	}

	const long Mouse::GetPositionXDelta() const
	{
		return mCurrentState.lX;
	}

	const long Mouse::GetPositionYDelta() const
	{
		return mCurrentState.lY;
	}

	const Vector2 Mouse::GetPositionDelta() const
	{
		return Vector2(mCurrentState.lX, mCurrentState.lY);
	}

	const long Mouse::GetMouseWheelDelta() const
	{
		return mCurrentState.lZ;
	}

	const bool Mouse::MovedMouse() const
	{
		return mCurrentState.lX != 0 || mCurrentState.lY != 0;
	}

	const bool Mouse::MovedMouseWheel() const
	{
		return mCurrentState.lZ != 0;
	}

	const bool Mouse::MovedMouseWheelUp() const
	{
		return mCurrentState.lZ > 0;
	}

	const bool Mouse::MovedMouseWheelDown() const
	{
		return mCurrentState.lZ < 0;
	}
};