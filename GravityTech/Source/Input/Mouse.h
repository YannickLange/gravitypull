#ifndef MOUSE_H_
#define MOUSE_H_

#include "GPECore.h"
#include <dinput.h>

namespace GT
{
	enum GT_API MouseButtons
	{
		LEFT = 0,
		RIGHT = 1,
		MIDDLE = 2
	};

	class GT_API Mouse
	{
	public:
		Mouse();
		virtual ~Mouse();

		bool Init(const LPDIRECTINPUT8& aDirectInput, const HWND ahwnd);
		void Update();
		void UpdatePrevious();

		const bool ButtonPressed(const MouseButtons aMouseButton) const;
		const bool ButtonReleased(const MouseButtons aMouseButton) const;
		const bool ButtonUp(const MouseButtons aMouseButton) const;
		const bool ButtonDown(const MouseButtons aMouseButton) const;
		
		const long GetPositionX() const;
		const long GetPositionY() const;
		const Vector2 GetPosition() const;
		const long GetMouseWheel() const;

		const long GetPositionXDelta() const;
		const long GetPositionYDelta() const;
		const Vector2 GetPositionDelta() const;
		const long GetMouseWheelDelta() const;

		const bool MovedMouse() const;
		const bool MovedMouseWheel() const;
		const bool MovedMouseWheelUp() const;
		const bool MovedMouseWheelDown() const;

	private:
		long mPosX;
		long mPosY;
		long mMouseWheel;

		IDirectInputDevice8* mpDIMouse;
		DIMOUSESTATE mCurrentState;
        DIMOUSESTATE mPreviousState;
	};
};

#endif