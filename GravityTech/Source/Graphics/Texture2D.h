#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include "GPECore.h"

namespace GT
{
	class GT_API Texture2D
	{
	public:
		Texture2D();
		virtual ~Texture2D();
		bool Init(const wstring aTextureFile, DeviceManagerPtr apDeviceManager);

		ID3D11ShaderResourceView* GetShaderResourceView() const;
		ID3D11SamplerState* GetSamplerState() const;
		const UINT GetWidth() const;
		const UINT GetHeight() const;
		const Vector2 GetSize();
	private:
		ID3D11ShaderResourceView* mpTexture;
		ID3D11SamplerState* mpSamplerState;
		D3D11_TEXTURE2D_DESC mDescription;
	};
};

#endif