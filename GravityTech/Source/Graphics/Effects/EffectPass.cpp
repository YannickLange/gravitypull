#include "EffectPass.h"
#include "EffectTechnique.h"

namespace GT
{
	EffectPass::EffectPass(ID3DX11EffectPass* apPass) :
		mpPass(apPass), mPassDesc()
	{
		 //mpTechnique = apTechnique;
		mpPass->GetDesc(&mPassDesc);
	}
	
	ID3DX11EffectPass* EffectPass::GetPass() const
	{
		return mpPass;
	}

	const D3DX11_PASS_DESC& EffectPass::GetPassDesc() const
	{
		return mPassDesc;
	}

	string EffectPass::GetName() const
	{
		return mPassDesc.Name;
	}

	void EffectPass::CreateInputLayout(ID3D11Device* apDevice,
		D3D11_INPUT_ELEMENT_DESC* apInputElementDesc, 
		const UINT aNumElements,  
		ID3D11InputLayout **apInputLayout)
	{
		HRESULT hr = apDevice->CreateInputLayout(apInputElementDesc, 
			aNumElements, mPassDesc.pIAInputSignature, mPassDesc.IAInputSignatureSize, apInputLayout);

		if (FAILED(hr))
		{
			throw GTException("ID3D11Device::CreateInputLayout() failed.", hr);
		}
	}

	void EffectPass::Apply(const UINT aFlags, ID3D11DeviceContext* apDeviceContext)
	{
		mpPass->Apply(aFlags, apDeviceContext);
	}
};