#include "EffectTechnique.h"
#include "EffectPass.h"

namespace GT
{
	EffectTechnique::EffectTechnique(/*EffectPtr& apEffect,*/ ID3DX11EffectTechnique* apTechnique) :
		/*EffectComponent(apEffect),*/ mpTechnique(apTechnique)
	{
		mpTechnique->GetDesc(&mTechniqueDesc);
	}

	EffectTechnique::~EffectTechnique()
	{
		mPasses.clear();
	}

	bool EffectTechnique::Init()
	{
		for (UINT i = 0; i < mTechniqueDesc.Passes; i++)
        {
			EffectPass* pass = new EffectPass(mpTechnique->GetPassByIndex(i));
			mPasses.insert(pair<string, EffectPassPtr>(pass->GetName(), EffectPassPtr(pass)));
        }

		return true;
	}

	ID3DX11EffectTechnique* EffectTechnique::GetTechnique() const
	{
		return mpTechnique;
	}

	const D3DX11_TECHNIQUE_DESC& EffectTechnique::GetTechniqueDesc() const
	{
		return mTechniqueDesc;
	}

	const EffectPassPtrMap& EffectTechnique::GetPasses() const
	{
		return mPasses;
	}

	const EffectPassPtr EffectTechnique::GetPass(const string& aName)
	{
		EffectPassPtr ptr = mPasses.at(aName);
		if(ptr == NULL)
		{
			throw GTException("Effect pass doesn't exist"); //Todo set the name of the variable
		}

		return ptr;
	}

	const string EffectTechnique::GetName() const
	{
		return mTechniqueDesc.Name;
	}

	EffectTechniquePtr
 EffectTechnique::GetSharedPtr()
	{
		return shared_from_this();
	}

};