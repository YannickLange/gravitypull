#ifndef EFFECTTECHNIQUE_H_
#define EFFECTTECHNIQUE_H_

#include "Common.h"
#include "Effect.h"

namespace GT
{
	typedef map<string, EffectPassPtr> EffectPassPtrMap;

	class EffectTechnique : /*public EffectComponent,*/
							public enable_shared_from_this<EffectTechnique>
	{
	public:
		EffectTechnique(ID3DX11EffectTechnique* apTechnique);
		~EffectTechnique();

		bool Init();
		
		ID3DX11EffectTechnique* GetTechnique() const;
		const D3DX11_TECHNIQUE_DESC& GetTechniqueDesc() const;
		const EffectPassPtrMap& GetPasses() const;
		const EffectPassPtr GetPass(const string& aName);
		virtual const string GetName() const;

	private:
		EffectTechniquePtr GetSharedPtr();

		ID3DX11EffectTechnique* mpTechnique;
		D3DX11_TECHNIQUE_DESC mTechniqueDesc;
		EffectPassPtrMap mPasses;

	};
};

#endif