#ifndef EFFECT_H_
#define EFFECT_H_

//#include "EffectPass.h"
//#include "EffectTechnique.h"
//#include "EffectVariable.h"
#include "Graphics/DXInclude.h"
#include <d3dx11effect.h>
#include "GPECore.h"

namespace GT
{
	class Effect;
	class EffectPass;
	class EffectTechnique;
	class EffectVariable;

	typedef shared_ptr<Effect> EffectPtr;
	typedef shared_ptr<EffectPass> EffectPassPtr;
	typedef shared_ptr<EffectTechnique> EffectTechniquePtr;
	typedef shared_ptr<EffectVariable> EffectVariablePtr;

	typedef weak_ptr<Effect> EffectWPtr;
	typedef weak_ptr<EffectPass> EffectPassWPtr;
	typedef weak_ptr<EffectTechnique> EffectTechniqueWPtr;

	typedef vector<EffectTechniquePtr> EffectTechniquePtrVec;
	typedef vector<EffectVariablePtr> EffectVariablePtrVec;
	typedef map<string, EffectTechniquePtr> EffectTechniquePtrMap;
	typedef map<string, EffectVariablePtr> EffectVariablePtrMap;

	class GT_API Effect : public enable_shared_from_this<Effect>
	{
	public:
		Effect();
		~Effect();
		bool Init(const wstring& aFileName, DeviceManagerPtr& apDeviceManager);
		
		void SetEffect(ID3DX11Effect* apEffect);
		const D3DX11_EFFECT_DESC& GetEffectDesc() const;
		const ID3DX11Effect* GetEffect() const;

		const EffectTechniquePtrMap& GetTechniqueMap() const;
		const EffectVariablePtrMap& GetVariableMap() const;

		const EffectTechniquePtr GetTechnique(const string& aName);
		const EffectVariablePtr GetVariable(const string& aName);

		bool LoadCompiledEffect(const wstring& aFileName, DeviceManagerPtr& apDeviceManager, ID3DX11Effect** apEffect);
		
	private:
		EffectPtr& GetSharedPtr();
		void ReadTechniques();
		void ReadVariables();

		EffectTechniquePtrMap mTechniques;
		EffectVariablePtrMap mVariables;

		ID3DX11Effect* mpEffect;
		D3DX11_EFFECT_DESC mEffectDesc;
	};

	/*class GT_API EffectComponent
	{
	public:
		EffectComponent(EffectPtr& apEffect);

		EffectPtr& GetEffect() const;
		virtual const string& GetName() const = 0;
	private:
		Effect* mpEffect;
	};*/
};

#endif