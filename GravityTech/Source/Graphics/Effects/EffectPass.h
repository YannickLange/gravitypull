#ifndef EFFECTPASS_H_
#define EFFECTPASS_H_

#include "Effect.h"
#include "Common.h"

namespace GT
{
	class GT_API EffectPass
	{
	public:
		EffectPass(ID3DX11EffectPass* apPass);
		
		ID3DX11EffectPass* GetPass() const;
        const D3DX11_PASS_DESC& GetPassDesc() const;
        string GetName() const;

        void CreateInputLayout(ID3D11Device* apDevice, D3D11_INPUT_ELEMENT_DESC* apInputElementDesc, 
			const UINT aNumElements,  ID3D11InputLayout **apInputLayout);
        void Apply(const UINT aFlags, ID3D11DeviceContext* apDeviceContext);

	private:
		//EffectTechniqueWPtr mpTechnique;
		ID3DX11EffectPass* mpPass;
		D3DX11_PASS_DESC mPassDesc;
	};
};

#endif