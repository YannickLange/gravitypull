#include "EffectVariable.h"


namespace GT
{
	EffectVariable::EffectVariable(/*EffectPtr& apEffect,*/ ID3DX11EffectVariable* apVariable) :
		/*EffectComponent(apEffect),*/ mpVariable(apVariable)
	{
		mpVariable->GetDesc(&mVariableDesc);
		mpVariable->GetType()->GetDesc(&mTypeDesc);
	}

	ID3DX11EffectVariable* EffectVariable::GetVariable() const
	{
		return mpVariable;
	}

	const D3DX11_EFFECT_VARIABLE_DESC& EffectVariable::GetVariableDesc() const
	{
		return mVariableDesc;
	}

	const ID3DX11EffectType* EffectVariable::GetEffectType() const
	{
		return mpVariable->GetType();
	}

	const D3DX11_EFFECT_TYPE_DESC& EffectVariable::GetEffectTypeDesc() const
	{
		return mTypeDesc;
	}

	const string EffectVariable::GetName() const
	{
		return mVariableDesc.Name;
	}

	EffectVariable& EffectVariable::operator<<(Matrix value)
	{
		ID3DX11EffectMatrixVariable* variable = mpVariable->AsMatrix();
		if (variable->IsValid() == false)
		{
			throw GTException("Invalid EffectVariable cast to Matrix");
		}

		variable->SetMatrix(reinterpret_cast<const float*>(&value));
	
		return *this;
	}

	EffectVariable& EffectVariable::operator<<(ID3D11ShaderResourceView* value)
	{
		ID3DX11EffectShaderResourceVariable* variable = mpVariable->AsShaderResource();
		if (variable->IsValid() == false)
		{
			throw GTException("Invalid EffectVariable cast to ResourceVariable");
		}

		variable->SetResource(value);
	
		return *this;
	}

	EffectVariable& EffectVariable::operator<<(Vector3 value)
	{
		ID3DX11EffectVectorVariable* variable = mpVariable->AsVector();
		if (variable->IsValid() == false)
		{
			throw GTException("Invalid EffectVariable cast to Vector");
		}

		variable->SetFloatVector(reinterpret_cast<const float*>(&value));
	
		return *this;
	}

	EffectVariable& EffectVariable::operator<<(float value)
	{
		ID3DX11EffectScalarVariable* variable = mpVariable->AsScalar();
		if (variable->IsValid() == false)
		{
			throw GTException("Invalid EffectVariable cast to ScalerVariable");
		}

		variable->SetFloat(value);
	
		return *this;
	}
};