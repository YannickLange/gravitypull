#ifndef EFFECTVARIABLE_H_
#define EFFECTVARIABLE_H_

#include "Common.h"
#include "Effect.h"
#include <d3dx11effect.h>

namespace GT
{
	class GT_API EffectVariable /*: public EffectComponent*/
	{
	public:
		EffectVariable(/*EffectPtr& apEffect,*/ ID3DX11EffectVariable* apVariable);

		ID3DX11EffectVariable* GetVariable() const;
		const D3DX11_EFFECT_VARIABLE_DESC& GetVariableDesc() const;
		const ID3DX11EffectType* GetEffectType() const;
		const D3DX11_EFFECT_TYPE_DESC& GetEffectTypeDesc() const;
		const string GetName() const;

		EffectVariable& operator<<(Matrix value);
        EffectVariable& operator<<(ID3D11ShaderResourceView* value);
        EffectVariable& operator<<(Vector3 value);
        EffectVariable& operator<<(float value);
	
	private:
		ID3DX11EffectVariable* mpVariable;
		D3DX11_EFFECT_VARIABLE_DESC mVariableDesc;
		D3DX11_EFFECT_TYPE_DESC mTypeDesc;

		wstring mName;
	};
};

#endif