#include "Effect.h"
#include "Managers/DeviceManager.h"

#include "EffectPass.h"
#include "EffectTechnique.h"
#include "EffectVariable.h"

#include "Graphics/DXInclude.h"

namespace GT
{
	Effect::Effect() : 
		mpEffect(NULL)
	{
		
	}

	Effect::~Effect()
	{
		mTechniques.clear();
		mVariables.clear();
		SAFE_RELEASE(mpEffect);
	}

	bool Effect::Init(const wstring& aFileName, DeviceManagerPtr& apDeviceManager)
	{
		LoadCompiledEffect(aFileName, apDeviceManager, &mpEffect);

		HRESULT hr = mpEffect->GetDesc(&mEffectDesc);
		if (FAILED(hr))
        {
			throw GTException("ID3DX11Effect::GetDesc() failed.", hr);
        }
	
		ReadTechniques();
		ReadVariables();
		return true;
	}
		
	void Effect::SetEffect(ID3DX11Effect* apEffect)
	{
		mpEffect = apEffect;
	}

	const D3DX11_EFFECT_DESC& Effect::GetEffectDesc() const
	{
		return mEffectDesc;
	}

	const ID3DX11Effect* Effect::GetEffect() const
	{
		return mpEffect;
	}

	const EffectTechniquePtrMap& Effect::GetTechniqueMap() const
	{
		return mTechniques;
	}

	const EffectVariablePtrMap& Effect::GetVariableMap() const
	{
		return mVariables;
	}

	const EffectTechniquePtr Effect::GetTechnique(const string& aName)
	{
		EffectTechniquePtr ptr = mTechniques.at(aName);
		if(ptr == NULL)
		{
			throw GTException("Effect technique doesn't exist"); //Todo set the name of the variable
		}

		return ptr;
	}

	const EffectVariablePtr Effect::GetVariable(const string& aName)
	{
		EffectVariablePtr ptr = mVariables.at(aName);
		if(ptr == NULL)
		{
			throw GTException("Effect variable doesn't exist"); //Todo set the name of the variable
		}

		return ptr;
	}

	bool Effect::LoadCompiledEffect(const wstring& aFileName, DeviceManagerPtr& apDeviceManager, ID3DX11Effect** apEffect)
	{
		vector<char> compiledShader;
		Utils::LoadBinaryFile(aFileName, compiledShader);

		HRESULT hr = D3DX11CreateEffectFromMemory(&compiledShader.front(), compiledShader.size(), NULL, apDeviceManager->GetDevice(), apEffect);
		if(FAILED(hr))
		{
			throw GTException("Could not create an effect from memory");
		}
		
		return true;
	}

	EffectPtr& Effect::GetSharedPtr()
	{
		return shared_from_this();
	}

	void Effect::ReadTechniques()
	{
		for (UINT i = 0; i < mEffectDesc.Techniques; i++)
        {
			EffectTechniquePtr technique(new EffectTechnique(mpEffect->GetTechniqueByIndex(i)));
			technique->Init();
			mTechniques.insert(pair<string, EffectTechniquePtr>(technique->GetName(), technique));
        }
	}

	void Effect::ReadVariables()
	{
		for (UINT i = 0; i < mEffectDesc.GlobalVariables; i++)
        {
			EffectVariablePtr variable(new EffectVariable(mpEffect->GetVariableByIndex(i)));
			mVariables.insert(pair<string, EffectVariablePtr>(variable->GetName(), variable));
        }
	}


	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//EffectComponent
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	/*EffectComponent::EffectComponent(EffectPtr& apEffect) :
		mwpEffect(apEffect)
	{
	}

	EffectPtr& EffectComponent::GetEffect() const
	{
		return EffectPtr(mpEffect);
	}*/

};