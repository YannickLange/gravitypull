#include "Graphics/Texture2D.h"
#include "Managers/DeviceManager.h"
#include "Graphics/DXInclude.h"
#include <D3Dcompiler.h>
#include <D3DX11.h>

namespace GT
{
	Texture2D::Texture2D() :
		mpTexture(NULL),
		mpSamplerState(NULL)
	{

	}

	Texture2D::~Texture2D()
	{
		SAFE_RELEASE(mpTexture);
		SAFE_RELEASE(mpSamplerState);
	}

	bool Texture2D::Init(const wstring aTextureFile, DeviceManagerPtr apDeviceManager)
	{
		HRESULT result;
		result = D3DX11CreateShaderResourceViewFromFile(apDeviceManager->GetDevice(), aTextureFile.c_str(), NULL, NULL, &mpTexture, NULL );
		if(FAILED(result))
		{
			return false;
		}

		// Describe the Sample State
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    
		//Create the Sample State
		result = apDeviceManager->GetDevice()->CreateSamplerState(&sampDesc, &mpSamplerState);
		if(FAILED(result))
		{
			return false;
		}

		ID3D11Resource* resource;
		mpTexture->GetResource(&resource);
		
		((ID3D11Texture2D*)resource)->GetDesc(&mDescription);
		resource->Release( );


		return true;
	}

	ID3D11ShaderResourceView* Texture2D::GetShaderResourceView() const
	{
		return mpTexture;
	}

	ID3D11SamplerState* Texture2D::GetSamplerState() const
	{
		return mpSamplerState;
	}

	//Todo do I need the description or just width and height
	const UINT Texture2D::GetWidth() const
	{
		return mDescription.Width;
	}

	const UINT Texture2D::GetHeight() const
	{
		return mDescription.Height;
	}

	const Vector2 Texture2D::GetSize()
	{
		return Vector2(GetWidth(), GetHeight());
	}
};