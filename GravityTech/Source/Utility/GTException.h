#ifndef GTEXCEPTION_H_
#define GTEXCEPTION_H_

#include "Export.h"
#include "Common.h"
#include <exception>

namespace GT
{
	class GTException : public std::exception
	{
	public:
		GTException(const char* const& aMessage, const HRESULT aResult = S_OK);
		HRESULT GetResult() const;
		std::wstring Whatw() const;
	private:
		HRESULT mResult;
	};
};

#endif