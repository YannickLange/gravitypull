#ifndef GTCLOCK_H_
#define GTCLOCK_H_

#include "GTTime.h"

#include <Windows.h>

namespace GT
{
	class GT_API GTClock
	{
	public:
		GTClock();

		void Reset();
		void UpdateTime(GTTime& aTime);

		void GetTime(LARGE_INTEGER& aTime) const;
		const double GetFrequency() const;
		const LARGE_INTEGER& GetStartTime() const;
		const LARGE_INTEGER& GetCurrentTime() const;
		const LARGE_INTEGER& GetLastTime() const;

	private:
		GTClock(const GTClock& aOther);
		GTClock& operator=(const GTClock aOther);

		LARGE_INTEGER mStartTime;
		LARGE_INTEGER mCurrentTime;
		LARGE_INTEGER mLastTime;
		double mFrequency;

	};
};

#endif