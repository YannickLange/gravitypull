#include "Utils.h"
#include <fstream>
#include <algorithm>

using namespace std;

namespace GT
{

	/**
	 * Name: OpenConsole
	 * Desc: Opens a debug console for printing
	 * Return: void
	 */
	void Utils::OpenConsole()
	{
	// Enable run-time memory check and console for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		AllocConsole();
		freopen("CONIN$", "r",stdin);
		freopen("CONOUT$", "w",stdout);
		freopen("CONOUT$", "w",stderr);
		std::cout << ""; 
	#endif
	}

	LRESULT CALLBACK WndProc(HWND ahwnd, UINT amsg, WPARAM awParam, LPARAM alParam)
	{
		switch( amsg )
		{

		case WM_KEYDOWN:
			//if escape key was pressed, display popup box
			if( awParam == VK_ESCAPE ){
				if(MessageBox(0, L"Are you sure you want to exit?",
					L"Really?", MB_YESNO | MB_ICONQUESTION) == IDYES)

					//Release the windows allocated memory  
					DestroyWindow(ahwnd);
			}

			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		}

		//return the message for windows to handle it
		return DefWindowProc(ahwnd, amsg, awParam, alParam);
	}

	bool Utils::CreateGTWindow(HWND& aWindow, LPCWSTR aTitle, HINSTANCE ahInstance,int aShowWnd, int aWidth, int aHeight)
	{
		//Start creating the window//

		WNDCLASSEX wc;	//Create a new extended windows class

		wc.cbSize = sizeof(WNDCLASSEX);					//Size of our windows class
		wc.style = CS_HREDRAW | CS_VREDRAW;				//class styles
		wc.lpfnWndProc = WndProc;						//Default windows procedure function
		wc.cbClsExtra = NULL;							//Extra bytes after our wc structure
		wc.cbWndExtra = NULL;							//Extra bytes after our windows instance
		wc.hInstance = ahInstance;						//Instance to current application
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);			//Title bar Icon
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);		//Default mouse Icon
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);	//Window bg color
		wc.lpszMenuName = NULL;							//Name of the menu attached to our window
		wc.lpszClassName = L"Window";					//Name of our windows class
		wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);		//Icon in your taskbar

		if (!RegisterClassEx(&wc))	//Register our windows class
		{
			//if registration failed, display error
			MessageBox(NULL, L"Error registering class",	L"Error", MB_OK | MB_ICONERROR);
			return false;
		}

		aWindow = CreateWindowEx(			//Create our Extended Window
			NULL,							//Extended style
			L"Window",						//Name of our windows class
			aTitle,							//Name in the title bar of our window
			WS_OVERLAPPEDWINDOW,			//style of our window
			CW_USEDEFAULT, CW_USEDEFAULT,	//Top left corner of window
			aWidth,							//Width of our window
			aHeight,						//Height of our window
			NULL,							//Handle to parent window
			NULL,							//Handle to a Menu
			ahInstance,						//Specifies instance of current program
			NULL							//used for an MDI client window
			);

		//Make sure our window has been created
		if (!aWindow)	
		{
			//If not, display error
			MessageBox(NULL, L"Error creating window", L"Error", MB_OK | MB_ICONERROR);
		}

		if(!ShowWindow(aWindow, aShowWnd))
		{
			return false;
		}

		if(!UpdateWindow(aWindow))
		{
			return false;
		}

		return true;
	}

	wstring& Utils::StringToWString(const string& aString)
	{
		wstring ws;
		ws.assign(aString.begin(), aString.end());
		return ws;
	}

	string& Utils::WStringToString(const wstring& aWString)
	{
		std::string s;
		s.assign(aWString.begin(), aWString.end());
		return s;
	}

	void Utils::GetFileNameAndDirectory(const wstring& aInputPath, wstring& aDirectory, wstring& aFileName)
	{
		wstring fullPath(aInputPath);
		replace(fullPath.begin(),fullPath.end(),'\\','/');

		wstring::size_type lastSlashIndex = fullPath.find_last_of('/');

		if (lastSlashIndex == std::string::npos)
		{
			aDirectory = L"";
			aFileName = fullPath;
		}
		else
		{
			aDirectory = fullPath.substr(0, lastSlashIndex);
			aFileName = fullPath.substr(lastSlashIndex + 1, fullPath.size() - lastSlashIndex- 1);
		}
	}

	wstring& Utils::GetFileName(const wstring& aInputPath)
	{
		wstring fileName;
		wstring fullPath(aInputPath);
		replace(fullPath.begin(),fullPath.end(),'\\','/');

		wstring::size_type lastSlashIndex = fullPath.find_last_of('/');

		if (lastSlashIndex == std::string::npos)
		{
			fileName = fullPath;
		}
		else
		{
			fileName = fullPath.substr(lastSlashIndex + 1, fullPath.size() - lastSlashIndex- 1);
		}

		return fileName;
	}

	wstring& Utils::GetDirectory(const wstring& aInputPath)
	{
		wstring directory;
		wstring fullPath(aInputPath);
		replace(fullPath.begin(),fullPath.end(),'\\','/');

		std::string::size_type lastSlashIndex = fullPath.find_last_of('/');

		if (lastSlashIndex == std::string::npos)
		{
			directory = L"";
		}
		else
		{		
			directory = fullPath.substr(0, lastSlashIndex);
		}

		return directory;
	}

	void Utils::LoadBinaryFile(const wstring& aFileName, vector<char>& aData)
	{
		ifstream file(aFileName.c_str(), ios::binary);
		if(file.bad())
		{
			throw GTException("Could not open file"); //Todo aFileName also
		}

		file.seekg(0, ios::end);
		UINT size = (UINT)file.tellg();
		if(size > 0)
		{
			aData.resize(size);
			file.seekg(0, ios::beg);
			file.read(&aData.front(), size);
		}
		else
		{
			PRINT_WARNING("File did not have any data");
		}

		file.close();
	}
};