#include "GTTime.h"

namespace GT
{
	GTTime::GTTime()
	{

	}

	void GTTime::SetElapsedTime(const double aElapsedTime)
	{
		mElapsedTime = aElapsedTime;
	}

	void GTTime::SetDeltaTime(const double aDeltaTime)
	{
		mDeltaTime = aDeltaTime;
	}

	const double GTTime::GetDeltaTime() const
	{
		return mDeltaTime;
	}

	const double GTTime::GetElapsedTime() const
	{
		return mElapsedTime;
	}
};