#include "File.h"

namespace GPECore
{
	File::File() :
		m_Name(L""),
		m_Type(L"")
	{
	
	}

	File::~File()
	{
		m_Name = L"";
		m_Type = L"";
	}

	void File::SetName(std::wstring a_Name)
	{
		m_Name = a_Name;
	}

	std::wstring File::GetName()
	{
		return m_Name;
	}

	std::wstring File::GetType()
	{
		return m_Type;
	}
};