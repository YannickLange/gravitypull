#include "GTClock.h"
#include <exception>

namespace GT
{
	GTClock::GTClock() :
		mStartTime(), 
		mCurrentTime(),
		mLastTime(), 
		mFrequency()
	{
		mFrequency = GetFrequency();
		Reset();
	}

	void GTClock::Reset()
	{
		GetTime(mStartTime);
		mCurrentTime = mStartTime;
		mLastTime = mCurrentTime;
	}

	void GTClock::UpdateTime(GTTime& aTime)
	{
		GetTime(mCurrentTime);
		aTime.SetElapsedTime((mCurrentTime.QuadPart - mStartTime.QuadPart) / mFrequency);
		aTime.SetDeltaTime((mCurrentTime.QuadPart - mLastTime.QuadPart) / mFrequency);
		mLastTime = mCurrentTime;
	}

	void GTClock::GetTime(LARGE_INTEGER& aTime) const
	{
		QueryPerformanceCounter(&aTime);
	}

	const double GTClock::GetFrequency() const
	{
		LARGE_INTEGER frequency;

        if (QueryPerformanceFrequency(&frequency) == false)
        {
            throw std::exception("QueryPerformanceFrequency() failed.");
        }

        return (double)frequency.QuadPart;
	}

	const LARGE_INTEGER& GTClock::GetStartTime() const
	{
		return mStartTime;
	}

	const LARGE_INTEGER& GTClock::GetCurrentTime() const
	{
		return mCurrentTime;
	}

	const LARGE_INTEGER& GTClock::GetLastTime() const
	{
		return mLastTime;
	}
};