#ifndef FILE_H_
#define FILE_H_

#include <string>

namespace GPECore
{
	class File
	{
	public:
		File();
		virtual ~File();

		void SetName(std::wstring a_Name);
		std::wstring GetName();
		std::wstring GetType();

	private:
		std::wstring m_Type;
		std::wstring m_Name;
	};
};

#endif