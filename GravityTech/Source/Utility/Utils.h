#ifndef UTILS_H_
#define UTILS_H_

#include "Common.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define SAFE_RELEASE(p) { if ( (p) ) { (p)->Release(); (p) = 0; } }
#define SAFE_DELETE(p) { if ( (p) ) { delete (p); (p) = nullptr; } }
#define SAFE_ARRAY_DELETE(p) { if ( (p) ) { delete[] (p); (p) = nullptr; } }

#ifdef GT_EXPORTS
#define PRINT_ERROR(text) { std::cout << "GT ERROR: " << text << std::endl; }
#define PRINT_WARNING(text) { std::cout << "GT WARNING: " << text << std::endl; }
#define PRINT_MESSAGE(text) { std::cout << "GT MESSAGE: " << text << std::endl; }
#else
#define PRINT_ERROR(text) { std::cout << "GAME ERROR: " << text << std::endl; }
#define PRINT_WARNING(text) { std::cout << "GAME WARNING: " << text << std::endl; }
#define PRINT_MESSAGE(text) { std::cout << "GAME MESSAGE: " << text << std::endl; }
#endif


namespace GT
{
	static std::string WStringToString(std::wstring aWString)
	{
		std::string s;
		s.assign(aWString.begin(), aWString.end());
		return s;
	}

	static std::wstring StringToWString(std::string aString)
	{
		std::wstring ws;
		ws.assign(aString.begin(), aString.end());
		return ws;
	}

	class GT_API Utils
	{
	public:
		static void OpenConsole();
		static bool CreateGTWindow(HWND& aWindow, LPCWSTR aTitle, HINSTANCE ahInstance,int aShowWnd, int aWidth = 1280, int aHeight = 720); 
		static wstring& StringToWString(const string& aString);
		static string& WStringToString(const wstring& aWString);

		static void GetFileNameAndDirectory(const wstring& aInputPath, wstring& aDirectory, wstring& aFileName);
		static wstring& GetFileName(const wstring& aInputPath);
		static wstring& GetDirectory(const wstring& aInputPath);

		static void LoadBinaryFile(const wstring& aFileName, vector<char>& aData);
	};
}

#endif