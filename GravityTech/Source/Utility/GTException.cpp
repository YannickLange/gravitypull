#include "GTException.h"

namespace GT
{
	GTException::GTException(const char* const& aMessage, const HRESULT aResult) :
		exception(aMessage), mResult(aResult)
	{
	}

	HRESULT GTException::GetResult() const
	{
		return mResult;
	}

	wstring GTException::Whatw() const
	{
		string whatString(what());
		wstring whatw;
		whatw.assign(whatString.begin(), whatString.end());

		return whatw;
	}
};