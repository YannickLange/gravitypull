#ifndef GTTIME_H_
#define GTTIME_H_

#include "Export.h"

namespace GT
{
	class GT_API GTTime
	{
	public:
		GTTime();

		void Update();

		void SetElapsedTime(const double aElapsedTime);
		void SetDeltaTime(const double aDeltaTime);

		const double GetDeltaTime() const;
		const double GetElapsedTime() const;
	private:
		double mDeltaTime;
		double mElapsedTime;
	};
};

#endif